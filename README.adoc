This program helps with practising numbers in different languages and numeral systems.

. Install requirements by running `pip install -r requirements.txt`; 
. Change configurations by editing `setting.yaml` and `/rule/setting/\*.yaml`; 
. Start by running `python numerate.py`.
