from random import getrandbits, randint

def s_negative(s_notation):
    if bool(getrandbits(1)):
        return '-' + s_notation
    return s_notation

def s_integer(d_practice_setting):
    s_integer_range = d_practice_setting['integer range']
    return str(randint(s_integer_range[0], s_integer_range[1]))

def s_ordinal_number(d_practice_setting):
    return s_integer() + '.'

def s_nominal_number(d_practice_setting):
    s_nominal_number = ''
    for i_place in range(d_practice_setting['nominal_number length']):
        s_nominal_number += str(randint(0, 10))
    return '#' + s_nominal_number

def integer_percent(d_practice_setting):
    return s_integer() + '%'

def fraction(d_practice_setting):
    s_numerator_range = d_practice_setting['numerator range']
    s_numerator = str(randint(s_numerator_range[0], s_numerator_range[1]))
    s_denominator_range = d_practice_setting['denominator range']
    s_denominator = str(randint(s_denominator_range[0], s_denominator_range[1]))
    return s_numerator + '/' + s_denominator

def mixed_number(d_practice_setting):
    s_mixed_number = s_integer() + '+' + fraction()
    return s_mixed_number

def decimal(d_practice_setting):
    s_integer_range = d_practice_setting['integer range']
    s_integer = str(randint(s_integer_range[0], s_integer_range[1]))
    s_decimal = ''
    for i_place in range(d_practice_setting['decimal length']):
        s_decimal += str(randint(0, 10))
    return s_integer + '.' + s_decimal

def decimal_percent(d_practice_setting):
    return decimal() + '%'

def scientific_notation(d_practice_setting):
    s_coefficient_integer = str(randint(0, 10))
    s_coefficient_decimal = ''
    for i_place in range(d_practice_setting['scientific_notation coefficient decimal length']):
        s_coefficient_decimal += str(randint(0, 10))
    s_coefficient = s_coefficient_integer + '.' + s_coefficient_decimal
    s_exponent_range = d_practice_setting['scientific_notation exponent range']
    s_exponent = str(randint(s_exponent_range[0], s_exponent_range[1]))
    s_exponent = s_negative(s_exponent)
    return s_coefficient + 'E' + s_exponent

def engineering_notation(d_practice_setting):
    s_coefficient_integer = str(randint(0, 1000))
    s_coefficient_decimal = ''
    for i_place in range(d_practice_setting['engineering s_notation s_coefficient s_decimal length']):
        s_coefficient_decimal += str(randint(0, 1000))
    s_coefficient = s_coefficient_integer + '.' + s_coefficient_decimal
    s_exponent_range = d_practice_setting['engineering s_notation s_exponent range']
    s_exponent = str(randint(s_exponent_range[0], s_exponent_range[1]) // 3 * 3)
    s_exponent = s_negative(s_exponent)
    return s_coefficient + 'E' + s_exponent

def date_and_time(d_practice_setting):
    s_calendar = d_practice_setting['calendar']
    s_clock = d_practice_setting['clock']

    li_year_range = d_practice_setting['year range']
    s_year = str(randint(li_year_range[0], li_year_range[1]))

    if s_calendar == 'gregorian':
        s_month = str(randint(1, 12))
        if s_month in ['4', '6', '9', '11']:
            s_day = str(randint(1, 30))
        elif s_month == '2':
            if int(s_year) // 4 == 0 and int(s_year) // 400 != 0:
                s_day = str(randint(1, 29))
            else:
                s_day = str(randint(1, 28))
        else:
            s_day = str(randint(1, 31))
    ls_date = [s_year, s_month, s_day]

    if s_clock == 'european':
        s_hour = str(randint(0, 23))
        s_minute = str(randint(0, 59))
        s_second = str(randint(0, 59))
    ls_time = [s_hour, s_minute, s_second]
    return [ls_date, ls_time]

def s_notation(d_practice_setting):
    s_notation_type = d_practice_setting['notation type']
    if s_notation_type == 'integer':
        return s_integer(d_practice_setting)
    elif s_notation_type == 'ordinal number':
        return s_ordinal_number(d_practice_setting)
    elif s_notation_type == 'nominal number':
        return s_nonnegative_integer(d_practice_setting)
    elif s_notation_type == 'fraction':
        return fraction(d_practice_setting)
    elif s_notation_type == 'mixed number':
        return mixed_number(d_practice_setting)
    elif s_notation_type == 'integer percent' :
        return integer_percent(d_practice_setting)
    elif s_notation_type == 'decimal':
        return decimal(d_practice_setting)
    elif s_notation_type == 'integer percent' :
        return decimal_percent(d_practice_setting)
    elif s_notation_type == 'scientific notation' :
        return scientific_notation(d_practice_setting)
    elif s_notation_type == 'engineering notation' :
        return engineering_notation(d_practice_setting)
    elif s_notation_type == 'date and time' :
        return date_and_time(d_practice_setting)
    return ''
