from importlib import import_module


def b_is_negative(s_notation):
    if s_notation.count('-') == 1:
        return s_notation.startswith('-') and len(s_notation) > 1
    elif s_notation.count('-') == 2:
        if s_notation.count('/') == 1 and s_notation.startswith('-') and len(s_notation) > 1:
            s_notation = s_notation.split('-')
            return s_notation[0] == '' and b_is_nonnegative_integer(s_notation[1]) and b_is_nonnegative_fraction(s_notation[2])
        elif s_notation.startswith('-') and s_notation.count('E-') == 1:
            s_notation = s_notation.split('E')
            return b_is_negative(s_notation[0]) and b_is_negative(s_notation[1])
    return False


def b_is_percent(s_notation):
    return len(s_notation) > 1 and s_notation.endswith('%')


def b_is_nonnegative_integer(s_notation):
    return s_notation.isdecimal()


def b_is_ordinal_number(s_notation):
    s_part = s_notation.split('.')
    return len(s_part) == 2 and b_is_nonnegative_integer(s_part[0]) and s_part[1] == ''


def b_is_nominal_number(s_notation):
    s_part = s_notation.split('#')
    return len(s_part) == 2 and s_part[0] == '' and b_is_nonnegative_integer(s_part[1])


def b_is_nonnegative_fraction(s_notation):
    if s_notation.count('/') == 1:
        s_numerator, s_denominator = s_notation.split('/')
        return b_is_nonnegative_integer(s_numerator) and b_is_nonnegative_integer(s_denominator) and int(s_denominator) > 1
    return False


def b_is_nonnegative_mixed_number(s_notation):
    s_notation = s_notation.replace('-', '+')
    if s_notation.count('+') == 1:
        s_integer, s_fraction = s_notation.split('+')
        return b_is_nonnegative_integer(s_integer) and b_is_nonnegative_fraction(s_fraction)
    return False


def b_is_nonnegative_decimal(s_notation):
    if s_notation.count('.') == 1:
        s_integer, s_decimal = s_notation.split('.')
        return b_is_nonnegative_integer(s_integer) and b_is_nonnegative_integer(s_decimal)
    return False


def b_is_nonnegative_scientific_notation(s_notation):
    if s_notation.count('E') == 1:
        s_coefficient, s_exponent = s_notation.split('E')
        if b_is_nonnegative_integer(s_exponent) or b_is_negative(s_exponent) and b_is_nonnegative_integer(s_exponent.removeprefix('-')):
            if b_is_nonnegative_integer(s_coefficient) or b_is_negative(s_coefficient) and b_is_nonnegative_integer(s_coefficient.removeprefix('-')):
                return abs(int(s_coefficient)) < 10
            if b_is_nonnegative_decimal(s_coefficient) or b_is_negative(s_coefficient) and b_is_nonnegative_decimal(s_coefficient.removeprefix('-')):
                return abs(float(s_coefficient)) < 10
    return False


def b_is_nonnegative_engineering_notation(s_notation):
    if s_notation.count('E') == 1:
        s_coefficient, s_exponent = s_notation.split('E')
        if b_is_nonnegative_integer(s_exponent) or b_is_negative(s_exponent) and b_is_nonnegative_integer(s_exponent.removeprefix('-')):
            if int(s_exponent) % 3 > 0:
                return False
            if b_is_nonnegative_integer(s_coefficient) or b_is_negative(s_coefficient) and b_is_nonnegative_integer(s_coefficient.removeprefix('-')):
                return abs(int(s_coefficient)) < 1000
            if b_is_nonnegative_decimal(s_coefficient) or b_is_negative(s_coefficient) and b_is_nonnegative_decimal(s_coefficient.removeprefix('-')):
                return abs(float(s_coefficient)) < 1000
    return False


def b_is_date_and_time(s_notation, s_calendar='gregorian', s_clock='european'):
    def b_is_date(s_notation):
        s_notation = s_notation.split('-')
        if len(s_notation) == 3 and '' not in s_notation:
            for s_character in s_notation:
                if not s_character.isdecimal():
                    return False
            if s_calendar in [ 'gregorian', 'Japanese', 'Republic of China' ]:
                if s_notation[1] in [ '1', '3', '5', '7', '8', '10', '12' ]:
                    return 1 <= int(s_notation[2]) <= 31
                if s_notation[1] in [ '4', '6', '9', '11' ]:
                    return 1 <= int(s_notation[2]) <= 30
                if s_notation[1] == '2':
                    return 1 <= int(s_notation[2]) <= 29
        return False

    def b_is_time(s_notation):
        s_notation = s_notation.split(':')
        if len(s_notation) == 3 and '' not in s_notation:
            for s_character in s_notation:
                if not s_character.isdecimal():
                    return False
            if s_clock == 'european':
                return 0 <= int(s_notation[0]) <= 23 and 0 <= int(s_notation[1]) <= 59 and 0 <= int(s_notation[2]) <= 60
        return False

    if s_notation.count(' ') != 1:
        return False
    s_date_notation, s_time_notation = s_notation.split(' ')
    return b_is_date(s_date_notation) and b_is_time(s_time_notation)


def b_numeral_system(s_numeral_system, s_notation):
    language_code_module = import_module('library.numeral_system.' + s_numeral_system)
    return language_code_module.b_check(s_notation)
