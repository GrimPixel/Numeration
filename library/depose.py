def s_leading_zeros(s_notation_type, s_notation):
    def s_integer(s_notation):
        if len(s_notation) > 1:
            return s_notation.lstrip('0')
        return s_notation
    
    def s_ordinal_number(s_notation):
        return s_integer(s_notation.removesuffix('.')) + '.'

    def s_decimal(s_notation):
        s_notation = s_integer(s_notation)
        if s_notation[0] == '.':
            return '0' + s_notation
        return s_notation

    def s_fraction(s_notation):
        s_numerator, s_denominator = s_notation.split('/')
        return s_integer(s_numerator) + '/' + s_integer(s_denominator)

    def s_mixed_number(s_notation):
        if b_is_negative:
            s_connector = '-'
            s_integer_part, s_fraction_part = s_notation.split('-')
        else:
            s_connector = '+'
            s_integer_part, s_fraction_part = s_notation.split('+')
        return s_integer(s_integer_part) + s_connector + s_fraction(s_fraction_part)

    def s_scientific_or_engineering_notation(s_notation):
        s_coefficient, s_exponent = s_notation.split('E')
        if '.' in s_coefficient:
            s_coefficient = s_decimal(s_coefficient)
        else:
            s_coefficient = s_integer(s_coefficient)
        if '-' in s_exponent:
            s_exponent = '-' + s_integer(s_exponent.removeprefix('-'))
        else:
            s_exponent = s_integer(s_exponent)
        return s_coefficient + 'E' + s_exponent

    def s_date_and_time(s_notation):
        s_date, s_time = s_notation.split()
        ls_date_unit = s_date.split('-')
        ls_time_unit = s_time.split(':')
        for date_unit_index in range(len(ls_date_unit)):
            ls_date_unit[date_unit_index] = s_integer(ls_date_unit[date_unit_index])
        for time_unit_index in range(len(ls_time_unit)):
            ls_time_unit[time_unit_index] = s_integer(ls_time_unit[time_unit_index])
        s_date = ''
        s_time = ''
        for unit in ls_date_unit:
            s_date += unit + '-'
        for unit in ls_time_unit:
            s_time += unit + ':'
        return s_date.removesuffix('-') + ' ' + s_time.removesuffix(':')

    b_is_negative = s_notation.startswith('-')
    if b_is_negative:
        s_notation = s_notation.removeprefix('-')
    b_is_percent = len(s_notation) > 1 and s_notation.endswith('%')
    if b_is_percent:
        s_notation = s_notation.removesuffix('%')

    if s_notation_type in [ 'integer', 'integer percent' ]:
        s_notation = s_integer(s_notation)
    elif s_notation_type == 'ordinal':
        s_notation = s_ordinal_number(s_notation)
    elif s_notation_type in [ 'decimal', 'decimal percent' ]:
        s_notation = s_decimal(s_notation)
    elif s_notation_type == 'fraction':
        s_notation = s_fraction(s_notation)
    elif s_notation_type == 'mixed number':
        s_notation = s_mixed_number(s_notation)
    elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
        s_notation = s_scientific_or_engineering_notation(s_notation)
    elif s_notation_type == 'date and time' :
        s_notation = s_date_and_time(s_notation)

    if b_is_percent:
        s_notation += '%'
    if b_is_negative:
        s_notation = '-' + s_notation
    return s_notation
