ls_unit_prevalent = [ '𓏺', '𓏻', '𓏼', '𓏽', '𓏾', '𓏿', '𓐀', '𓐁', '𓐂' ]
ls_unit_alternative = '𓐃' # 5
ls_ten_prevalent = [ '𓎆', '𓎇', '𓎈', '𓎉', '𓎊', '𓎋', '𓎌', '𓎍', '𓎎' ]
ls_ten_alternative = [ '𓎏', '𓎐', '𓎑', '𓎒' ] # 20, 30, 40, 50
ls_hundred = [ '𓍢', '𓍣', '𓍤', '𓍥', '𓍦', '𓍧', '𓍨', '𓍩', '𓍪' ]
ls_hundred_alternative = '𓍫' # 500
ls_thousand = [ '𓆼', '𓆽', '𓆾', '𓆿', '𓇀', '𓇁', '𓇂', '𓇃', '𓇄' ]
ls_ten_thousand = [ '𓂭', '𓂮', '𓂯', '𓂰', '𓂱', '𓂲', '𓂳', '𓂴', '𓂵' ]
ls_ten_thousand_alternative: '𓂶' # 50000
ls_date_unit = [ '𓐄', '𓐅', '𓐆', '𓐇', '𓐈', '𓐉', '𓐊', '𓐋', '𓐌' ]
ls_date_ten = [ '𓎭', '𓎮' ]
