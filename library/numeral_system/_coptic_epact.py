ls_unit = [ '𐋡', '𐋢', '𐋣', '𐋤', '𐋥', '𐋦', '𐋧', '𐋨', '𐋩' ]
ls_ten = [ '𐋪', '𐋫', '𐋬', '𐋭', '𐋮', '𐋯', '𐋰', '𐋱', '𐋲' ]
ls_hundred = [ '𐋳', '𐋴', '𐋵', '𐋶', '𐋷', '𐋸', '𐋹', '𐋺', '𐋻' ]
ls_thousand = ' 𐋠'


def b_check(s_notation):
    i_allowed_magnitude = 1
    for i_character_index in range(len(s_notation)):
        if i_character_index in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            if i_character_index < len(s_notation) - 1 and s_notation[i_character_index + 1] == ls_thousand:
                i_allowed_magnitude = 2
            else:
                i_allowed_magnitude = -1
        elif i_character_index in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif i_character_index == ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
        elif i_character_index == ls_thousand:
            if i_allowed_magnitude < 3:
                return False
            i_allowed_magnitude = 2
    return True


def s_convert_to_principal_numeral_system():
    i_value = 0
    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] in ls_unit:
            i_value += (ls_unit.index(s_notation[i_character_index]) + 1) * 1
        elif s_notation[i_character_index] in ls_ten:
            i_value += (ls_ten.index(s_notation[i_character_index]) + 1) * 10
        elif s_notation[i_character_index] in ls_hundred:
            i_value += (ls_hundred.index(s_notation[i_character_index]) + 1) * 100
        elif s_notation[i_character_index] in ls_thousand:
            i_value += (ls_unit.index(s_notation[i_character_index - 1]) + 1) * 999
    return str(i_value)
