ls_unit = [ 'Ι', 'Π' ] # 1, 5
ls_ten = [ 'Δ', '𐅄' ] # 10, 50
ls_hundred = [ 'Η', '𐅅' ] # 100, 500
ls_thousand = [ 'Χ', '𐅆' ] # 1000, 5000
ls_myriad = [ 'Μ', '𐅇' ] # 10000, 50000
ls_one_quarter = [ '𐅀' ]
ls_one_half = [ '𐅁' ]
