ls_unit = [ '𐧀', '𐧁', '𐧂', '𐧃', '𐧄', '𐧅', '𐧆', '𐧇', '𐧈' ]
ls_ten = [ '𐧉', '𐧊', '𐧋', '𐧌', '𐧍', '𐧎', '𐧏' ]
ls_hundred = [ '𐧒', '𐧓', '𐧔', '𐧕', '𐧖', '𐧗', '𐧘', '𐧙', '𐧚' ]
ls_thousand = [ '𐧛', '𐧜', '𐧝', '𐧞', '𐧟', '𐧠', '𐧡', '𐧢', '𐧣' ]
ls_ten_thousand = [ '𐧤', '𐧥', '𐧦', '𐧧', '𐧨', '𐧩', '𐧪', '𐧫', '𐧬' ]
ls_hundred_thousand = [ '𐧭', '𐧮', '𐧯', '𐧰', '𐧱', '𐧲', '𐧳', '𐧴', '𐧵' ]
ls_half = [ '𐦽' ]
ls_twelfth = [ '𐧶', '𐧷', '𐧸', '𐧹', '𐧺', '𐧻', '𐧼', '𐧽', '𐧾', '𐧿', '𐦼' ]
