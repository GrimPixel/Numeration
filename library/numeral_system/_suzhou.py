ls_unit = [ '〇', '〡', '〢', '〣', '〤', '〥', '〦', '〧', '〨', '〩' ]
ls_unit_supplementary = [ '一', '二', '三' ] # 1', '2', '3
ls_ten = [ '〸', '〹', '〺' ]


def b_check(s_notation):
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_ten:
            if s_character_index > 0:
                return False
            if len(s_notation) == 2:
                if s_notation[-1] not in ls_unit['main'] + ls_unit_supplementary:
                    return False
            if len(s_notation) == 1:
                break
            return False
        else:
            if s_notation[s_character_index] in ls_unit['main'][1:4]:
                if s_character_index < len(s_notation) - 1 and s_notation[s_character_index + 1] in ls_unit['main'][1:4]:
                    return False
            if s_notation[s_character_index] in ls_unit_supplementary:
                if s_character_index < len(s_notation) - 1 and s_notation[s_character_index + 1] in ls_unit_supplementary or s_character_index > 0 and s_notation[x - 1] not in ls_unit['main'][1:4] or s_character_index == 0:
                    return False
    return True


def s_convert_to_principal_numeral_system():
    if s_notation[0] in ls_ten:
        if len(s_notation) == 1:
            return str((ls_ten.index(s_notation) + 1) * 10)
        return str(ls_ten.index(s_notation[0]) + 1) + str(ls_unit['main'].index(s_notation[1]))

    s_s_new_notation = s_notation
    for s_character_index in range(len(ls_unit_supplementary)):
        s_s_new_notation = s_s_new_notation.replace(ls_unit_supplementary[s_character_index], ls_unit['main'][s_character_index + 1])
    return replace_note(s_s_new_notation, ls_unit['main'])
