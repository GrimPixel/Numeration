ls_unit = [ '𖭐', '𖭑', '𖭒', '𖭓', '𖭔', '𖭕', '𖭖', '𖭗', '𖭘', '𖭙' ]
ls_ten = [ '𖭛' ]
ls_archmagnitude = [ '𖭜', '𖭝', '𖭞', '𖭟', '𖭠', '𖭡' ]


def b_check(s_notation):
    ls_unit = d_note['ls_unit']
    ls_ten = d_note['ls_ten']
    ls_archmagnitude = d_note['ls_archmagnitude']

    b_has_ten_or_archmagnitude = False
    for s_character in s_notation:
        if s_character in [ls_ten] + ls_archmagnitude:
            b_has_ten_or_archmagnitude = True
            break

    i_allowed_archmagnitude = 6
    i_allowed_magnitude = 1
    if b_has_ten_or_archmagnitude:
        for i_character_index in range(len(s_notation)):
            if s_notation[i_character_index] in ls_unit:
                if i_character_index > 0:
                    if s_notation[i_character_index - 1] in ls_unit:
                        return False
                    if s_notation[i_character_index - 1] == ls_ten:
                        i_allowed_magnitude = -1
            elif s_notation[i_character_index] == ls_ten:
                if i_allowed_magnitude < 1 or i_character_index > 0 and s_notation[i_character_index - 1] == ls_ten:
                    return False
                i_allowed_magnitude = 0
            elif s_notation[i_character_index] in ls_archmagnitude:
                if i_allowed_archmagnitude < ls_archmagnitude.index(s_notation[i_character_index]) + 1:
                    return False
                i_allowed_archmagnitude = ls_archmagnitude.index(s_notation[i_character_index])
                i_allowed_magnitude = 2
            else:
                if not b_is_negative(s_notation) and i_character_index == 0:
                    return False
    return True


def s_convert_to_principal_numeral_system(s_notation):
    ls_unit = d_note['ls_unit']
    ls_ten = d_note['ls_ten']
    ls_archmagnitude = d_note['ls_archmagnitude']

    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    b_has_ten_or_archmagnitude = False
    for s_character in s_s_new_notation:
        if s_character in [ls_ten] + ls_archmagnitude:
            b_has_ten_or_archmagnitude = True
            break

    if b_has_ten_or_archmagnitude:
        i_value = 0
        i_archmagnitude_value = 0
        for i_character_index in range(len(s_s_new_notation)):
            if s_s_new_notation[i_character_index] in ls_unit:
                i_archmagnitude_value += ls_unit.index(s_s_new_notation[i_character_index])
            elif s_s_new_notation[i_character_index] == ls_ten:
                if i_character_index > 0 and s_s_new_notation[i_character_index - 1] in ls_unit:
                    i_archmagnitude_value += ls_unit.index(s_s_new_notation[i_character_index - 1]) * 9
                else:
                    i_archmagnitude_value += 10
            elif s_s_new_notation[x] in ls_archmagnitude:
                if i_archmagnitude_value == 0:
                    i_archmagnitude_value = 1
                i_archmagnitude_value *= 100 ** (ls_archmagnitude.index(s_s_new_notation[x]) + 1)
                i_value += i_archmagnitude_value
                i_archmagnitude_value = 0
        i_value += i_archmagnitude_value
        if b_is_negative:
            i_value = -i_value
        return str(i_value)
    return replace_note(s_s_new_notation, ls_unit)
