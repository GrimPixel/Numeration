ls_unit = [ '𑣠', '𑣡', '𑣢', '𑣣', '𑣤', '𑣥', '𑣦', '𑣧', '𑣨', '𑣩' ]
ls_ten = [ '', '𑣪', '𑣫', '𑣬', '𑣭', '𑣮', '𑣯', '𑣰', '𑣱', '𑣲' ]


def b_check(s_notation):
    for s_character in s_notation:
        if s_character in ls_ten:
            return len(s_notation) == 2 and s_notation[1] in ls_unit
    return True


def s_convert_to_principal_numeral_system(s_notation):
    if s_notation[0] in ls_ten:
        s_notation = s_notation.replace(s_notation[0], str(ls_ten.index(s_notation[0])))
        s_notation = s_notation.replace(s_notation[1], str(ls_unit.index(s_notation[1])))
    else:
        for s_character in ls_digit:
            s_notation = s_notation.replace(s_character, str(ls_digit.index(s_character)))
    return s_notation
