ls_unit = [ '፩', '፪', '፫', '፬', '፭', '፮', '፯', '፰', '፱' ]
ls_ten = [ '፲', '፳', '፴', '፵', '፶', '፷', '፸', '፹', '፺' ]
ls_hundred_multiplier = [ '፻' ]
ls_myriad_multiplier = [ '፼' ]


def b_check(s_notation):
    i_allowed_archmagnitude = 2
    i_allowed_magnitude = 1
    for s_character in s_notation:
        if s_character in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            i_allowed_magnitude = -1
        elif s_character in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif s_character == ls_hundred:
            if i_allowed_archmagnitude < 1:
                return False
            i_allowed_archmagnitude = 0
            i_allowed_magnitude = 1
        elif s_character == ls_myriad:
            if i_allowed_archmagnitude < 2:
                return False
            i_allowed_archmagnitude = 1
            i_allowed_magnitude = 1
    return True

def s_convert_to_principal_numeral_system(d_note, s_notation):
    i_value = 0
    ls_archi_magnitude_value = 0
    for s_character in s_notation:
        if s_character in ls_unit:
            ls_archi_magnitude_value += (ls_unit.index(s_character) + 1) * 1
        elif s_character in ls_ten:
            ls_archi_magnitude_value += (ls_ten.index(s_character) + 1) * 10
        elif s_character == ls_hundred:
            if ls_archi_magnitude_value == 0:
                ls_archi_magnitude_value = 1
            ls_archi_magnitude_value *= 100
            i_value += ls_archi_magnitude_value
            ls_archi_magnitude_value = 0
        elif s_character == ls_myriad:
            if ls_archi_magnitude_value == 0:
                ls_archi_magnitude_value = 1
            ls_archi_magnitude_value *= 10000
            i_value += ls_archi_magnitude_value
            ls_archi_magnitude_value = 0
    return str(i_value + ls_archi_magnitude_value)
