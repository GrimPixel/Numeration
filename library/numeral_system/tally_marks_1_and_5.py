ls_note = [ '𝍷', '𝍸' ]


def b_check(s_notation):
    return not (ls_note[0] * 5 in s_notation or ls_note[0] + ls_note[1] in s_notation)


def s_convert_to_principal_numeral_system(s_notation):
    return str(s_notation.count(ls_note[1]) * 5 + s_notation.count(ls_note[0]))
