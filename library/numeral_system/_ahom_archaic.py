ls_unit = [ '𑜰', '𑜱', '𑜲', '𑜳', '𑜴', '𑜵', '𑜶', '𑜷', '𑜸', '𑜹' ]
ls_ten = [ '𑜺', '𑜻' ]
  
def b_check(s_notation):
    for s_character_index in range(len(s_notation)):
        if s_character_index in ls_ten:
            if s_character_index > 0 or len(s_notation) > 2 or len(s_notation) == 2 and s_notation[x + 1] not in ls_unit:
                return False
            break
    return True

def s_convert_to_principal_numeral_system(s_notation):
    if s_notation.startswith('-'):
        b_is_negative = True
        s_new_notation = s_notation[1:]
    else:
        b_is_negative = False
        s_new_notation = s_notation

    b_has_ten = False
    for s_character in s_new_notation:
        if s_character in ls_ten:
            b_has_ten = True
    if b_has_ten:
        if len(s_new_notation) == 1:
            s_new_notation = str((ls_ten.index(s_new_notation[0]) + 1) * 10)
        else:
            s_new_notation = str((ls_ten.index(s_new_notation[0]) + 1) * 10 + ls_unit.index(s_new_notation[1]) * 1)
        if b_is_negative:
            s_new_notation = '-' + s_new_notation
        return s_new_notation
    return replace_note(s_notation, note['ls_unit'])
