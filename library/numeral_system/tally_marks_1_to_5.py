ls_note = [ '𝍲', '𝍳', '𝍴', '𝍵', '𝍶' ]


def b_check(s_notation):
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] != ls_note[4] and s_character_index < len(s_notation) - 1:
            return False
    return True


def s_convert_to_principal_numeral_system(s_notation):
    return str(s_notation.count(ls_note[4]) * 5 + ls_note.index(s_notation[-1]) + 1)
