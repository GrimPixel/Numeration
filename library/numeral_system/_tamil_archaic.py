ls_unit = [ '௦', '௧', '௨', '௩', '௪', '௫', '௬', '௭', '௮', '௯' ]
ls_magnitude = [ '௰', '௱', '௲' ]
ls_three_hundred_twentieth = [ '𑿀' ]
ls_one_hundred_sixtieth = [ '𑿁' ]
ls_one_eightieth = [ '𑿂' ]
ls_three_eightieth = [ '𑿆' ]
ls_one_sixty_fourth = [ '𑿃' ]
ls_three_sixty_fourth = [ '𑿇' ]
ls_fortieth = [ '𑿄' ]
ls_thirty_second = [ '𑿅' ]
ls_one_twentieth = [ '𑿈' ]
ls_three_twentieth = [ '𑿍' ]
ls_one_sixteenth = [ '𑿉', '𑿊' ]
ls_three_sixteenth = [ '𑿎' ]
ls_tenth = [ '𑿋' ]
ls_eighth = [ '𑿌' ]
ls_fifth = [ '𑿏' ]
ls_fourth = [ '𑿐', '𑿓' ]
ls_half = [ '𑿑', '𑿒' ]
ls_one_three_hundred_twentieth = [ '𑿔' ]


def b_check(s_notation):
    ls_the_fraction = []
    for s_character in ls_fraction:
        if isinstance(ls_fraction[s_character], str):
            ls_the_fraction += [ls_fraction[s_character]]
        elif isinstance(ls_fraction[s_character], list):
            ls_the_fraction += ls_fraction[s_character]
        elif isinstance(ls_fraction[s_character], dict):
            for x in ls_fraction[s_character]:
                if isinstance(x, str):
                    ls_the_fraction += [x]
                elif isinstance(x, list):
                    ls_the_fraction += x

    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_the_fraction:
            if s_character_index < len(s_notation) - 1:
                return False
            s_nonfraction_notation = s_notation[:-1]
            break
    else:
        s_nonfraction_notation = s_notation

    for s_character in s_nonfraction_notation:
        if s_character in ls_magnitude:
            b_has_magnitude = True
            break
    else:
        b_has_magnitude = False

    if b_has_magnitude:
        allowed_magnitude = 3
        for s_character in s_nonfraction_notation:
            for x in range(len(ls_magnitude)):
                if s_character == ls_magnitude[x]:
                    if allowed_magnitude < x + 1:
                        return False
                    allowed_magnitude = x
    return True

def s_convert_to_principal_numeral_system(s_notation):
    ls_the_fraction = []
    for s_fraction in ls_fraction:
        if isinstance(ls_fraction[s_fraction], str):
            ls_the_fraction += [ls_fraction[s_fraction]]
        elif isinstance(ls_fraction[s_fraction], list):
            ls_the_fraction += ls_fraction[s_fraction]
        elif isinstance(ls_fraction[s_fraction], dict):
            for x in ls_fraction[s_fraction]:
                if isinstance(x, str):
                    ls_the_fraction += [x]
                elif isinstance(x, list):
                    ls_the_fraction += x

    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    b_has_fraction = False
    b_has_downscaler = False
    s_numerator = ''
    s_denominator = ''
    s_fraction_notation = ''
    for s_character_index in range(len(s_s_new_notation)):
        if s_s_new_notation[s_character_index] in ls_the_fraction:
            b_has_fraction = True
            s_numerator = ls_european_note[1]
            if s_s_new_notation[s_character_index] == ls_fraction['320']:
                s_denominator = ls_european_note[3] + ls_european_note[2] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] == ls_fraction['160']:
                s_denominator = ls_european_note[1] + ls_european_note[6] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] in ls_fraction['80']:
                if s_s_new_notation[s_character_index] == ls_fraction['80'][1]:
                    s_numerator = ls_european_note[3]
                s_denominator = ls_european_note[8] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] == ls_fraction['64']:
                if s_s_new_notation[s_character_index] == ls_fraction['64'][1]:
                    s_numerator = ls_european_note[3]
                s_denominator = ls_european_note[6] + ls_european_note[4]
            elif s_s_new_notation[s_character_index] == ls_fraction['40']:
                s_denominator = ls_european_note[4] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] == ls_fraction['32']:
                s_denominator = ls_european_note[3] + ls_european_note[2]
            elif s_s_new_notation[s_character_index] == ls_fraction['20']:
                if s_s_new_notation[s_character_index] == ls_fraction['20'][1]:
                    s_numerator = ls_european_note[3]
                s_denominator = ls_european_note[2] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] in ls_fraction['16']['1']:
                s_numerator = ls_european_note[1]
            elif s_s_new_notation[s_character_index] == ls_fraction['16']['3']:
                s_numerator = ls_european_note[3]
                s_denominator = ls_european_note[1] + ls_european_note[6]
            elif s_s_new_notation[s_character_index] == ls_fraction['10']:
                s_denominator = ls_european_note[1] + ls_european_note[0]
            elif s_s_new_notation[s_character_index] == ls_fraction['8']:
                s_denominator = ls_european_note[8]
            elif s_s_new_notation[s_character_index] == ls_fraction['5']:
                s_denominator = ls_european_note[5]
            elif s_s_new_notation[s_character_index] in ls_fraction['2']['1']:
                s_denominator = ls_european_note[2]
            elif s_s_new_notation[s_character_index] in ls_fraction['1/320']:
                s_denominator = ls_european_note[3] + ls_european_note[2] + ls_european_note[0]
                b_has_downscaler = True
    if b_has_fraction:
        s_fraction_notation = s_numerator + s_slash + s_denominator
        s_nonfraction_notation = s_s_new_notation[:-1]
    else:
        s_nonfraction_notation = s_s_new_notation

    b_has_nonfraction = s_s_new_notation[0] in ls_unit + ls_magnitude
    for s_character in s_nonfraction_notation:
        if s_character in ls_magnitude:
            b_has_magnitude = True
            break
    else:
        b_has_magnitude = False
    if b_has_nonfraction:
        for s_character in s_notation:
            if s_character in ls_magnitude:
                b_has_magnitude = True
                break
        if b_has_magnitude:
            i_value = 0
            i_magnitude_value = 0
            for s_character_index in range(len(s_notation)):
                if s_notation[s_character_index] in ls_unit:
                    i_magnitude_value += ls_unit.index(s_notation[s_character_index])
                elif s_notation[s_character_index] in ls_magnitude:
                    if i_magnitude_value == 0:
                        i_magnitude_value = 10 ** (ls_magnitude.index(s_notation[s_character_index]) + 1)
                    else:
                        i_magnitude_value += ls_unit.index(s_notation[s_character_index - 1]) * (10 ** (ls_magnitude.index(s_notation[s_character_index]) + 1) - 1)
                    i_value += i_magnitude_value
                    i_magnitude_value = 0
            i_value += i_magnitude_value
            s_nonfraction_notation = str(i_value)
        else:
            s_nonfraction_notation = replace_note(s_nonfraction_notation, ls_unit)

    if b_has_downscaler:
        s_fraction_notation = s_nonfraction_notation + s_slash + ls_european_note[3] + ls_european_note[2] + ls_european_note[0]
        b_has_fraction = True
        b_has_nonfraction = False

    if b_has_nonfraction and not b_has_fraction:
        if b_is_negative:
            return '-' + s_nonfraction_notation
        return s_nonfraction_notation
    elif not b_has_nonfraction and b_has_fraction:
        if b_is_negative:
            return '-' + s_fraction_notation
        return s_fraction_notation
    else:
        if b_is_negative:
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        return s_nonfraction_notation + s_plus + s_fraction_notation
