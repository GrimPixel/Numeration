ls_unit = [ '൦', '൧', '൨', '൩', '൪', '൫', '൬', '൭', '൮', '൯' ]
ls_magnitude = [ '൰', '൱', '൲' ]
ls_half = [ '൴' ]
ls_one_quarter = [ '൳' ]
ls_three_quarters = [ '൵' ]
ls_one_fifth = [ '൞' ]
ls_one_eighth = [ '൷' ]
ls_one_tenth = [ '൜' ]
ls_one_sixteenth = [ '൶' ]
ls_three_sixteenth = [ '൸' ]
ls_one_twentieth = [ '൛' ]
ls_three_twentieth = [ '൝' ]
ls_one_fourtieth = [ '൙' ]
ls_three_eightieth = [ '൚' ]
ls_one_one_hundred_sixtieth = [ '൘' ]


def b_check():
    ls_fraction = []
    ls_value = s_fraction.i_values()
    for i_value in ls_value:
        if isinstance(i_value, str):
            ls_fraction += [i_value]
        else:
            ls_fraction += i_value
    s_nonfraction_notation = s_notation
    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] in ls_fraction:
            if i_character_index < len(s_notation) - 1:
                return False
            s_nonfraction_notation = s_notation[:i_character_index]
            break

    b_has_magnitude = False
    for s_character in s_notation:
        if s_character in ls_magnitude:
            b_has_magnitude = True
            break
    i_allowed_magnitude = 3
    if b_has_magnitude:
        for i_character_index in range(len(s_nonfraction_notation)):
            if s_nonfraction_notation[i_character_index] in ls_unit:
                if i_character_index < len(s_nonfraction_notation) - 1 and s_nonfraction_notation[i_character_index + 1] in ls_unit:
                    return False
            else:
                for ls_magnitude_character_index in range(len(ls_magnitude)):
                    if s_nonfraction_notation[i_character_index] == ls_magnitude[ls_magnitude_character_index]:
                        if i_allowed_magnitude < ls_magnitude_character_index + 1:
                            return False
                        i_allowed_magnitude = ls_magnitude_character_index
                        break
                else:
                    return False
    return True

def s_convert_to_principal_numeral_system():
    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    ls_fraction = []
    i_value = s_fraction.i_values()
    for s_character in i_value:
        if isinstance(i_value, str):
            ls_fraction += [chaacter]
        else:
            ls_fraction += s_character

    b_has_fraction = s_s_new_notation[-1] in ls_fraction
    b_has_nonfraction = s_s_new_notation[0] not in ls_fraction
    if b_has_fraction:
        s_fraction_notation = s_s_new_notation[-1]
        s_nonfraction_notation = s_s_new_notation[:-1]
    else:
        s_fraction_notation = ''
        s_nonfraction_notation = s_s_new_notation

    b_has_magnitude = False
    for s_character in s_s_new_notation:
        if s_character in ls_magnitude:
            b_has_magnitude = True
            break

    s_numerator = ls_european_note[1]
    s_denominator = ''
    if b_has_fraction:
        if s_fraction_notation == s_fraction['2']:
            s_denominator = ls_european_note[2]
        elif s_fraction_notation in s_fraction['4']:
            s_denominator = ls_european_note[4]
            if s_fraction_notation == s_fraction['4'][1]:
                s_numerator = ls_european_note[3]
        elif s_fraction_notation == s_fraction['5']:
            s_denominator = ls_european_note[5]
        elif s_fraction_notation == s_fraction['8']:
            s_denominator = ls_european_note[8]
        elif s_fraction_notation == s_fraction['10']:
            s_denominator = ls_european_note[1] + ls_european_note[0]
        elif s_fraction_notation in s_fraction['16']:
            s_denominator = ls_european_note[1] + ls_european_note[6]
            if s_fraction_notation == s_fraction['16'][1]:
                s_numerator = ls_european_note[3]
        elif s_fraction_notation in s_fraction['20']:
            s_denominator = ls_european_note[2] + ls_european_note[0]
            if s_fraction_notation == s_fraction['20'][1]:
                s_numerator = ls_european_note[3]
        elif s_fraction_notation == s_fraction['40']:
            s_denominator = ls_european_note[4] + ls_european_note[0]
        elif s_fraction_notation == s_fraction['80']:
            s_denominator = ls_european_note[8] + ls_european_note[0]
            s_numerator = ls_european_note[3]
        elif s_fraction_notation == s_fraction['160']:
            s_denominator = ls_european_note[1] + ls_european_note[6] + ls_european_note[0]
        s_fraction_notation = s_numerator + s_slash + s_denominator

    if b_has_magnitude:
        i_value = 0
        i_magnitude_value = 0
        for i_character_index in range(len(s_nonfraction_notation)):
            if s_nonfraction_notation[i_character_index] in ls_unit:
                i_magnitude_value += ls_unit.index(s_nonfraction_notation[i_character_index])
            elif s_nonfraction_notation[i_character_index] in ls_magnitude:
                if i_character_index > 0 and s_nonfraction_notation[i_character_index - 1] in ls_unit:
                    i_magnitude_value += ls_unit.index(s_nonfraction_notation[i_character_index - 1]) * (10 ** (ls_magnitude.index(s_nonfraction_notation[i_character_index]) + 1) - 1)
                else:
                    i_magnitude_value += 10 ** (ls_magnitude.index(s_nonfraction_notation[i_character_index]) + 1)
                i_value += i_magnitude_value
                i_magnitude_value = 0
            i_value += i_magnitude_value
        s_nonfraction_notation = str(i_value)
    else:
        s_nonfraction_notation = replace_note(s_nonfraction_notation, ls_unit)

    if b_has_nonfraction and not b_has_fraction:
        if b_is_negative:
            return '-' + s_nonfraction_notation
        return s_nonfraction_notation
    elif not b_has_nonfraction and b_has_fraction:
        if b_is_negative:
            return '-' + s_fraction_notation
        return s_fraction_notation
    else:
        if b_is_negative:
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        return s_nonfraction_notation + s_plus + s_fraction_notation
