ls_number = [ '𝋀', '𝋁', '𝋂', '𝋃', '𝋄', '𝋅', '𝋆', '𝋇', '𝋈', '𝋉', '𝋊', '𝋋', '𝋌', '𝋍', '𝋎', '𝋏', '𝋐', '𝋑', '𝋒', '𝋓' ]


def b_check(s_notation):
    return True


def s_convert_from_principal_numeral_system(s_notation):
    i_value = 0
    i_exponent = len(s_notation) - 1
    for s_character in s_notation:
        i_value += ls_number.index(s_character) * (20 ** i_exponent)
    return str(i_value)
