ls_unit = [ 'Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ' ]
ls_ten = [ 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ' ]
ls_hundred = [ 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ' ]
ls_thousand = [ 'Ռ', 'Ս', 'Վ', 'Տ', 'Ր', 'Ց', 'Ւ', 'Փ', 'Ք' ]
ls_multiplier =[ ' ̅' ]

def b_check(s_notation):
    def b_check_digit_group(s_notation):
        i_allowed_magnitude = 3
        for s_character in s_notation:
            if s_character in ls_unit:
                if i_allowed_magnitude < 0:
                    return False
                i_allowed_magnitude = -1
            elif s_character in ls_ten:
                if i_allowed_magnitude < 1:
                    return False
                i_allowed_magnitude = 0
            elif s_character in ls_hundred:
                if i_allowed_magnitude < 2:
                    return False
                i_allowed_magnitude = 1
            elif s_character in ls_thousand:
                if i_allowed_magnitude < 3:
                    return False
                i_allowed_magnitude = 2
        return True

    if ls_multiplier in s_notation:
        time_of_ls_multiplier = 0
        for i_character_index in range(len(s_notation)):
            if s_notation[i_character_index] == ls_multiplier:
                time_of_ls_multiplier += 1
                if time_of_ls_multiplier != (i_character_index + 1) / 2 or i_character_index > 7:
                    return False
        multiplied_notation = s_notation[:2 * time_of_ls_multiplier]
        multiplied_notation = multiplied_notation.replace(ls_multiplier, '')
        unmultiplied_notation = s_notation[2 * time_of_ls_multiplier:]
        if unmultiplied_notation != '':
            return b_check_digit_group(multiplied_notation) and b_check_digit_group(unmultiplied_notation)
        return b_check_digit_group(multiplied_notation)
    return b_check_digit_group(s_notation)


def s_convert_to_principal_numeral_system(s_notation):
    i_value = 0

    def i_value_to_add(index):
        i_value = 0
        if s_notation[index] in ls_unit:
            i_value = (ls_unit.index(s_notation[index]) + 1) * 1
        elif s_notation[index] in ls_ten:
            i_value = (ls_ten.index(s_notation[index]) + 1) * 10
        elif s_notation[index] in ls_hundred:
            i_value = (ls_hundred.index(s_notation[index]) + 1) * 100
        elif s_notation[index] in ls_thousand:
            i_value = (ls_thousand.index(s_notation[index]) + 1) * 1000
        return i_value

    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] == ls_multiplier:
            i_value += i_value_to_add(i_character_index - 1) * 9999
        else:
            i_value += i_value_to_add(i_character_index)
    return str(i_value)
