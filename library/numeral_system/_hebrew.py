ls_unit = [ 'א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ז', 'ח', 'ט' ]
ls_ten = [ 'י', 'כ', 'ל', 'מ', 'נ', 'ס', 'ע', 'פ', 'צ' ]
ls_hundred = [ 'ק', 'ר', 'ש', 'ת', 'ך', 'ם', 'ן', 'ף', 'ץ' ]
s_multiplier = '׳'
s_initiator = '״'


def b_check(s_notation):
    s_new_notation = s_notation.replace(ls_hundred[0] + s_initiator + ls_hundred[3] * 2, ls_hundred[8])
    s_new_notation = s_new_notation.replace(ls_hundred[3] + s_initiator + ls_hundred[3], ls_hundred[7])
    s_new_notation = s_new_notation.replace(ls_hundred[2] + s_initiator + ls_hundred[3], ls_hundred[6])
    s_new_notation = s_new_notation.replace(ls_hundred[1] + s_initiator + ls_hundred[3], ls_hundred[5])
    s_new_notation = s_new_notation.replace(ls_hundred[0] + s_initiator + ls_hundred[3], ls_hundred[4])
    s_new_notation = s_new_notation.replace(ls_unit[8] + ls_unit[4], ls_ten[0] + ls_unit[5])
    s_new_notation = s_new_notation.replace(ls_unit[8] + ls_unit[5], ls_ten[0] + ls_unit[4])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[0] + s_initiator + (s_multiplier + ls_hundred[3]) * 2, s_multiplier + ls_hundred[8])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[3] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[7])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[2] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[6])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[1] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[5])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[0] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[4])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_unit[8] + s_multiplier + ls_unit[4], s_multiplier + ls_ten[0] + s_multiplier + ls_unit[5])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_unit[8] + s_multiplier + ls_unit[5], s_multiplier + ls_ten[0] + s_multiplier + ls_unit[4])

    i_allowed_magnitude = 2
    i_allowed_archmagnitude = 1
    for i_character_index in range(len(s_new_notation)):
        if i_character_index in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            i_allowed_magnitude = -1
        elif i_character_index in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif i_character_index in ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
        elif i_character_index == s_multiplier:
            if i_allowed_archmagnitude < 1:
                return False
            if i_character_index % 2 != 0:
                return False
            if i_character_index < len(s_new_notation) - 3 and s_new_notation[i_character_index + 2] != s_multiplier:
                i_allowed_archmagnitude = 0
                i_allowed_magnitude = 2
    return True


def s_convert_to_principal_numeral_system(s_notation):
    s_new_notation = s_notation.replace(ls_hundred[0] + s_initiator + ls_hundred[3] * 2, ls_hundred[8])
    s_new_notation = s_new_notation.replace(ls_hundred[3] + s_initiator + ls_hundred[3], ls_hundred[7])
    s_new_notation = s_new_notation.replace(ls_hundred[2] + s_initiator + ls_hundred[3], ls_hundred[6])
    s_new_notation = s_new_notation.replace(ls_hundred[1] + s_initiator + ls_hundred[3], ls_hundred[5])
    s_new_notation = s_new_notation.replace(ls_hundred[0] + s_initiator + ls_hundred[3], ls_hundred[4])
    s_new_notation = s_new_notation.replace(ls_unit[8] + ls_unit[4], ls_ten[0] + ls_unit[5])
    s_new_notation = s_new_notation.replace(ls_unit[8] + ls_unit[5], ls_ten[0] + ls_unit[4])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[0] + s_initiator + (s_multiplier + ls_hundred[3]) * 2, s_multiplier + ls_hundred[8])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[3] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[7])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[2] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[6])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[1] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[5])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_hundred[0] + s_initiator + s_multiplier + ls_hundred[3], s_multiplier + ls_hundred[4])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_unit[8] + s_multiplier + ls_unit[4], s_multiplier + ls_ten[0] + s_multiplier + ls_unit[5])
    s_new_notation = s_new_notation.replace(s_multiplier + ls_unit[8] + s_multiplier + ls_unit[5], s_multiplier + ls_ten[0] + s_multiplier + ls_unit[4])

    def i_magnitude_value(the_i_value, x):
        if s_new_notation[x] in ls_unit:
            the_i_value = (ls_unit.index(s_new_notation[x]) + 1) * 1
        elif s_new_notation[x] in ls_ten:
            the_i_value = (ls_ten.index(s_new_notation[x]) + 1) * 10
        elif s_new_notation[x] in ls_hundred:
            the_i_value = (ls_hundred.index(s_new_notation[x]) + 1) * 100
        return the_i_value

    i_value = 0
    for i_character_index in range(len(s_new_notation)):
        if s_new_notation[i_character_index] == s_multiplier:
            i_value += i_magnitude_value(i_value, i_character_index + 1) * 999
        else:
            i_value += i_magnitude_value(i_value, i_character_index)
    return str(i_value)
