from pathlib import Path

ls_unit = [ 'Ⲁ', 'Ⲃ', 'Ⲅ', 'Ⲇ', 'Ⲉ', 'Ⲋ', 'Ⲍ', 'Ⲏ', 'Ⲑ' ]
ls_ten = [ 'Ⲓ', 'Ⲕ', 'Ⲗ', 'Ⲙ', 'Ⲛ', 'Ⲝ', 'Ⲟ', 'Ⲡ', 'Ϥ' ]
ls_hundred = [ 'Ⲣ', 'Ⲥ', 'Ⲧ', 'Ⲩ', 'Ⲫ', 'Ⲭ', 'Ⲯ', 'Ⲱ', 'Ⳁ' ]
ls_fraction = [ '⳽' ] # 1/2


def b_check(s_notation):
    b_has_fraction = False
    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] == ls_fraction:
            if i_character_index < len(s_notation) - 1:
                return False
            b_has_fraction = True
            break

    if b_has_fraction:
        for i_character_index in range(len(s_notation)):
            if s_notation[i_character_index] not in ls_unit + ls_ten + ls_hundred + [ls_fraction]:
                return False
    else:
        i_allowed_magnitude = 2
        for i_character_index in range(len(s_notation)):
            if s_notation[i_character_index] in ls_unit:
                if i_allowed_magnitude < 0:
                    return False
                i_allowed_magnitude = -1
            elif s_notation[i_character_index] in ls_ten:
                if i_allowed_magnitude < 1:
                    return False
                i_allowed_magnitude = 0
            elif s_notation[i_character_index] in ls_hundred:
                if i_allowed_magnitude < 2:
                    return False
                i_allowed_magnitude = 1
    return True

def s_convert_to_principal_numeral_system(s_notation):
    ls_european_note = yaml.load(Path('rule/lexicon/note.yaml').read_text())['western arabic']

    s_plus = yaml.load(Path('rule/lexicon/_sign.yaml').read_text())['plus']
    s_slash = yaml.load(Path('rule/lexicon/_sign.yaml').read_text())['slash']

    b_has_fraction = s_notation.endswith(ls_fraction)
    i_value = 0
    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] in ls_unit:
            i_value += (ls_unit.index(s_notation[i_character_index]) + 1) * 1
        elif s_notation[i_character_index] in ls_ten:
            i_value += (ls_ten.index(s_notation[i_character_index]) + 1) * 10
        elif s_notation[i_character_index] in ls_hundred:
            i_value += (ls_hundred.index(s_notation[i_character_index]) + 1) * 100

    if b_has_fraction:
        if i_value == 0:
            return ls_european_note[1] + s_slash + ls_european_note[2]
        return str(i_value) + s_plus + ls_european_note[1] + s_slash + ls_european_note[2]
    return str(i_value)
