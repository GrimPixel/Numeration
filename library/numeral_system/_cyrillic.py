ls_unit = [ 'А', 'В', 'Г', 'Д', 'Е', 'Ѕ', 'З', 'И', 'Ѳ' ]
ls_ten = [ 'І', 'К', 'Л', 'М', 'Н', 'Ѯ', 'О', 'П', 'Ч' ]
ls_ten_alternative = [ 'Ҁ' ] # 90
ls_hundred = [ 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ѱ', 'Ѡ', 'Ц' ]
ls_hundred_alternative = [ 'Ѵ', 'Ѧ' ] # 400, 900
ls_multiplier = [ '҂', '⃝', ' ҈', ' ҉', ' ꙰', ' ꙱', ' ꙲' ]


def b_check(s_notation):
    i_allowed_archmagnitude = 7
    i_allowed_magnitude = 2

    for i_character_index in range(len(s_notation)):
        if s_notation[x] in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            if i_character_index > 0 and s_notation[x - 1] == multiplier[0]:
                i_allowed_magnitude = 3
            else:
                i_allowed_magnitude = -1
        elif s_notation[i_character_index] in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif s_notation[i_character_index] in ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
        elif s_notation[i_character_index] in multiplier:
            if s_notation[i_character_index] == multiplier[0]:
                if i_allowed_archmagnitude < 1 or i_character_index == len(s_notation) - 1 or s_notation[i_character_index + 1] in multiplier:
                    return False
            else:
                if i_allowed_archmagnitude < multiplier.index(s_notation[i_character_index]) + 1 or i_character_index == 0 or s_notation[i_character_index - 1] in multiplier:
                    return False
            i_allowed_archmagnitude = multiplier.index(s_notation[i_character_index])
            i_allowed_magnitude = 3
    return True


def s_convert_to_principal_numeral_system(s_notation):
    def i_magnitude_value(i_value, x):
        if s_new_notation[x] in ls_unit:
            i_value = (ls_unit.index(s_new_notation[x]) + 1) * 1
        elif s_new_notation[x] in ls_ten:
            i_value = (ls_ten.index(s_new_notation[x]) + 1) * 10
        elif s_new_notation[x] in ls_hundred:
            i_value = (ls_hundred.index(s_new_notation[x]) + 1) * 100
        return i_value

    i_value = 0
    for i_character_index in range(len(s_new_notation)):
        if s_new_notation[i_character_index] in ls_multiplier:
            if s_new_notation[i_character_index] == ls_multiplier[0]:
                i_archmagnitude_value = i_magnitude_value(i_value, i_character_index + 1) * 999
            else:
                i_archmagnitude_value = i_magnitude_value(i_value, i_character_index - 1) * (1000 ** (multiplier.index(s_new_notation[i_character_index]) + 1) - 1)
        else:
            i_archmagnitude_value = i_magnitude_value(i_value, i_character_index)
        i_value += i_archmagnitude_value
    return str(i_value)
