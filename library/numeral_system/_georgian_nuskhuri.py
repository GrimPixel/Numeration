ls_unit = [ 'ⴀ', 'ⴁ', 'ⴂ', 'ⴃ', 'ⴄ', 'ⴅ', 'ⴆ', 'ⴡ', 'ⴇ' ]
ls_ten = [ 'ⴈ', 'ⴉ', 'ⴊ', 'ⴋ', 'ⴌ', 'ⴢ', 'ⴍ', 'ⴎ', 'ⴏ' ]
ls_hundred = [ 'ⴐ', 'ⴑ', 'ⴒ', 'ⴓ', 'ⴔ', 'ⴕ', 'ⴖ', 'ⴗ', 'ⴘ' ]
ls_hundred_alternative = [ 'ⴣ' ] # 400
ls_thousand = [ 'ⴙ', 'ⴚ', 'ⴛ', 'ⴜ', 'ⴝ', 'ⴞ', 'ⴤ', 'ⴟ', 'ⴠ' ]
ls_myriad = [ 'ⴥ' ]


def b_check(s_notation):
    i_allowed_magnitude = 4
    for s_character in s_notation:
        if s_character in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            i_allowed_magnitude = -1
        elif s_character in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif s_character in ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
        elif s_character in ls_thousand:
            if i_allowed_magnitude < 3:
                return False
            i_allowed_magnitude = 2
        elif s_character == ls_myriad:
            if i_allowed_magnitude < 4:
                return False
            i_allowed_magnitude = 3
    return True

    
def s_convert_to_principal_numeral_system(s_notation):
    i_value = 0
    for i_character_index in range(len(s_notation)):
        if s_notation[i_character_index] in ls_unit:
            i_value += (ls_unit.index(s_notation[i_character_index]) + 1) * 1
        elif s_notation[i_character_index] in ls_ten:
            i_value += (ls_ten.index(s_notation[i_character_index]) + 1) * 10
        elif s_notation[i_character_index] in ls_hundred:
            i_value += (ls_hundred.index(s_notation[i_character_index]) + 1) * 100
        elif s_notation[i_character_index] in ls_thousand:
            i_value += (ls_thousand.index(s_notation[i_character_index]) + 1) * 1000
        elif s_notation[i_character_index] == ls_myriad:
            i_value += 10000
    return str(i_value)
