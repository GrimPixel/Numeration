ls_magnitude_even = [ '𝍩', '𝍪', '𝍫', '𝍬', '𝍭', '𝍮', '𝍯', '𝍰', '𝍱' ]
ls_magnitude_odd = [ '𝍠', '𝍡', '𝍢', '𝍣', '𝍤', '𝍥', '𝍦', '𝍧', '𝍨' ]


def b_check(s_notation):
    ls_magnitude_even = d_note['ls_magnitude even']
    ls_magnitude_odd = d_note['ls_magnitude odd']

    for s_character_negative_index in range(1, len(s_notation) + 1):
        if (s_character_negative_index - 1) % 2 == 0:
            if s_notation[-s_character_negative_index] not in ls_magnitude_even + [' ']:
                return False
        else:
            if s_notation[-s_character_negative_index] not in ls_magnitude_odd + [' ']:
                return False
    return True


def s_convert_to_principal_numeral_system():
    s_new_notation = replace_note(s_notation, [' '] + ls_magnitude_even)
    s_new_notation = replace_note(s_new_notation, [' '] + ls_magnitude_odd)
    return s_new_notation
