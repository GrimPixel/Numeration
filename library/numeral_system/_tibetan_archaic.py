ls_integer = [ '༠', '༡', '༢', '༣', '༤', '༥', '༦', '༧', '༨', '༩' ]
ls_half = [ '༳', '༪', '༫', '༬', '༭', '༮', '༯', '༰', '༱', '༲' ]


def b_check(s_notation):
    if '-' + ls_half[0] in s_notation:
        return False

    for s_character_index in range(len(s_notation)):
        if s_character_index == ls_half:
            if s_character_index < len(s_notation) - 1:
                return False
            for x in range(s_character_index):
                if x not in ls_integer:
                    if not (x == 0 and b_is_negative(s_notation)):
                        return False
    return True


def s_convert_to_principal_numeral_system(s_notation):
    ls_european_digit = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ]

    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    b_has_fraction = s_s_new_notation[-1] in ls_half
    b_has_nonfraction = s_s_new_notation[0] not in ls_half
    if b_has_fraction:
        s_fraction_notation = s_s_new_notation[-1]
        if b_has_nonfraction:
            s_nonfraction_notation = s_s_new_notation[:-1] + ls_integer[ls_half.index(s_fraction_notation)]
            s_nonfraction_notation = replace_note(s_nonfraction_notation, ls_integer)
            s_nonfraction_notation = str(int(s_nonfraction_notation) - 1)
        else:
            s_nonfraction_notation = ls_half.index(s_fraction_notation) - 1
            if s_nonfraction_notation < 0:
                s_nonfraction_notation = 0
            s_nonfraction_notation = str(s_nonfraction_notation)
        b_is_negative = s_fraction_notation == ls_half[0] and not b_has_nonfraction
        s_fraction_notation = ls_european_digit[1] + '/' + ls_european_digit[2]
    else:
        s_fraction_notation = ''
        s_nonfraction_notation = s_s_new_notation
    if b_has_nonfraction and not b_has_fraction:
        s_nonfraction_notation = replace_note(s_nonfraction_notation, ls_integer)

    if b_has_nonfraction and not b_has_fraction:
        if b_is_negative:
            return '-' + s_nonfraction_notation
        return s_nonfraction_notation
    elif not b_has_nonfraction and b_has_fraction:
        if b_is_negative:
            if s_nonfraction_notation == ls_european_digit[0]:
                return '-' + s_fraction_notation
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        if s_nonfraction_notation == ls_european_digit[0]:
            return s_fraction_notation
        return s_nonfraction_notation + '+' + s_fraction_notation
    else:
        if b_is_negative:
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        return s_nonfraction_notation + '+' + s_fraction_notation
