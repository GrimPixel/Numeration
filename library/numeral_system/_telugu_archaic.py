ls_integer = [ '౦', '౧', '౨', '౩', '౪', '౫', '౬', '౭', '౮', '౯' ]
ls_fraction_denominator_four_to_the_odd = [ '౸', '౹', '౺', '౻' ]
ls_fraction_denominator_four_to_the_even = [ '౦', '౼', '౽', '౾' ]


def b_check(s_notation):
    b_has_fraction = False
    for s_character in s_notation:
        if s_character in ls_integer:
            pass
        elif s_character in ls_fraction_denominator_four_to_the_odd or x in ls_fraction_denominator_four_to_the_even:
            b_has_fraction = True
            break
    for s_character_index in range(len(s_notation)):
        if b_has_fraction and s_notation[s_character_index] not in ls_integer:
            if not (s_character_index == 0 and b_is_negative(s_notation)):
                return False
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_fraction_denominator_four_to_the_odd:
            if s_character_index < len(s_notation) - 1 and s_notation[s_character_index + 1] not in ls_fraction_denominator_four_to_the_even:
                return False
        elif s_notation[s_character_index] in ls_fraction_denominator_four_to_the_even:
            if s_character_index < len(s_notation) - 1 and s_notation[s_character_index + 1] not in ls_fraction_denominator_four_to_the_odd:
                return False
    return True

def s_convert_to_principal_numeral_system(s_notation):
    def convert_nons_fraction():
        return replace_note(s_nonfraction_notation, ls_integer)

    def convert_fraction():
        s_numerator = 0
        s_denominator_exponent = 0
        for s_character in s_notation:
            if s_character in s_denominator_4_to_the_odd:
                s_numerator = s_numerator * 4 + s_denominator_4_to_the_odd.index(x)
                s_denominator_exponent += 1
            elif s_character in s_denominator_4_to_the_even:
                s_numerator = s_numerator * 4 + s_denominator_4_to_the_even.index(x)
                s_denominator_exponent += 1
        s_denominator = 4 ** s_denominator_exponent
        return str(s_numerator) + '/' + str(s_denominator)

    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    b_has_fraction = s_s_new_notation[-1] in s_denominator_4_to_the_odd or s_s_new_notation[-1] in s_denominator_4_to_the_even and (len(s_s_new_notation) > 1 and s_s_new_notation[-2] in s_denominator_4_to_the_odd)
    b_has_nonfraction = s_s_new_notation[0] in ls_integer or b_is_negative and s_s_new_notation[1] in ls_integer

    s_nonfraction_notation = s_s_new_notation
    s_fraction_notation = ''

    if b_has_fraction:
        for s_character_index in range(len(s_s_new_notation)):
            if s_s_new_notation[s_character_index] in s_fraction['denominator 4 to the odd']:
                if s_character_index > 0 or b_is_negative and s_character_index > 1:
                    s_nonfraction_notation = s_s_new_notation[:s_character_index]
        s_fraction_notation = convert_fraction()
    if b_has_nonfraction:
        s_nonfraction_notation = convert_nons_fraction()

    if b_has_nonfraction and not b_has_fraction:
        if b_is_negative:
            return '-' + s_nonfraction_notation
        return s_nonfraction_notation
    elif not b_has_nonfraction and b_has_fraction:
        if b_is_negative:
            return '-' + s_fraction_notation
        return s_fraction_notation
    else:
        if b_is_negative:
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        return s_nonfraction_notation + '+' + s_fraction_notation
