ls_unit = [ 'ا', 'ب', 'ج', 'د', 'ه', 'و', 'ز', 'ح', 'ط' ]
ls_persian_unit = [ 'پ', 'چ', 'ژ' ] # 2', '3', '7
ls_ten = [ 'ي', 'ك', 'ل', 'م', 'ن', 'س', 'ع', 'ف', 'ص' ]
ls_persian_ten = [ 'گ' ] # 20
ls_hundred = [ 'ق', 'ر', 'ش', 'ت', 'ث', 'خ', 'ذ', 'ض', 'ظ' ]
ls_thousand = [ 'غ' ]


def b_check(s_notation):
    i_allowed_magnitude = 3
    for s_character in s_notation:
        if s_character in ls_unit:
            if i_allowed_magnitude < 0:
                return False
            i_allowed_magnitude = -1
        elif s_character in ls_ten:
            if i_allowed_magnitude < 1:
                return False
            i_allowed_magnitude = 0
        elif s_character in ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
        elif s_character in ls_thousand:
            if i_allowed_magnitude < 3:
                return False
            i_allowed_magnitude = 2
    return True


def s_convert_to_principal_numeral_system(s_notation):
    i_value = 0
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_unit:
            i_value += (ls_unit.index(s_notation[s_character_index]) + 1) * 1
        elif s_notation[s_character_index] in ls_ten:
            i_value += (ls_ten.index(s_notation[s_character_index]) + 1) * 10
        elif s_notation[s_character_index] in ls_hundred:
            i_value += (ls_hundred.index(s_notation[s_character_index]) + 1) * 100
        elif s_notation[s_character_index] == ls_thousand:
            i_value += (ls_thousand.index(s_notation[s_character_index]) + 1) * 1000
    return str(i_value)
