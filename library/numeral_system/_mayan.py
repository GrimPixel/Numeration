ls_number = [ '𝋠', '𝋡', '𝋢', '𝋣', '𝋤', '𝋥', '𝋦', '𝋧', '𝋨', '𝋩', '𝋪', '𝋫', '𝋬', '𝋭', '𝋮', '𝋯', '𝋰', '𝋱', '𝋲', '𝋳' ]


def b_check(s_notation):
    return True


def s_convert_from_principal_numeral_system(s_notation):
    i_value = 0
    s_exponent = len(s_notation) - 1
    for s_character in s_notation:
        i_value += ls_number.index(s_character) * (20 ** s_exponent)
        s_exponent -= 1
    return str(i_value)


def s_convert_to_principal_numeral_system(s_notation):
    import re
    for s_character in s_notation:
        if not re.search('[0-9]', s_character):
            return ''
    i_value = int(s_notation)
    i_allowed_value = 0
    i_place = 1
    while True:
        i_allowed_value = 20 ** i_place -1
        if i_allowed_value >= i_value:
            break
        i_place += 1
    s_s_new_notation = ''
    while i_place > 0:
        s_s_new_notation += ls_number[int(s_notation) // (20 ** i_place)]
    return s_s_new_notation
