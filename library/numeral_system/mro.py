ls_digit = [ '𖩠', '𖩡', '𖩢', '𖩣', '𖩤', '𖩥', '𖩦', '𖩧', '𖩨', '𖩩' ]


def b_check(s_notation):
    return True


def s_convert_to_principal_numeral_system(s_notation):
    for s_character in ls_digit:
        s_notation = s_notation.replace(s_character, str(ls_digit.index(s_character)))
    return s_notation


def s_convert_from_principal_numeral_system(s_notation):
    for s_character in ls_digit:
        s_notation = s_notation.replace(str(ls_digit.index(s_character)), s_character)
    return s_notation
