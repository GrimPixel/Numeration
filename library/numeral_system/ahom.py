ls_digit = [ '𑜰', '𑜱', '𑜲', '𑜳', '𑜴', '𑜵', '𑜶', '𑜷', '𑜸', '𑜹' ]


def b_check(s_notation):
    return True


def s_convert_to_principal_numeral_system(s_notation):
    for s_character in ls_digit:
        s_notation = s_notation.replace(s_character, str(ls_digit.index(s_character)))
    return s_notation


def s_convert_from_principal_numeral_system(s_notation):
    for s_character in ls_digit:
        s_notation = s_notation.replace(str(ls_digit.index(s_character)), s_character)
    return s_notation
