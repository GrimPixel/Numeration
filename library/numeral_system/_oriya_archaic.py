from pathlib import Path

ls_integer = [ '୦', '୧', '୨', '୩', '୪', '୫', '୬', '୭', '୮', '୯' ]
ls_fourth = [ '୲', '୳', '୴' ]
ls_sixteenth = [ '୵', '୶', '୷' ]


def b_check():
    d_note = yaml.load(Path('rule/lexicon/note.yaml').read_text())['oriya']
    ls_integer = d_note['integer']
    s_fraction = d_note['fraction']

    for i_character_index in range(len(s_notation)):
        if i_character_index == s_fraction:
            if i_character_index in s_fraction['4']:
                for x in range(i_character_index):
                    if x not in ls_integer:
                        if not (x == 0 and b_is_negative(s_notation)):
                            return False
            elif i_character_index in s_fraction['16']:
                if i_character_index < len(s_notation) - 1 or s_notation[i_character_index - 1] not in s_fraction['4']:
                    return False
    return True


def s_convert_to_principal_numeral_system():
    d_note = yaml.load(Path('rule/lexicon/note.yaml').read_text())['oriya']
    ls_integer = d_note['integer']
    s_fraction = d_note['fraction']
    ls_european_note = yaml.load(Path('rule/lexicon/note.yaml').read_text())['western arabic']

    s_plus = yaml.load(Path('rule/lexicon/_sign.yaml').read_text())['plus']
    s_slash = yaml.load(Path('rule/lexicon/_sign.yaml').read_text())['slash']

    b_is_negative = False
    if s_notation.startswith('-'):
        b_is_negative = True
        s_s_new_notation = s_notation[1:]
    else:
        s_s_new_notation = s_notation

    b_has_fraction = s_s_new_notation[-1] in s_fraction['4'] + s_fraction['16']
    b_has_nonfraction = s_s_new_notation[0] in ls_integer
    s_nonfraction_notation = s_s_new_notation
    s_fraction_notation = ''

    s_numerator = ''
    s_denominator = ''
    if b_has_fraction:
        s_nonfraction_notation = s_s_new_notation[:-1]
        if s_s_new_notation[-1] in s_fraction['4']:
            s_numerator = str(s_fraction['4'].index(s_s_new_notation[-1]))
            s_denominator = ls_european_note[4]
        if s_s_new_notation[-1] in s_fraction['16']:
            s_numerator = str(s_fraction['16'].index(s_s_new_notation[-1]))
            if len(s_s_new_notation) > 1 and s_s_new_notation[-2] in s_fraction['4']:
                s_nonfraction_notation = s_s_new_notation[:-2]
                s_numerator = str(int(s_numerator) + s_fraction['4'].index(s_s_new_notation[-2]) * 4)
            s_denominator = ls_european_note[1] + ls_european_note[6]
        s_fraction_notation = s_numerator + s_slash + s_denominator
    if b_has_nonfraction:
        s_nonfraction_notation = replace_note(s_nonfraction_notation, ls_integer)

    if b_has_nonfraction and not b_has_fraction:
        if b_is_negative:
            return '-' + s_nonfraction_notation
        return s_nonfraction_notation
    elif not b_has_nonfraction and b_has_fraction:
        if b_is_negative:
            return '-' + s_fraction_notation
        return s_fraction_notation
    else:
        if b_is_negative:
            return '-' + s_nonfraction_notation + '-' + s_fraction_notation
        return s_nonfraction_notation + s_plus + s_fraction_notation
