ls_unit_prevalent = [ 'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ϛ', 'Ζ', 'Η', 'Θ' ]
ls_unit_alternative = [ 'Ϝ' ] # 6
ls_ten_prevalent = [ 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ϙ' ]
ls_ten_alternative = [ 'Ϟ' ] # 90
ls_hundred_prevalent = [ 'Ρ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ', 'Ω', 'Ͳ' ]
ls_hundred_alternative = [ 'Ϡ' ] # 900
ls_myriad = [ 'Μ' ]
s_upper_sign = 'ʹ'
s_lower_sign = '͵'
s_zero = '𐆊'
s_one_quarter = '𐆋'
s_two_thirds = '𐅷'
s_three_quarters = '𐅸'


def b_check(s_notation):
    s_new_notation = s_notation
    if s_notation.count(s_upper_sign) > 0:
        if s_notation.startswith(s_upper_sign):
            return False
        if s_notation.endswith(s_upper_sign):
            if not (s_notation.count(s_upper_sign) == 2 and s_notation.count(s_upper_sign + ls_myriad) == 1):
                return False
            s_new_notation = s_notation[:-1]
        else:
            if not (s_notation.count(s_upper_sign) == 1 and s_notation.count(s_upper_sign + ls_myriad) == 1):
                return False

    i_allowed_magnitude = 3
    i_allowed_archmagnitude = 1
    for i_character_index in range(len(s_new_notation)):
        if s_new_notation[i_character_index] in ls_unit:
            if i_character_index > 0 and s_new_notation[i_character_index - 1] == s_lower_sign:
                if i_allowed_magnitude < 3:
                    return False
                i_allowed_magnitude = 2
            if i_allowed_magnitude < 0:
                return False
            i_allowed_magnitude = -1
        elif s_new_notation[i_character_index] in ls_ten:
            if s_new_notation[i_character_index] == ls_myriad and i_character_index > 0 and s_new_notation[i_character_index - 1] == s_upper_sign:
                if i_allowed_archmagnitude < 1:
                    return False
                i_allowed_archmagnitude = 0
                i_allowed_magnitude = 3
            else:
                if i_allowed_magnitude < 1:
                    return False
                i_allowed_magnitude = 0
        elif s_new_notation[i_character_index] in ls_hundred:
            if i_allowed_magnitude < 2:
                return False
            i_allowed_magnitude = 1
    return True


def s_convert_to_principal_numeral_system(s_notation):
    i_value = 0
    for i_character_index in range(len(s_new_notation)):
        if s_new_notation[i_character_index] in ls_unit:
            i_value += (ls_unit.index(s_new_notation[i_character_index]) + 1) * 1
        if s_new_notation[i_character_index] in ls_ten:
            if s_new_notation[i_character_index] == ls_myriad and i_character_index > 0 and s_new_notation[i_character_index - 1] == s_upper_sign:
                i_value *= 10000
            else:
                i_value += (ls_ten.index(s_new_notation[i_character_index]) + 1) * 10
        if s_new_notation[i_character_index] in ls_hundred:
            i_value += (ls_hundred.index(s_new_notation[i_character_index]) + 1) * 100
        if s_new_notation[i_character_index] == s_lower_sign:
            i_value += (ls_unit.index(s_new_notation[i_character_index + 1]) + 1) * 999
    return str(i_value)
