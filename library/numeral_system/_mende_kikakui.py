from pathlib import Path

ls_unit = [ '𞣇', '𞣈', '𞣉', '𞣊', '𞣋', '𞣌', '𞣍', '𞣎', '𞣏' ]
ls_eleven_to_nineteen = [ '𞣐' ]
ls_magnitude = [ '𞣑'', ''𞣒, ''𞣓, ''𞣔, ''𞣕, ''𞣖 ]


def b_check(s_notation):
    ls_unit = d_note['ls_unit']
    ls_eleven_to_nineteen = d_note['eleven to nineteen']
    ls_magnitude = d_note['ls_magnitude']

    allowed_magnitude = 6
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_unit:
            if s_character_index > 0 and s_notation[s_character_index - 1] in ls_unit:
                return False
        elif s_notation[s_character_index] in ls_eleven_to_nineteen:
            if allowed_magnitude < 1:
                return False
            allowed_magnitude = -1
        elif s_notation[s_character_index] in ls_magnitude:
            if s_notation[s_character_index] == ls_magnitude[0]:
                if allowed_magnitude < 1 or s_character_index == len(s_notation) - 1 or s_notation[s_character_index + 1] in ls_magnitude:
                    return False
    return True

def s_convert_to_principal_numeral_system():
    d_note = yaml.load(Path('rule/lexicon/note.yaml').read_text())['mende kikakui']
    ls_unit = d_note['ls_unit']
    ls_eleven_to_nineteen = d_note['eleven to nineteen']
    ls_magnitude = d_note['ls_magnitude']

    i_value = 0
    for s_character_index in range(len(s_notation)):
        if s_notation[s_character_index] in ls_unit:
            i_value += (ls_unit.index(s_notation[s_character_index]) + 1) * 1
        elif s_notation[s_character_index] in ls_eleven_to_nineteen:
            i_value += (ls_unit.index(s_notation[s_character_index - 1]) + 1) + 10
        elif s_notation[s_character_index] in ls_magnitude:
            i_value += (ls_unit.index(s_notation[s_character_index - 1]) + 1) * (10 ** (ls_magnitude.index(s_notation[s_character_index]) + 1) - 1)
    return str(i_value)
