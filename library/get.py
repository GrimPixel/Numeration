import library.check as check
import re
from pathlib import Path
from ruamel.yaml import YAML
yaml = YAML(typ='safe')

def i_common_maximal_length(ls_language_code):
    li_maximal_length = []
    for s_the_language_code in ls_language_code:
        if s_the_language_code in ['eng', 'por', 'zho']:
            if s_the_language_code == 'eng':
                s_scale = yaml.load(Path('rule/setting/eng.yaml').read_text())['s scale']
            elif s_the_language_code == 'por':
                s_scale = yaml.load(Path('rule/setting/por.yaml').read_text())['s escala']
            elif s_the_language_code == 'zho':
                s_scale = yaml.load(Path('rule/setting/zho.yaml').read_text())['s 進制']
            i_the_maximal_length = yaml.load(Path('library/dictionary/maximal_length.yaml').read_text())['d '+s_the_language_code]['i '+s_scale]
        else:
            i_the_maximal_length = yaml.load(Path('library/dictionary/maximal_length.yaml').read_text())['i '+s_the_language_code]
        li_maximal_length.append(i_the_maximal_length)
    return min(li_maximal_length)


def s_numeral_system(s_notation):
    ls_character_set = yaml.load(Path('library/dictionary/character_set.yaml').read_text())
    s_numeral_system = ''
    for s_character_set in ls_character_set:
        for s_character in s_notation:
            if not re.search(s_character_set.removeprefix('s '), s_character):
                break
        s_numeral_system = ls_character_set[s_character_set]
        break
    if check.b_numeral_system(s_numeral_system, s_notation):
        return s_numeral_system
    return ''


def s_notation_type(s_notation):
    if check.b_is_nonnegative_integer(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_integer(s_notation.removeprefix('-')):
        return 'integer'
    elif check.b_is_nonnegative_integer(s_notation.removesuffix('%')) and check.b_is_percent(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_integer(s_notation.removeprefix('-').removesuffix('%')) and check.b_is_percent(s_notation):
        return 'integer percent' 
    elif check.b_is_ordinal_number(s_notation):
        return 'ordinal number'
    elif check.b_is_nonnegative_integer(s_notation):
        return 'nominal number'
    elif check.b_is_nonnegative_decimal(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_decimal(s_notation.removeprefix('-')):
        return 'decimal'
    elif check.b_is_nonnegative_decimal(s_notation.removesuffix('%')) and check.b_is_percent(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_decimal(s_notation.removeprefix('-').removesuffix('%')) and check.b_is_percent(s_notation):
        return 'decimal percent' 
    elif check.b_is_nonnegative_fraction(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_fraction(s_notation.removeprefix('-')):
        return 'fraction'
    elif check.b_is_nonnegative_mixed_number(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_mixed_number(s_notation.removeprefix('-')):
        return 'mixed number'
    elif check.b_is_nonnegative_scientific_notation(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_scientific_notation(s_notation.removeprefix('-')):
        return 'scientific notation' 
    elif check.b_is_nonnegative_engineering_notation(s_notation) or check.b_is_negative(s_notation) and check.b_is_nonnegative_engineering_notation(s_notation.removeprefix('-')):
        return 'engineering notation' 
    elif check.b_is_date_and_time(s_notation):
        return 'date and time' 
    return ''
