import library.get as get
import library.convert as convert
from pathlib import Path
from ruamel.yaml import YAML
yaml = YAML(typ='safe')

def s_symbols(s_notation):
    ls_fullwidth_character = [ '＃', '％', '＋', '，', '－', '．', '／', '０', '１', '２', '３', '４', '５', '６', '７', '８', '９', '：' ]
    ls_halfwidth_character = [ '#', '%', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':' ]
    for s_character_index in range(len(ls_fullwidth_character)):
        s_notation = s_notation.replace(ls_fullwidth_character[s_character_index], ls_halfwidth_character[s_character_index])
    
    if ' ' in s_notation:
        if yaml.load(Path('setting.yaml').read_text())['date-and-time format'] == 'time date':
            s_time_notation, s_date_notation = s_notation.split()
        else:
            s_date_notation, s_time_notation = s_notation.split()
        
        s_date_notation = s_date_notation.replace('൹', '')
        s_date_notation = s_date_notation.replace('𖭧', '')
        s_date_notation = s_date_notation.replace('𐅹', '')
        s_date_notation = s_date_notation.replace('.', '-')
        s_date_notation = s_date_notation.replace('‐', '-')
        s_date_notation = s_date_notation.replace('‒', '-')
        s_date_notation = s_date_notation.replace('/', '-')
        s_date_notation = s_date_notation.replace('؍', '-')
        s_time_notation = s_time_notation.replace('.', ':')
        s_date_notation = s_date_notation.split('-')
        s_time_notation = s_time_notation.split(':')

        s_numeral_system = get.s_numeral_system(s_notation)
        s_date_format = yaml.load(Path('setting.yaml').read_text())['date format']
        
        for s_date_unit in s_date_notation:
            s_date_unit = convert.s_to_european(s_numeral_system, s_date_unit)
            s_date_unit = s_date_unit.lstrip('0')
            if s_date_unit == '':
                s_date_unit = '0'
        if s_date_format == 'day-month-year':
            s_day, s_month, s_year = s_date_notation
        elif s_date_format == 'month-day-year':
            s_month, s_day, s_year = s_date_notation
        else:
            s_year, s_month, s_day = s_date_notation
        s_date_notation = s_year + '-' + s_month + '-' + s_day

        for s_time_unit in s_time_notation:
            s_time_unit = convert.s_to_european(s_numeral_system, s_time_unit)
            s_time_unit = s_time_unit.lstrip('0')
            if s_time_unit == '':
                s_time_unit = '0'
        s_hour, s_minute, s_second = s_time_notation
        s_time_notation = s_hour + ':' + s_minute + ':' + s_second

        s_notation = s_date_notation + ' ' + s_time_notation
    else:
        if get.s_numeral_system(s_notation) != 'georgian_nuskhuri':
            s_notation = s_notation.upper()
        s_notation = s_notation.replace('؀', '#')
        s_notation = s_notation.replace('𑂽', '#')
        s_notation = s_notation.replace('௺', '#')
        s_notation = s_notation.replace('𑿩', '#')
        s_notation = s_notation.replace('⁄', '/')
        s_notation = s_notation.replace('٪', '%')
        s_notation = s_notation.replace('𖬻', '%')
        s_notation = s_notation.replace(',', '.')
        s_notation = s_notation.replace('·', '.')
        s_notation = s_notation.replace("'", '.')
        s_notation = s_notation.replace('٫', '.')
        s_notation = s_notation.replace('−', '-')

        s_numeral_system = get.s_numeral_system(s_notation)
        if s_numeral_system == '':
            return ''
        s_notation = convert.s_to_european(s_numeral_system, s_notation)
    return s_notation
