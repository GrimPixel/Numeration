from importlib import import_module


def s_to_european(s_numeral_system, s_notation):
    numeral_system_module = import_module('library.numeral_system.' + s_numeral_system)
    return numeral_system_module.s_convert_to_principal_numeral_system(s_notation)


def s_from_european(s_numeral_system, s_notation):
    numeral_system_module = import_module('library.numeral_system.' + s_numeral_system)
    return numeral_system_module.s_convert_from_principal_numeral_system(s_notation)
