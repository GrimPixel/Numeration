def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/eng.yaml').read_text())
    ls_script = d_setting['ls script']
    s_writing = ''

    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/eng.yaml').read_text())['d '+s_the_script]
        ls_digit = d_lexicon['ls digits']
        if d_setting['i “zero” / “nought”'] != 0:
            ls_digit[0] = d_lexicon['s nought']
        s_the_writing = ''

        if d_setting['s scale'] == 'south asian':
            ls_archmagnitude = d_lexicon['ls south asian archmagnitudes']
            li_digit_grouping = [3, 2]
        else:
            i_digit_grouping = 3
            if d_setting['s scale'] == 'short':
                ls_archmagnitude = d_lexicon['ls short-scale archmagnitudes']
            elif d_setting['s scale'] == 'long':
                ls_archmagnitude = d_lexicon['ls long-scale archmagnitudes']
        
        def s_write_integer(s_notation):
            def s_determine_initials():
                if d_setting['s scale'] == 'south asian':
                    if len(s_notation) <= li_digit_grouping[0]:
                        i_the_archmagnitude = 0
                        i_the_magnitude = len(s_notation) - 1
                    else:
                        i_the_archmagnitude = (len(s_notation) - 1 - li_digit_grouping[0]) // li_digit_grouping[1] + 1
                        i_the_magnitude = (len(s_notation) - 1 - li_digit_grouping[0]) % li_digit_grouping[1]
                else:
                    i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
                    i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
                return [i_the_archmagnitude, i_the_magnitude]

            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    if s_the_digit == '1':
                        if d_setting['i “one hundred”, “one” archmagnitude / “a hundred”, “an” archmagnitude'] != 0 and i_the_archmagnitude > 0 and int(s_the_digit_group) == 1:
                            return d_lexicon['s a'] + ' '
                    return ls_digit[int(s_the_digit)] + ' '

                def s_write_ten_and_unit():
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        return d_lexicon['ls ten to nineteen'][int(s_the_digit_group[-1])] + ' '
                    if s_the_digit_group[-1] != '0':
                        return d_lexicon['ls tens'][int(s_the_digit)] + '-' + ls_digit[int(s_the_digit_group[-1])] + ' '
                    return d_lexicon['ls tens'][int(s_the_digit)] + ' '

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    if s_the_digit == '1':
                        if d_setting['i “one hundred”, “one” archmagnitude / “a hundred”, “an” archmagnitude'] != 0:
                            return d_lexicon['s a'] + ' ' + d_lexicon['s hundred'] + ' '
                    return ls_digit[int(s_the_digit)] + ' ' + d_lexicon['s hundred'] + ' '

                s_writing = ''
                if i_the_magnitude == 0:
                    s_writing += s_write_unit()
                elif i_the_magnitude == 1:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude == 2:
                    s_writing += s_write_hundred()
                    if d_setting['i _ / “and” between hundred and ten'] != 0 and s_the_digit_group[-2] != '0':
                        s_writing += d_lexicon['s and'] + ' '
                if d_setting['i _ / “and” for places of “0”'] != 0 and s_the_digit == '0' and i_the_magnitude > 0 and s_the_digit_group[-i_the_magnitude] != '0':
                    s_writing += d_lexicon['s and'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if i_the_archmagnitude == 0 or int(s_the_digit_group) == 0:
                    return ''
                return ls_archmagnitude[i_the_archmagnitude] + ' '

            s_writing = ''
            i_the_archmagnitude, i_the_magnitude = s_determine_initials()
            while i_the_archmagnitude >= 0:
                if d_setting['s scale'] == 'south asian':
                    if i_the_archmagnitude == 0:
                        s_the_digit_group_end = li_digit_grouping[0] * i_the_archmagnitude + 1
                        s_the_digit_group_start = s_the_digit_group_end + li_digit_grouping[0]
                    else:
                        s_the_digit_group_end = li_digit_grouping[1] * (i_the_archmagnitude - 1) + li_digit_grouping[0] + 1
                        s_the_digit_group_start = s_the_digit_group_end + li_digit_grouping[0]
                else:
                    s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                    s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                while i_the_magnitude >= 0:
                    s_the_digit = s_the_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude == 1 and s_the_digit != '0':
                        i_the_magnitude -= 2
                    else:
                        i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                if isinstance(i_digit_grouping, int):
                    i_the_magnitude = i_digit_grouping - 1
                else:
                    if i_the_archmagnitude == 0:
                        i_the_magnitude = i_digit_grouping[0] - 1
                    else:
                        i_the_magnitude = i_digit_grouping[1] - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            if len(s_notation) < 3:
                if int(s_notation) < 10:
                    return d_lexicon['ls ordinal digits'][int(s_notation)]
                if int(s_notation) < 20:
                    return d_lexicon['ls ordinal ten to nineteen'][int(s_notation[-1])]
                if s_notation.endswith('0'):
                    return d_lexicon['ls ordinal tens'][int(s_notation[-2])]
                return d_lexicon['ls tens'][int(s_notation[-2])] + '-' + d_lexicon['ls ordinal digits'][int(s_notation[-1])]
            if s_notation.endswith('00'):
                return s_write_integer(s_notation) + d_lexicon['s th']
            return s_write_integer(s_notation[:-2] + '00') + ' ' + s_write_ordinal_number(s_notation[-2:])

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            if d_setting['i “oh” as nominal 0 / “zero” as nominal 0'] == 0:
                ls_digit[0] = d_lexicon['s oh']
            s_nominal_writing = ''
            for s_character in s_notation:
                s_nominal_writing += ls_digit[int(s_character)] + '-'
            return s_nominal_writing.removesuffix('-')

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            s_numerator_writing = s_write_integer(s_numerator_notation)
            if s_denominator_notation == '2':
                if s_numerator_notation == '1':
                    s_denominator_writing = d_lexicon['s half']
                    if d_setting['i “one half”, “one quarter” / “a half”, “a quarter”'] != 0:
                        s_numerator_writing = d_lexicon['s a']
                else:
                    s_denominator_writing = d_lexicon['s halves']
            elif s_denominator_notation == '4':
                s_denominator_writing = d_lexicon['s quarter']
                if s_numerator_notation == '1':
                    if d_setting['i “one half”, “one quarter” / “a half”, “a quarter”'] != 0:
                        s_numerator_writing = d_lexicon['s a']
                else:
                    s_denominator_writing += d_lexicon['s s']
            else:
                s_denominator_writing = s_write_ordinal_number(s_denominator_notation)
                if s_numerator_notation != '1':
                    s_denominator_writing += d_lexicon['s s']
            return s_numerator_writing + '-' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + d_lexicon['s and'] + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += ls_digit[int(s_character)] + '-'
            return s_integer_writing + ' ' + d_lexicon['s point'] + ' ' + s_decimal_writing.removesuffix('-')

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                if d_setting['i “negative” / “minus”'] == 0:
                    s_exponent_writing = d_lexicon['s negative'] + ' '
                else:
                    s_exponent_writing = d_lexicon['s minus'] + ' '
                s_exponent_notation = s_exponent_notation.removeprefix('-')
            else:
                s_exponent_writing = ''
            if d_setting['i cardinal exponent / ordinal exponent'] == 0:
                s_exponent_writing += s_write_integer(s_exponent_notation)
            else:
                s_exponent_writing += s_write_ordinal_number(s_exponent_notation)
            return s_coefficient_writing + ' ' + d_lexicon['s times'] + ' ' + d_lexicon['ls ten to nineteen'][0] + ' ' + d_lexicon['s to'] + ' ' + d_lexicon['s the'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                s_the_digit_group = [s_year_notation[:-2], s_year_notation[-2:]]
                if s_the_digit_group[0].endswith('00'):
                    s_year_writing = s_write_integer(s_the_digit_group[-2]) + ' ' + d_lexicon['s hundred'] + ' ' + s_write_integer(s_the_digit_group[1])
                elif s_the_digit_group[0].endswith('0') and s_the_digit_group[-1].startswith('0'):
                    s_year_writing = s_write_integer(s_year_notation)
                else:
                    s_year_writing = s_write_integer(s_the_digit_group[-2]) + ' ' + s_write_integer(s_the_digit_group[-1])
                s_month_writing = d_lexicon['s months'][int(s_month_notation)]
                s_day_writing = d_lexicon['s the'] + ' ' + s_write_ordinal_number(s_day_notation)

                if d_setting['i day of month / month day'] == 0:
                    s_date_writing = s_day_writing + ' ' + d_lexicon['s of'] + ' ' + s_month_writing
                else:
                    s_date_writing = s_month_writing + ' ' + s_day_writing
                s_date_writing += ',' + ' ' + s_year_writing
                return s_date_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')
                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        s_hour_writing += s_write_integer(s_hour_notation) + ' ' + d_lexicon['s o’clock']
                        if s_minute_notation == '0':
                            if d_setting['i _ / “sharp”'] != 0:
                                s_hour_writing += s_write_integer(s_hour_notation) + ' ' + d_lexicon['s sharp']
                    elif s_minute_notation == '30':
                        if d_setting['i “thirty” / “half past”'] != 0:
                            s_hour_writing += d_lexicon['s half'] + ' ' + d_lexicon['s past'] + ' ' + s_write_integer(s_hour_notation)
                    elif s_minute_notation in ['15', '45']:
                        if d_setting['i “fifteen, “fourty-five” / “one quarter past”, “one quarter to”'] != 0:
                            if d_setting['i “one half”, “one quarter” / “a half”, “a quarter”'] == 0:
                                s_hour_writing += ls_digit[1]
                            else:
                                s_hour_writing += d_lexicon['s a']
                            s_hour_writing += ' ' + d_lexicon['s quarter'] + ' '
                        if s_minute_notation == '15':
                            s_hour_writing += d_lexicon['s past'] + ' ' + s_write_integer(s_hour_notation)
                        else:
                            if s_hour_notation == '23':
                                s_hour_notation = '-1'
                            s_hour_writing += d_lexicon['s to'] + ' ' + s_write_integer(str(int(s_hour_notation) + 1))
                    else:
                        if d_setting['i hour and minute / “past”, “to”'] == 0:
                            s_minute_writing += s_write_integer(s_minute_notation)
                            s_hour_writing += s_write_integer(s_hour_notation)
                        else:
                            if int(s_minute_notation) < 30:
                                s_hour_writing += s_write_integer(s_minute_notation) + ' ' + d_lexicon['s past'] + ' ' + s_write_integer(s_hour_notation)
                            else:
                                if s_hour_notation == '23':
                                    s_hour_notation = '-1'
                                s_hour_writing += s_write_integer(str(60 - int(s_minute_notation))) + ' ' + d_lexicon['s to'] + ' ' + s_write_integer(str(int(s_hour_notation) + 1))
                else:
                    s_hour_writing += s_write_integer(s_hour_notation)
                    if int(s_minute_notation) < 10:
                        s_minute_writing += d_lexicon['s oh'] + '-' + s_write_integer(s_minute_notation)
                    else:
                        s_minute_writing += s_write_integer(s_minute_notation)
                    if int(s_second_notation) < 10:
                        s_second_writing += d_lexicon['s oh'] + '-' + s_write_integer(s_second_notation)
                    else:
                        s_second_writing += s_write_integer(s_second_notation)

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += ' ' + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()
        
        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing += ' ' + d_lexicon['s percent']
        if b_is_negative:
            s_notation = '-' + s_notation
            if d_setting['i “negative” / “minus”'] == 0:
                s_the_writing = d_lexicon['s negative'] + ' ' + s_the_writing
            else:
                s_the_writing = d_lexicon['s minus'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
