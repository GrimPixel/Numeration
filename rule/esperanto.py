def do(notation, notation_type, setting_identifier, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['esperanto']
    if is_text_to_speech:
        script = ['Latn']
    else:
        script = setting['script']
    final_writing = ''
    for x in script:
        current_script = yaml.load(open('rule/library/script decode.yaml'))[x]
        common_lexicon = yaml.load(open('rule/lexicon/esperanto.yaml'))[current_script]['common']

        _space = common_lexicon['_space']
        if setting['archmagnitude – preferred or alternative'] == 0:
            archmagnitude = common_lexicon['integer']['archmagnitude']['preferred']
        else:
            archmagnitude = common_lexicon['integer']['archmagnitude']['alternative']

        def write_integer(integer_notation):
            def determine_initial_focused():
                focused_archmagnitude = (len(integer_notation) - 1) // setting['digit grouping']
                focused_magnitude = (len(integer_notation) - 1) % setting['digit grouping']
                return [focused_archmagnitude, focused_magnitude]

            def renew_focused_magnitude():
                return setting['digit grouping'] - 1

            def write_digit_and_magnitude():
                if current_digit == '0' and integer_notation != '0':
                    return ''
                if current_digit == '1':
                    if focused_magnitude == 0:
                        if focused_archmagnitude > 0 and int(current_digit_group) == 1:
                            return ''
                    else:
                        return common_lexicon['integer']['magnitude'][focused_magnitude] + _space
                return common_lexicon['integer']['digit'][int(current_digit)] + \
                       common_lexicon['integer']['magnitude'][focused_magnitude] + _space

            def write_archmagnitude():
                if focused_archmagnitude == 0 or int(current_digit_group) == 0:
                    return ''
                if focused_archmagnitude == 1 or int(current_digit_group) == 1:
                    return archmagnitude[focused_archmagnitude] + _space
                return archmagnitude[focused_archmagnitude] + common_lexicon['plural'] + _space

            writing = ''
            focused_archmagnitude, focused_magnitude = determine_initial_focused()
            while focused_archmagnitude >= 0:
                current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                      setting['digit grouping'])
                while focused_magnitude >= 0:
                    current_digit = current_digit_group[-focused_magnitude - 1]
                    writing += write_digit_and_magnitude()
                    focused_magnitude -= 1
                focused_magnitude = renew_focused_magnitude()
                writing += write_archmagnitude()
                focused_archmagnitude -= 1
            return writing[:-1]

        def write_fraction(fraction_notation):
            numerator, denominator = fraction_notation.split('/')
            numerator = write_integer(numerator)
            denominator = write_integer(denominator).replace(_space, common_lexicon['_hyphen'])
            if denominator[-1] == 'o':
                denominator = denominator[:-1] + common_lexicon['fraction']
            elif denominator[-2:] == 'oj':
                denominator = denominator[:-2] + common_lexicon['fraction']
            else:
                denominator += common_lexicon['fraction']
            if int(fraction_notation[0]) > 1:
                denominator += common_lexicon['plural']
            return numerator + _space + denominator

        def output_math_numeral(math_numeral_notation):
            math_numeral_lexicon = yaml.load(open('rule/lexicon/esperanto.yaml'))[current_script]['math numeral']

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                return integer + _space + common_lexicon['conjunction'] + _space + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                integer = write_integer(integer)
                decimal = write.consecutive_digit(decimal, common_lexicon['integer']['digit'],
                                                  common_lexicon['_hyphen'])
                return integer + _space + math_numeral_lexicon['decimal'] + _space + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)
                multiplier = multiplier.replace(_space, common_lexicon['_hyphen'])
                if check.is_negative(exponent):
                    exponent = common_lexicon['negative'] + _space + write_integer(exponent[1:])
                else:
                    exponent = write_integer(exponent)
                multiply = math_numeral_lexicon['scientific notation']['multiply']
                high = math_numeral_lexicon['scientific notation']['high']
                return multiplier + multiply + _space + \
                       common_lexicon['integer']['magnitude'][1] + _space + \
                       high + _space + exponent

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                if setting['percent – preferred or alternative'] == 0:
                    writing += _space + common_lexicon['percentage']['preferred']
                else:
                    writing += _space + common_lexicon['percentage']['alternative']
                if float(math_numeral_notation) != 1.0:
                    writing += common_lexicon['plural']
            if is_negative:
                writing = common_lexicon['negative'] + _space + writing
            return writing

        def write_ordinal(ordinal_notation):
            writing = write_integer(ordinal_notation)
            if writing[-2:] == 'oj':
                return writing[:-2] + common_lexicon['ordinal'] + \
                       writing[-1]
            elif writing[-1] == 'o':
                return writing[:-1] + common_lexicon['ordinal']
            return writing + common_lexicon['ordinal']

        def output_ordinal_number(ordinal_notation):
            return write_ordinal(ordinal_notation)
        
        def output_nominal_number(nominal_notation):
            return write.consecutive_digit(nominal_notation, common_lexicon['integer']['digit'], 
                                           common_lexicon['_hyphen'])

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/esperanto.yaml'))[current_script]['date and time']

            def write_date():
                year_notation, month_notation, day_notation = date_and_time_notation[0]
                if 0 <= int(year_notation) <= 100:
                    year_writing = date_and_time_lexicon['article'] + _space + \
                                   date_and_time_lexicon['year'] + _space + write_integer(year_notation)
                else:
                    year_writing = write_integer(year_notation)
                month_writing = date_and_time_lexicon['preposition']['de'] + _space + \
                                date_and_time_lexicon['month'][int(month_notation)]
                day_writing = date_and_time_lexicon['article'] + _space + \
                              write_ordinal(day_notation).replace(_space, '')
                _comma = date_and_time_lexicon['_comma']
                return day_writing + _space + month_writing + _comma + _space + year_writing

            def write_time():
                def write_minute_or_second(minute_or_second_notation, unit):
                    minute_or_second = write_integer(minute_or_second_notation)
                    if setting['time – without or with time unit'] != 0:
                        minute_or_second += _space + unit
                        if int(second_notation) > 1:
                            minute_or_second += common_lexicon['plural']
                    return minute_or_second

                def write_exact_hour():
                    return date_and_time_lexicon['article'] + _space + write_ordinal(hour_notation)

                def write_quarter(hour_preposition):
                    fraction_suffix = common_lexicon['fraction']['suffix']
                    hour = common_lexicon['integer']['digit'][4] + fraction_suffix + _space + \
                        hour_preposition + _space
                    if hour_preposition == date_and_time_lexicon['preposition']['antaŭ']:
                        if hour_notation == '23':
                            return hour + write_integer('0')
                        return hour + write_integer(str(int(hour_notation) + 1))
                    return hour + write_integer(str(int(hour_notation)))

                def write_half():
                    fraction_suffix = common_lexicon['fraction']['suffix']
                    hour_writing = ''
                    if setting['minute 30 expressed by half hour – half past or half to'] == 0:
                        hour_writing += common_lexicon['integer']['digit'][2] + fraction_suffix + _space + \
                                        date_and_time_lexicon['preposition']['post'] + _space + \
                                        date_and_time_lexicon['article'] + _space + \
                                        write_ordinal(hour_notation).replace(_space, '')
                    else:
                        hour_writing += common_lexicon['integer']['digit'][2] + fraction_suffix + _space + \
                                        date_and_time_lexicon['preposition']['antaŭ'] + _space + \
                                        date_and_time_lexicon['article'] + _space
                        if hour_notation == '23':
                            hour_writing += write_ordinal('0')
                        else:
                            hour_writing += write_ordinal(str(int(hour_notation) + 1))
                    return hour_writing

                def write_regular_hour_and_minute():
                    hour_writing = ''
                    minute_writing = ''
                    if setting['time – without or with preposition'] == 0:
                        hour_writing += date_and_time_lexicon['article'] + _space + \
                                        write_ordinal(hour_notation).replace(_space, '')
                        minute_writing += write_minute_or_second(minute_notation,
                                                                 date_and_time_lexicon['minute'])
                    else:
                        if int(minute_notation) < 30:
                            hour_writing += date_and_time_lexicon['preposition']['post'] + _space + \
                                            date_and_time_lexicon['article'] + _space + \
                                            write_ordinal(hour_notation).replace(_space, '')
                            minute_writing += write_minute_or_second(minute_notation,
                                                                     date_and_time_lexicon['minute'])
                        else:
                            hour_writing += date_and_time_lexicon['preposition']['antaŭ'] + _space
                            minute_writing += write_minute_or_second(str(60 - int(minute_notation)),
                                                                     date_and_time_lexicon['minute'])
                            if hour_notation == '23':
                                hour_writing += date_and_time_lexicon['article'] + _space + \
                                                write_ordinal('0').replace(_space, '')
                            else:
                                hour_writing += date_and_time_lexicon['article'] + _space + \
                                                write_ordinal(str(int(hour_notation) + 1)).replace(_space, '')
                    return [hour_writing, minute_writing]

                def write_units():
                    hour_writing = ''
                    minute_writing = ''
                    second_writing = ''
                    if second_notation == '0':
                        if minute_notation == '0':
                            hour_writing += write_exact_hour()
                            return [hour_writing, minute_writing, second_writing]
                        elif minute_notation == '15' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            write_quarter(date_and_time_lexicon['preposition']['post'])
                        elif minute_notation == '30' and setting['minute 30 – number or half hour'] != 0:
                            hour_writing += write_half()
                        elif minute_notation == '45' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            write_quarter(date_and_time_lexicon['preposition']['antaŭ'])
                        else:
                            hour_writing, minute_writing = write_regular_hour_and_minute()
                    else:
                        hour_writing += date_and_time_lexicon['article'] + _space + write_ordinal(hour_notation)
                        minute_writing += write_minute_or_second(minute_notation, date_and_time_lexicon['minute'])
                        second_writing += write_minute_or_second(second_notation, date_and_time_lexicon['second'])
                    if setting['time – without or with time unit'] != 0:
                        hour_writing += _space + date_and_time_lexicon['hour']
                    return [hour_writing, minute_writing, second_writing]

                hour_notation, minute_notation, second_notation = date_and_time_notation[1]
                hour_writing, minute_writing, second_writing = write_units()
                time_writing = hour_writing
                if minute_writing != '':
                    if setting['time – without or with preposition'] != 0 and second_writing == '':
                        time_writing = minute_writing + _space + time_writing
                    else:
                        if setting['time – without or with conjunction'] == 0:
                            time_writing += _space + minute_writing
                            if second_writing != '':
                                time_writing += _space + second_writing
                        else:
                            time_writing += _space + common_lexicon['conjunction'] + _space + minute_writing
                            if second_writing != '':
                                time_writing += _space + common_lexicon['conjunction'] + _space + second_writing
                return time_writing
            if is_text_to_speech:
                return write_date() + '. ' + write_time()
            return write_date() + '\n' + write_time()

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
    return final_writing
