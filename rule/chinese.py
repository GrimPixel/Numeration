def do(notation, notation_type, setting_identifier, individual_language_name, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['chinese']
    if is_text_to_speech:
        script = ['Hant']
    else:
        script = setting[individual_language_name]['script']
    final_writing = ''
    for x in script:
        current_script_code = x
        if current_script_code.count('.') == 1:
            current_script_code, script_variety = current_script_code.split('.')
            current_script_name = yaml.load(open('rule/library/script decode.yaml'))[current_script_code] + ' ' + script_variety
        else:
            current_script_name = yaml.load(open('rule/library/script decode.yaml'))[current_script_code]

        common_lexicon = yaml.load(open('rule/lexicon/chinese.yaml'))[current_script_name]['common']

        if current_script_name == 'han simplified variant' or current_script_name == 'han traditional variant':
            if setting['digit – common or formal'] == 0:
                digit = common_lexicon['integer']['digit']['common']['prevalent']
                if is_text_to_speech:
                    digit[0] = common_lexicon['integer']['digit']['formal'][0]
                magnitude = common_lexicon['integer']['magnitude']['common']
            else:
                digit = common_lexicon['integer']['digit']['formal']
                magnitude = common_lexicon['integer']['magnitude']['formal']
        else:
            digit = common_lexicon['integer']['digit']['prevalent']
            magnitude = common_lexicon['integer']['magnitude']

        if setting['scale'] == 'short':
            magnitude += common_lexicon['integer']['archmagnitude'][1:]
        else:
            archmagnitude = common_lexicon['integer']['archmagnitude']

        if current_script_name == 'latin jyutping':
            syllable_separator = common_lexicon['_space']
            word_separator = common_lexicon['_space']
        elif current_script_name == 'latin pinyin':
            _apostrophe = common_lexicon['_apostrophe']
            syllable_separator = ''
            word_separator = common_lexicon['_space']
        else:
            syllable_separator = ''
            word_separator = ''

        def write_apostrophe(writing):
            if current_script_name == 'latin pinyin':
                writing = writing.replace(digit[2], _apostrophe + digit[2])
                if writing[0] == _apostrophe:
                    writing = writing[1:]
            return writing

        def write_integer(integer_notation):
            def renew_focused_magnitude():
                return setting['digit grouping'] - 1

            def write_digit(current_digit, current_integer_notation, focused_archmagnitude, focused_magnitude):
                if current_digit == '0' and notation != '0':
                    return ''
                if current_digit == '1' and focused_magnitude == 1 and \
                        (len(current_integer_notation) == 2 or setting[
                            'one in 10 – omit only at initial or always'] != 0):
                    return ''
                if current_digit == '2':
                    if focused_magnitude == 2 and setting['200 – common or alternative'] == 1 or \
                            focused_magnitude == 3 and setting['2000 – common or alternative'] == 1:
                        return common_lexicon['integer']['digit']['alternative'] + syllable_separator
                    if focused_magnitude == 0 and focused_archmagnitude > 0 and \
                            setting['2 archmagnitude – common or alternative'] == 1:
                        return common_lexicon['integer']['digit']['alternative'] + syllable_separator
                return digit[int(current_digit)] + syllable_separator

            def write_magnitude(current_integer_notation, focused_archmagnitude, focused_magnitude):
                if focused_archmagnitude == 0 and focused_magnitude > 0 and \
                        setting['last magnitude – keep or omit'] != 0:
                    if int(current_integer_notation[-focused_magnitude:]) == 0:
                        return ''
                return magnitude[focused_magnitude] + word_separator

            def write_short_scale_integer(current_integer_notation):
                def write_magnitude_conjunction():
                    next_digit = current_integer_notation[-focused_magnitude]
                    if current_digit == '0' and focused_magnitude > 0 and next_digit != '0':
                        return common_lexicon['integer']['conjunction'] + word_separator
                    return ''

                def write_digit_and_magnitude():
                    digit_and_magnitude_writing = ''
                    if individual_language_name == 'yue chinese' and \
                            setting['yue chinese']['tens – common or colloquial'] == 1 and \
                            focused_magnitude == 1 and 2 <= int(current_digit) <= 4:
                        digit_and_magnitude_writing += common_lexicon['integer']['tens'][int(current_digit)] + \
                                                       word_separator
                    else:
                        digit_and_magnitude_writing += write_digit(current_digit, current_integer_notation, 0,
                                                                   focused_magnitude)
                        if focused_magnitude > 0 and current_integer_notation[-focused_magnitude - 1] != '0':
                            if focused_magnitude == 17 and current_script_name == 'latin pinyin':
                                digit_and_magnitude_writing += _apostrophe
                            digit_and_magnitude_writing += write_magnitude(current_integer_notation, 0,
                                                                           focused_magnitude)
                    return digit_and_magnitude_writing

                writing = ''
                focused_magnitude = len(current_integer_notation) - 1
                while focused_magnitude >= 0:
                    current_digit = current_integer_notation[-focused_magnitude - 1]
                    writing += write_digit_and_magnitude() + write_magnitude_conjunction()
                    focused_magnitude -= 1
                return writing.strip()

            def write_myriad_scale_integer(current_integer_notation):
                def determine_initial_focused():
                    focused_archmagnitude = (len(current_integer_notation) - 1) // setting['digit grouping']
                    focused_magnitude = (len(current_integer_notation) - 1) % setting['digit grouping']
                    return [focused_archmagnitude, focused_magnitude]

                def write_archmagnitude():
                    if int(current_digit_group) != 0 and focused_archmagnitude > 0:
                        return archmagnitude[focused_archmagnitude] + word_separator
                    return ''

                def write_archmagnitude_conjunction():
                    if focused_archmagnitude >= 1:
                        next_digit_group = get.digit_group(notation, focused_archmagnitude - 1,
                                                           setting['digit grouping'])
                        if focused_magnitude == 0 and int(current_digit_group) == 0 and int(next_digit_group) != 0:
                            return common_lexicon['integer']['conjunction'] + word_separator
                    return ''

                writing = ''
                focused_archmagnitude, focused_magnitude = determine_initial_focused()
                while focused_archmagnitude >= 0:
                    current_digit_group = get.digit_group(current_integer_notation, focused_archmagnitude,
                                                          setting['digit grouping'])
                    writing += write_short_scale_integer(current_digit_group)
                    if int(current_digit_group) > 0:
                        writing += word_separator + write_archmagnitude()
                    writing += write_archmagnitude_conjunction()
                    focused_magnitude = renew_focused_magnitude()
                    focused_archmagnitude -= 1
                return writing.strip()

            def write_middle_scale_integer():
                def determine_initial_focused():
                    return [(len(integer_notation) - 1) // (setting['digit grouping'] * 2),
                            (len(integer_notation) - 1) % setting['digit grouping']]

                writing = ''
                focused_archmagnitude, focused_magnitude = determine_initial_focused()
                middle_scale_archmagnitude = archmagnitude[:1] + archmagnitude[2:]
                while focused_archmagnitude >= 0:
                    current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                          setting['digit grouping'] * 2)
                    writing += write_myriad_scale_integer(current_digit_group)
                    if int(current_digit_group) > 0:
                        writing += word_separator + middle_scale_archmagnitude[focused_archmagnitude]
                        if focused_archmagnitude > 0:
                            writing += word_separator
                    focused_archmagnitude -= 1
                return writing.strip()

            if setting['scale'] == 'short':
                return write_short_scale_integer(integer_notation)
            elif setting['scale'] == 'myriad':
                return write_myriad_scale_integer(integer_notation)
            elif setting['scale'] == 'middle':
                return write_middle_scale_integer()

        def write_ordinal(ordinal_notation):
            return yaml.load(open('rule/lexicon/chinese.yaml'))[current_script_name]['ordinal'] + \
                   word_separator + write_integer(ordinal_notation)

        def output_math_numeral(math_numeral_notation):
            math_numeral_lexicon = yaml.load(open('rule/lexicon/chinese.yaml'))[current_script_name]['math numeral']

            def write_fraction(fraction_notation):
                numerator, denominator = fraction_notation.split('/')
                numerator = write_integer(numerator)
                denominator = write_integer(denominator) + word_separator + math_numeral_lexicon['fraction']['word']
                return denominator + word_separator + \
                       math_numeral_lexicon['fraction']['particle'] + word_separator + numerator

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                return integer + word_separator + math_numeral_lexicon['mixed number'] + word_separator + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                integer = write_integer(integer)
                decimal = write.consecutive_digit(decimal, digit, syllable_separator)
                decimal = write_apostrophe(decimal)
                return integer + word_separator + math_numeral_lexicon['decimal'] + word_separator + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)

                multiplied_by = math_numeral_lexicon['scientific notation']['multiply']
                if setting['multiplication – full or shortened'] == 0:
                    preposition = math_numeral_lexicon['scientific notation']['preposition']
                    multiplied_by += word_separator + preposition

                particle = math_numeral_lexicon['scientific notation']['particle']
                if current_script_name == 'han simplified variant' or current_script_name == 'han traditional variant':
                    if individual_language_name == 'mandarin chinese':
                        particle = particle['mandarin']
                    else:
                        particle = particle['yue']
                else:
                    particle = particle

                exponent_is_negative = check.is_negative(exponent)
                if exponent_is_negative:
                    exponent = common_lexicon['negative'] + word_separator + write_integer(exponent[1:])
                else:
                    exponent = write_integer(exponent)

                return multiplier + word_separator + multiplied_by + word_separator + \
                       magnitude[1] + word_separator + particle + word_separator + \
                       exponent + word_separator + math_numeral_lexicon['scientific notation']['power']

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed_number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                writing = magnitude[2] + syllable_separator + \
                          math_numeral_lexicon['fraction']['word'] + syllable_separator + \
                          math_numeral_lexicon['fraction']['particle'] + word_separator + writing
            if is_negative:
                writing = common_lexicon['negative'] + word_separator + writing
            return writing

        def output_ordinal_number(ordinal_notation):
            return write_ordinal(ordinal_notation)

        def output_nominal_number(nominal_notation):
            if individual_language_name == 'mandarin chinese':
                if setting['mandarin chinese']['nominal 1 – change or keep'] == 0:
                    digit[1] = yaml.load(open('rule/lexicon/chinese.yaml'))[current_script_name]['common']['nominal']
            return write.consecutive_digit(nominal_notation, digit, word_separator)

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/chinese.yaml'))[current_script_name]['date and time']
            
            def write_date():
                def write_year():
                    if year_notation == '1':
                        year_writing = date_and_time_lexicon['year']['first']
                    else:
                        year_writing = write.consecutive_digit(year_notation, digit, syllable_separator)
                        year_writing = write_apostrophe(year_writing)
                    return year_writing + word_separator + date_and_time_lexicon['year']['suffix']

                def write_common_month():
                    return write_integer(month_notation) + word_separator + \
                           date_and_time_lexicon['month']['suffix']

                def write_common_day():
                    if setting['day – common or colloquial'] == 0:
                        return write_integer(month_notation) + word_separator + \
                               date_and_time_lexicon['day']['common']
                    else:
                        return write_integer(month_notation) + word_separator + \
                               date_and_time_lexicon['day']['colloquial']

                def write_agricultural_month():
                    if month_notation == '1':
                        return date_and_time_lexicon['month']['agricultural']['1'] + word_separator + \
                               date_and_time_lexicon['month']['suffix']
                    elif month_notation == '12':
                        return date_and_time_lexicon['month']['agricultural']['12'] + word_separator + \
                               date_and_time_lexicon['month']['suffix']
                    else:
                        return write_integer(month_notation) + word_separator + \
                               date_and_time_lexicon['month']['suffix']

                def write_agricultural_day():
                    if int(day_notation) < 10:
                        return date_and_time_lexicon['day']['agricultural'] + write_integer(day_notation)
                    return write_integer(day_notation)

                year_notation, month_notation, day_notation = date_and_time_notation[0]
                year_writing = write_year()
                if setting['calendar – common or agricultural'] == 0:
                    month_writing = write_common_month()
                    day_writing = write_common_day()
                else:
                    month_writing = write_agricultural_month()
                    day_writing = write_agricultural_day()
                date_writing = year_writing + word_separator + month_writing + word_separator + day_writing
                return date_writing

            def write_time():
                def write_hour():
                    def write_latin_hour_2_alternative():
                        return common_lexicon['integer']['digit']['alternative']

                    def write_han_hour_2_alternative():
                        return common_lexicon['integer']['digit']['common']['alternative']

                    hour_writing = ''
                    if hour_notation == '2' and \
                            setting['hour 2 – common or alternative'] == 1 and setting['hour – common or formal'] == 0:
                        if current_script_name == 'han simplified variant' or \
                                current_script_name == 'han traditional variant':
                            hour_writing += write_han_hour_2_alternative() + word_separator
                        elif current_script_name == 'latin jyutping' or current_script_name == 'latin pinyin':
                            hour_writing += write_latin_hour_2_alternative() + word_separator
                    else:
                        hour_writing += write_integer(hour_notation) + word_separator
                    hour = date_and_time_lexicon['hour']
                    if setting['hour – common or formal'] == 0:
                        hour_writing += hour['common']
                    else:
                        hour_writing += hour['formal']
                    return hour_writing

                def write_minute():
                    def write_yue_chinese_colloquial_tens():
                        return write_integer(str(int(minute_notation) // 5))

                    def write_han_minute_2_alternative():
                        minute = date_and_time_lexicon['minute']
                        return common_lexicon['integer']['digit']['common']['alternative'] + word_separator + minute

                    def write_latin_minute_2_alternative():
                        minute = date_and_time_lexicon['minute']
                        return common_lexicon['integer']['digit']['alternative'] + word_separator + minute

                    if minute_notation == '0':
                        return ''
                    if individual_language_name == 'yue_chinese':
                        if setting['yue chinese']['minute – number or hour division'] != 0 and \
                                int(minute_notation) % 5 == 0 and second_notation == '0':
                            return write_yue_chinese_colloquial_tens()
                    if minute_notation == '2' and setting['minute 2 – common or alternative'] != 0:
                        if current_script_name == 'han simplified variant' or \
                                current_script_name == 'han traditional variant':
                            return write_han_minute_2_alternative()
                        elif current_script_name == 'latin jyutping' or current_script_name == 'latin pinyin':
                            return write_latin_minute_2_alternative()
                    else:
                        if individual_language_name == 'mandarin chinese' and \
                                setting['mandarin chinese']['last minute and second unit – keep or omit'] != 0 and \
                                second_notation == '0':
                            if int(minute_notation) < 10:
                                return common_lexicon['integer']['conjunction'] + word_separator + \
                                       digit[int(minute_notation)]
                            return write_integer(minute_notation)
                    return write_integer(minute_notation) + word_separator + date_and_time_lexicon['minute']

                def write_second():
                    def write_han_second_2_alternative():
                        second = date_and_time_lexicon['second']
                        return common_lexicon['integer']['digit']['common']['alternative'] + word_separator + second

                    def write_latin_second_2_alternative():
                        second = date_and_time_lexicon['second']
                        return common_lexicon['integer']['digit']['alternative'] + word_separator + second

                    if second_notation == '2' and setting['second 2 – common or alternative'] != 0:
                        if current_script_name == 'han simplified variant' or \
                                current_script_name == 'han traditional variant':
                            return write_han_second_2_alternative()
                        elif current_script_name == 'latin jyutping' or current_script_name == 'latin pinyin':
                            return write_latin_second_2_alternative()
                    else:
                        if individual_language_name == 'mandarin chinese' and \
                                setting['mandarin chinese']['last minute and second unit – keep or omit'] != 0:
                            if int(second_notation) < 10:
                                return common_lexicon['integer']['conjunction'] + word_separator + \
                                       digit[int(second_notation)]
                            return write_integer(second_notation)
                    return write_integer(second_notation) + word_separator + date_and_time_lexicon['second']

                def write_quarter(number_of_quarter):
                    hour = date_and_time_lexicon['hour']
                    return write_hour() + word_separator + digit[number_of_quarter] + word_separator + \
                           hour['quarter unit']

                def write_regular_time():
                    hour_writing = write_hour()
                    minute_writing = write_minute()
                    return [hour_writing, minute_writing]

                def write_units():
                    if second_notation == '0':
                        hour_writing = ''
                        minute_writing = ''
                        second_writing = ''
                        if minute_notation == '0' and setting['exact hour – without or with sharp'] != 0:
                            hour_writing += hour_writing + word_separator + \
                                            date_and_time_lexicon['hour']['sharp']
                        elif minute_notation == '15' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            hour_writing += write_quarter(1)
                        elif minute_notation == '30' and setting['minute 30 – number or half hour'] != 0:
                            hour_writing += date_and_time_lexicon['hour']['half']
                        elif minute_notation == '45' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            hour_writing += write_quarter(3)
                        else:
                            hour_writing, minute_writing = write_regular_time()
                    else:
                        hour_writing = write_hour()
                        minute_writing = write_minute()
                        second_writing = write_second()
                    return [hour_writing, minute_writing, second_writing]

                hour_notation, minute_notation, second_notation = date_and_time_notation[1]
                hour_writing, minute_writing, second_writing = write_units()
                time_writing = hour_writing
                if minute_writing != '':
                    time_writing += word_separator + minute_writing
                    if second_writing != '':
                        time_writing += word_separator + second_writing
                return time_writing
            if is_text_to_speech:
                return write_date() + '。' + write_time()
            return write_date() + '\n' + write_time()

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
        if script.index(x) < len(script) - 1:
            final_writing += '\n'
    return final_writing
