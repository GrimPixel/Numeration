def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/por.yaml').read_text())
    if b_is_for_TTS:
        ls_script = ['latim']
    else:
        ls_script = d_setting['ls escrita']
    i_digit_grouping = 3
    s_writing = ''

    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/por.yaml').read_text())['d '+s_the_script]
        s_the_writing = ''

        ls_ten_to_nineteen = d_lexicon['ls dez para dezenove']
        if d_setting['i «catorze» / «quatorze»'] != 0:
            ls_ten_to_nineteen[4] = d_lexicon['s quatorze']
        if d_setting['i «dezasseis», «dezassete», «dezanove» / «dezesseis», «dezessete», «dezenove»'] != 0:
            ls_ten_to_nineteen[6] = d_lexicon['s s dezesseis']
            ls_ten_to_nineteen[7] = d_lexicon['s s dezessete']
            ls_ten_to_nineteen[9] = d_lexicon['s s dezenove']
        ls_archmagnitudes = d_lexicon['s arquil_magnitudes']
        plurals_archmagnitudes = d_lexicon['s arquil_magnitudes plurais']
        if d_setting['i «quatriliões», «quatrilionésimo» / «quadriliões», «quadrilionésimo»'] != 0:
            ls_archmagnitudes[4] = d_lexicon['s quadrilião']
            plurals_archmagnitudes[4] = d_lexicon['s quadriliões']

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0' and s_notation != '0':
                        return ''
                    if s_the_digit == '1' and not (i_the_magnitude == 0 and i_the_archmagnitude == 0):
                        if i_the_magnitude == i_digit_grouping and int(s_the_digit_group) == 1:
                            return ''
                    return d_lexicon['ls digitos'][int(s_the_digit)] + ' '

                def s_write_ten_and_unit():
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        return ls_ten_to_nineteen[int(s_the_digit_group[-1])] + ' '
                    if s_the_digit_group.endswith('0'):
                        return d_lexicon['s decenas'][int(s_the_digit)] + ' '
                    return d_lexicon['s decenas'][int(s_the_digit)] + ' ' + d_lexicon['s e'] + ' ' + d_lexicon['ls digitos'][int(s_the_digit_group[-1])] + ' '

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    if s_the_digit == '1' and s_the_digit_group.endswith('00'):
                        return d_lexicon['s cem'] + ' '
                    return d_lexicon['s centenas'][int(s_the_digit)] + ' '

                s_writing = ''
                if i_the_magnitude == 0:
                    s_writing += s_write_unit()
                elif i_the_magnitude == 1:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude in [2, 5]:
                    s_writing += s_write_hundred()
                if i_the_magnitude in [3, 4] and int(s_the_digit_group) > 0:
                    s_writing += d_lexicon['s mil'] + ' '
                if d_setting['i _ / «y» en lugares vacantes'] != 0 and s_the_digit == '0' and i_the_magnitude > 0 and s_the_digit_group[-i_the_magnitude] != '0':
                    s_writing += d_lexicon['s y'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if i_the_archmagnitude > 0:
                    if int(s_the_double_digit_group) == 1:
                        return d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + ' '
                    return d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s es'] + ' '
                return ''

            s_writing = ''
            if d_setting['s escala'] == 'curta':
                i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
                i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
                while i_the_archmagnitude >= 0:
                    while i_the_magnitude >= 0:
                        s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                        s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                        s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                        s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                        s_writing += s_write_digit_and_magnitude()
                        if i_the_magnitude == 1 and s_the_digit != '0':
                            i_the_magnitude -= 1
                        i_the_magnitude -= 1
                    s_writing += s_write_archmagnitude()
                    i_the_archmagnitude -= 1
                    i_the_magnitude = i_digit_grouping - 1
            elif d_setting['i escala'] == 'longa':
                i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
                i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
                while i_the_archmagnitude >= 0:
                    s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                    s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                    s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                    while i_the_magnitude >= 0:
                        if i_the_magnitude < i_digit_grouping:
                            s_the_digit_group = s_the_double_digit_group
                        else:
                            s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                        s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                        s_writing += s_write_digit_and_magnitude()
                        if i_the_magnitude in [1, 4] and s_the_digit != '0':
                            i_the_magnitude -= 2
                        else:
                            i_the_magnitude -= 1
                    s_writing += s_write_archmagnitude()
                    i_the_archmagnitude -= 1
                    i_the_magnitude = i_digit_grouping * 2 - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0' and s_notation != '0':
                        return ''
                    if s_the_digit == '9' and d_setting['i «nono» / «noveno»'] != 0:
                        return d_lexicon['s noveno']
                    return d_lexicon['ls dígitos ordinais'][int(s_the_digit)]

                def s_write_ten_and_unit():
                    if s_the_digit_group.endswith('0'):
                        return d_lexicon['ls dezenas ordinais'][int(s_the_digit)]
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1' and d_setting['i «décimo primeiro», «décimo segundo» / «undécimo», «duodécimo»'] != 0:
                        if s_the_digit_group.endswith('1'):
                            return d_lexicon['s undécimo']
                        elif s_the_digit_group.endswith('2'):
                            return d_lexicon['s duodécimo']
                    return d_lexicon['ls dezenas ordinais'][int(s_the_digit)] + ' ' + d_lexicon['ls dígitos ordinais'][int(s_the_digit_group[-1])]

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    return d_lexicon['ls centenas ordinais'][int(s_the_digit)] + ' '

                s_writing = ''
                if i_the_magnitude in [0, 3]:
                    s_writing += s_write_unit()
                elif i_the_magnitude in [1, 4]:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude in [2, 5]:
                    s_writing += s_write_hundred()
                if i_the_magnitude in [3, 4] and int(s_the_digit_group) > 0:
                    s_writing = s_writing.rstrip() + d_lexicon['s mil'] + d_lexicon['s ésimo'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if i_the_archmagnitude > 0:
                    if int(s_the_double_digit_group) == 1:
                        return d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + ' '
                    return d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s es'] + ' '
                return ''

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
            i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
            while i_the_archmagnitude >= 0:
                s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                while i_the_magnitude >= 0:
                    if i_the_magnitude < i_digit_grouping:
                        s_the_digit_group = s_the_double_digit_group
                    else:
                        s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                    s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude in [1, 4] and s_the_digit != '0':
                        i_the_magnitude -= 2
                    else:
                        i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping * 2 - 1
            return s_writing.rstrip()

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            s_writing = ''
            for s_character in s_notation:
                s_writing += d_lexicon['ls digitos'][int(s_character)] + '-'
            return s_writing.removesuffix('-')

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            if s_denominator_notation == '2':
                if s_the_digit == '2' and d_setting['i «meio» / «metade»'] != 0:
                    s_denominator_writing = d_lexicon['s meio']
                else:
                    s_denominator_writing = d_lexicon['s metade']
            else:
                def s_write_denominator(s_notation):
                    def s_write_digit_and_magnitude():
                        def s_write_unit():
                            if s_the_digit == '0':
                                return ''
                            if s_the_digit == '7' and d_setting['i «séptimo» / «sétimo»'] != 0:
                                return d_lexicon['s sétimo']
                            if not (i_the_magnitude == 0 and i_the_archmagnitude == 0):
                                if s_the_digit == '1':
                                    return d_lexicon['s primer']
                                elif s_the_digit == '3':
                                    return d_lexicon['s tercer']
                            return d_lexicon['s unidades fraccionarias'][int(s_the_digit)]

                        def s_write_ten_and_unit():
                            if s_the_digit_group.endswith('0'):
                                return lexicon['ls decenas fraccionarios'][int(s_the_digit)]
                            if s_the_digit == '0':
                                return ''
                            elif s_the_digit == '1':
                                if d_setting['i «onceavo», «doceavo» / «undécimo», «duodécimo»'] != 0:
                                    if s_the_digit_group.endswith('1'):
                                        return d_lexicon['s undécimo']
                                    elif s_the_digit_group.endswith('2'):
                                        return d_lexicon['s duodécimo']
                                return d_lexicon['ls dez para dezenove fraccionarios'][int(s_the_digit_group[-1])]
                            if s_the_digit_group.endswith('0'):
                                if d_setting['i «veinteavo», y así sucesivamente / «vigésimo», y así sucesivamente'] == 0:
                                    return d_lexicon['s decenas fraccionarios'][int(s_the_digit)]
                                return d_lexicon['ls dezenas ordinais'][int(s_the_digit)]
                            if s_the_digit_group.endswith('8') and d_setting['i «-ochoavo» / «-ochavo»'] != 0:
                                return d_lexicon['s decenas'][int(s_the_digit)] + d_lexicon['s i'] + d_lexicon['s ochavo']
                            return d_lexicon['s decenas'][int(s_the_digit)] + d_lexicon['s i'] + d_lexicon['ls digitos fraccionarias'][int(s_the_digit_group[-1])]

                        def s_write_hundred():
                            if s_the_digit == '0':
                                return ''
                            if s_the_digit == '1' and d_setting['i «centésimo» / «céntimo» o «centavo»'] != 0:
                                if d_setting['i «céntimo» / «centavo»'] == 0:
                                    return d_lexicon['s céntimo']
                                return lexicon['ls centavo']
                            if s_the_digit_group.endswith('00'):
                                return d_lexicon['s centenas']
                            return d_lexicon['s centenas ordinales'][int(s_the_digit)]

                        s_writing = ''
                        if i_the_magnitude in [0, 3]:
                            s_writing += s_write_unit()
                        elif i_the_magnitude in [1, 4]:
                            s_writing += s_write_ten_and_unit()
                        else:
                            s_writing += s_write_hundred()
                        if i_the_magnitude in [3, 4] and int(s_the_digit_group) > 0:
                            s_writing = s_writing + d_lexicon['s mil']
                        if i_the_archmagnitude == 0 and int(s_the_digit_group[-i_digit_grouping:]) == 0:
                            s_writing += d_lexicon['s ésimo']
                        return s_writing

                    def s_write_archmagnitude():
                        if i_the_archmagnitude > 0:
                            if int(s_the_double_digit_group) == 1:
                                return ' ' + d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + ' '
                            return ' ' + d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s es'] + ' '
                        return ''

                    s_writing = ''
                    i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
                    i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
                    while i_the_archmagnitude >= 0:
                        s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                        s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                        s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                        while i_the_magnitude >= 0:
                            if i_the_magnitude < i_digit_grouping:
                                s_the_digit_group = s_the_double_digit_group
                            else:
                                s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                            s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                            s_writing += s_write_digit_and_magnitude()
                            if i_the_magnitude in [1, 4] and s_the_digit != '0':
                                i_the_magnitude -= 2
                            else:
                                i_the_magnitude -= 1
                        s_writing += s_write_archmagnitude()
                        i_the_archmagnitude -= 1
                        i_the_magnitude = i_digit_grouping * 2 - 1
                    return s_writing.rstrip()

                s_numerator_writing = s_write_integer(s_numerator_notation)
                s_denominator_writing = s_write_denominator(s_denominator_notation)
                if s_numerator_notation != '1':
                    s_denominator_writing += d_lexicon['s s']
                return s_numerator_writing + ' ' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + d_lexicon['s y'] + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += d_lexicon['ls digitos'][int(s_character)] + '-'
            return s_integer_writing + ' ' + d_lexicon['s coma'] + ' ' + s_decimal_writing.removesuffix('-')

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s menos'] + ' ' + s_write_integer(s_exponent_writing.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing + ' ' + d_lexicon['s por'] + ' ' + ls_ten_to_nineteen[0] + ' ' + d_lexicon['s a'] + ' ' + d_lexicon['s la'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                if 0 <= int(s_year_notation) <= 100:
                    s_year_writing = d_lexicon['s el'] + d_lexicon['s año'] + ' ' + s_write_integer(s_year_notation)
                else:
                    s_year_writing = s_write_integer(s_year_notation)
                s_month_writing = d_lexicon['s meses'][int(s_month_notation)]
                if s_day_notation == '1':
                    s_day_writing = s_write_ordinal_number(s_day_notation)
                else:
                    s_day_writing = s_write_integer(s_day_notation)
                return d_lexicon['s el'] + ' ' + s_day_writing + ' ' + d_lexicon['s de'] + ' ' + s_month_writing + ' ' + d_lexicon['s de'] + ' ' + s_year_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')
                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        s_hour_writing = s_write_integer(s_hour_notation)
                        if d_setting['i _ / «en punto»'] != 0:
                            s_hour_writing += ' ' + d_lexicon['s en'] + ' ' + d_lexicon['s punto']
                    else:
                        if s_minute_notation == '45' and d_setting['i «quince», «cuarenta y cinco» / «y cuarto», «un cuarto para»'] != 0:
                            if s_hour_notation == '23':
                                s_hour_notation = '0'
                            else:
                                s_hour_notation = str(int(s_hour_notation) + 1)
                        if s_hour_notation == '1':
                            s_hour_writing = d_lexicon['s la'] + ' ' + d_lexicon['s una']
                        else:
                            s_hour_writing = d_lexicon['s la'] + d_lexicon['s s'] + ' ' + s_write_integer(s_hour_notation)
                        if s_minute_notation in ['15', '45'] and d_setting['i «quince», «cuarenta y cinco» / «y cuarto», «un cuarto para»'] != 0:
                            if s_minute_notation == '15':
                                s_hour_writing += ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s cuarto']
                            else:
                                s_hour_writing = d_lexicon['s un'] + ' ' + d_lexicon['s cuarto'] + ' ' + d_lexicon['s para'] + ' ' + s_hour_writing
                        elif s_minute_notation == '30' and d_setting['i «treinta» / «media»'] != 0:
                            s_hour_writing += ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s media']
                        else:
                            s_minute_writing += s_write_integer(s_minute_notation)
                else:
                    if s_hour_notation == '1':
                        s_hour_writing = d_lexicon['s una']
                    else:
                        s_hour_writing = s_write_integer(s_hour_notation)
                    if s_minute_notation == '1':
                        s_minute_writing = d_lexicon['s un'] + ' ' + d_lexicon['s minuto']
                    else:
                        s_minute_writing = s_write_integer(s_minute_notation) + ' ' + d_lexicon['s minuto'] + d_lexicon['s s']
                    if s_second_notation == '1':
                        s_second_writing = d_lexicon['s un'] + ' ' + d_lexicon['s segundo']
                    else:
                        s_second_writing = s_write_integer(s_second_notation) + ' ' + d_lexicon['s segundo'] + d_lexicon['s s']

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += ' ' + d_lexicon['s y'] + ' ' + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += ' ' + d_lexicon['s y'] + ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing += ' ' + d_lexicon['s por'] + d_lexicon['s centenas'][1]
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s menos'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
