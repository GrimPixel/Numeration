def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/spa.yaml').read_text())
    if b_is_for_TTS:
        ls_script = ['latino']
    else:
        ls_script = d_setting['ls escritura']
    i_digit_grouping = 3
    s_writing = ''

    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/spa.yaml').read_text())['d '+s_the_script]
        s_the_writing = ''

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    elif s_the_digit == '1':
                        if not (i_the_archmagnitude == 0 and i_the_magnitude == 0):
                            if i_the_magnitude == 0 and int(s_the_double_digit_group) == 1 or i_the_magnitude == 3 and int(s_the_digit_group) == 1:
                                return ''
                            return d_lexicon['s un'] + ' '
                    return d_lexicon['ls digitos'][int(s_the_digit)] + ' '
                
                def s_write_ten_and_unit():
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        return d_lexicon['ls diez a diecinueve'][int(s_the_digit_group[-1])] + ' '
                    elif s_the_digit == '2':
                        if s_the_digit_group.endswith('1') and (i_the_archmagnitude > 0 or i_the_magnitude == 4):
                            return d_lexicon['s veinti'] + d_lexicon['s ún'] + ' '
                        return d_lexicon['ls veinte a veintinueve'][int(s_the_digit_group[-1])] + ' '
                    if s_the_digit_group.endswith('0'):
                        return d_lexicon['ls decenas'][int(s_the_digit)] + ' '
                    elif s_the_digit_group.endswith('1'):
                        if i_the_archmagnitude > 0:
                            return d_lexicon['ls decenas'][int(s_the_digit)] + ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s ún'] + ' '
                    return d_lexicon['ls decenas'][int(s_the_digit)] + ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s ls_digitos'][int(s_the_digit_group[-1])] + ' '

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        if s_the_digit_group.endswith('00'):
                            return d_lexicon['s cien'] + ' '
                    return d_lexicon['ls centenas'][int(s_the_digit)] + ' '
                
                def write_thousand():
                    return d_lexicon['s mil'] + ' '

                def write_milliard():
                    if i_the_magnitude == 3 and int(s_the_digit_group) == 1:
                        return d_lexicon['s millardo'] + ' '
                    return d_lexicon['s millardo'] + d_lexicon['s s'] + ' '

                s_writing = ''
                if i_the_magnitude in [0, 3]:
                    s_writing += s_write_unit()
                elif i_the_magnitude in [1, 4]:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude in [2, 5]:
                    s_writing += s_write_hundred()
                if s_the_digit != '0':
                    if i_the_magnitude in [3, 4] or i_the_magnitude == 5 and s_the_digit_group.endswith('00'):
                        if i_the_archmagnitude == 1 and i_the_magnitude >= 3 and d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0:
                            s_writing += write_milliard()
                        else:
                            s_writing += write_thousand()
                if d_setting['i _ / «y» para lugares de «0»'] != 0 and s_the_digit_group.startswith('00') and not s_the_digit_group.endswith('0'):
                    if i_the_magnitude == 1 or i_the_archmagnitude == 1 and i_the_magnitude == 4 and d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0:
                        return d_lexicon['s y'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if not (d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0 and i_the_archmagnitude == 1 and int(s_the_digit_group) == 0):
                    if i_the_archmagnitude > 0:
                        if int(s_the_double_digit_group) == 1:
                            return d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + ' '
                        return d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s es'] + ' '
                return ''

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
            i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
            while i_the_archmagnitude >= 0:
                s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                while i_the_magnitude >= 0:
                    if i_the_magnitude < i_digit_grouping:
                        s_the_digit_group = s_the_double_digit_group[-i_digit_grouping:]
                    else:
                        s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                    s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude in [1, 4] and s_the_digit != '0':
                        i_the_magnitude -= 2
                    else:
                        i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping * 2 - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                        if d_setting['i «ceroésimo» / «cero»'] == 0:
                            return d_lexicon['s ls_digitos'][0]
                    elif s_the_digit == '1':
                        if not (i_the_archmagnitude == 0 and i_the_magnitude == 0):
                            if i_the_magnitude == 0 and int(s_the_double_digit_group) == 1 or i_the_magnitude == 3 and int(s_the_digit_group) == 1:
                                return ''
                            return d_lexicon['s primer'] + ' '
                    elif s_the_digit == '3':
                        if i_the_archmagnitude > 0:
                            return d_lexicon['s tercer'] + ' '
                    elif s_the_digit == '7':
                        if d_setting['i «séptimo» / «sétimo»'] != 0:
                            return d_lexicon['s sétimo']
                    return d_lexicon['s ls_digitos ordinales'][int(s_the_digit)]

                def s_write_ten_and_unit():
                    if s_the_digit_group.endswith('0'):
                        return d_lexicon['ls decenas ordinales'][int(s_the_digit)]
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        if i_the_archmagnitude == 0 and d_setting['i «decimoprimero», «decimosegundo» o «décimo primero», «décimo segundo» / «undécimo», «duodécimo» o «doceno»'] != 0:
                            if s_the_digit_group.endswith('1'):
                                return d_lexicon['s undécimo']
                            elif s_the_digit_group.endswith('2'):
                                if d_setting['i «duodécimo» / «doceno»'] != 0:
                                    return d_lexicon['s doceno']
                                return d_lexicon['s duodécimo']
                    if d_setting['i «decimoprimero», «decimosegundo», y así sucesivamente / «décimo primero», «décimo segundo», y así sucesivamente'] == 0:
                        return d_lexicon['ls decenas ordinales'][int(s_the_digit)].replace(d_lexicon['s é'], d_lexicon['s e']) + d_lexicon['s ls_digitos ordinales'][int(s_the_digit_group[-1])]
                    return d_lexicon['ls decenas ordinales'][int(s_the_digit)] + ' ' + d_lexicon['s ls_digitos ordinales'][int(s_the_digit_group[-1])]

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    return d_lexicon['ls centenas ordinales'][int(s_the_digit)] + ' '
                
                s_writing = ''
                if i_the_magnitude == 0:
                    s_writing += s_write_unit()
                elif i_the_magnitude == 1:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude == 2:
                    s_writing += s_write_hundred()
                elif i_the_magnitude in [3, 4, 5]:
                    if int(s_the_digit_group) > 0:
                        s_writing += s_write_integer(s_the_digit_group + '000').replace(' ', '')
                        s_writing = s_writing.replace(d_lexicon['s ún'], d_lexicon['s un'])
                        s_writing = s_writing.replace(d_lexicon['s y'], d_lexicon['s i']) + d_lexicon['s ésimo'] + ' '
                if s_the_digit != '0':
                    if i_the_magnitude in [3, 4] or i_the_magnitude == 5 and s_the_digit_group.endswith('00'):
                        if i_the_archmagnitude == 1 and i_the_magnitude >= 3 and d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0:							
                            s_writing = s_writing.removesuffix(d_lexicon['s mil'] + d_lexicon['s ésimo'] + ' ') + d_lexicon['s millardésimo'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if not (d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0 and i_the_archmagnitude == 1 and int(s_the_digit_group) == 0):
                    if i_the_archmagnitude > 0:
                        return d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s ésimo'] + ' '
                return ''

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
            i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
            while i_the_archmagnitude >= 0:
                s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                while i_the_magnitude >= 0:
                    if i_the_magnitude < i_digit_grouping:
                        s_the_digit_group = s_the_double_digit_group[-i_digit_grouping:]
                    else:
                        s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                    s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude == 1 and s_the_digit != '0':
                        i_the_magnitude -= 2
                    elif i_the_magnitude in [3, 4, 5]:
                        i_the_magnitude = 2
                    else:
                        i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping * 2 - 1
            return s_writing.rstrip().replace(d_lexicon['s o']*2, d_lexicon['s o'])

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            s_writing = ''
            for s_character in s_notation:
                s_writing += d_lexicon['s ls_digitos'][int(s_character)] + '-'
            return s_writing.removesuffix('-')

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            if s_denominator_notation == '2':
                if d_setting['i «medio» / «mitad»'] == 0:
                    s_denominator_writing = d_lexicon['ls unidades fraccionarias'][2]
                else:
                    s_denominator_writing = d_lexicon['s mitad']

            def s_write_denominator(s_notation):
                def s_write_digit_and_magnitude():
                    def s_write_unit():
                        if s_the_digit == '0':
                            return ''
                        elif s_the_digit == '1':
                            if i_the_archmagnitude > 0 or i_the_archmagnitude == 1 and i_the_magnitude == 3 and d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0:
                                return ''
                            return d_lexicon['s ls_digitos ordinales'][1] + ' '
                        elif s_the_digit == '2':
                            if s_notation == '2' and i_the_archmagnitude == 0 and i_the_magnitude == 0:
                                if d_setting['i «medio» / «mitad»'] != 0:
                                    return d_lexicon['s mitad'] + ' '
                        elif s_the_digit == '8':
                            if int(s_the_digit_group) > 8:
                                if d_setting['i «dieciochoavo», «veintiochoavo», y así sucesivamente / «dieciochavo», «veintiochavo», y así sucesivamente'] != 0:
                                    return d_lexicon['s ochavo'] + ' '
                        if d_setting['i «centavo», y así sucesivamente / «centésimo» o «céntimo», y así sucesivamente'] == 0:
                            return d_lexicon['s ls_digitos fraccionarias'][int(s_the_digit)] + ' '
                        return d_lexicon['ls unidades fraccionarias'][int(s_the_digit)] + ' '

                    def s_write_ten_and_unit():
                        if s_the_digit == '0':
                            return ''
                        elif s_the_digit == '1':
                            if d_setting['i «onceavo», «doceavo» / «undécimo», «duodécimo»'] != 0:
                                if s_the_digit_group.endswith('1'):
                                    return d_lexicon['s undécimo']
                                elif s_the_digit_group.endswith('2'):
                                    return d_lexicon['s duodécimo']
                            return d_lexicon['ls diez a diecinueve fraccionarios'][int(s_the_digit_group[-1])] + ' '
                        elif s_the_digit == '2':
                            if s_the_digit_group.endswith('0'):
                                if d_setting['i «veinteavo» / «veintavo»'] != 0:
                                    return d_lexicon['s veinteavo'] + ' '
                                return d_lexicon['ls decenas fraccionarios'][2] + ' '
                            elif s_the_digit_group.endswith('8'):
                                if d_setting['i «dieciochoavo», «veintiochoavo», y así sucesivamente / «dieciochavo», «veintiochavo», y así sucesivamente'] != 0:
                                    return d_lexicon['s veinti'] + d_lexicon['s i'] + d_lexicon['s ochavo'] + ' '
                            return d_lexicon['s veinti'] + d_lexicon['s ls_digitos fraccionarias'][int(s_the_digit_group[-1])] + ' '
                        if s_the_digit_group.endswith('0'):
                            if d_setting['i «veinteavo», y así sucesivamente / «vigésimo», y así sucesivamente'] == 0:
                                return d_lexicon['ls decenas fraccionarios'][int(s_the_digit)] + ' '
                            return d_lexicon['ls decenas ordinales'][int(s_the_digit)] + ' '
                        elif s_the_digit_group.endswith('8'):
                            if d_setting['i «dieciochoavo», «veintiochoavo», y así sucesivamente / «dieciochavo», «veintiochavo», y así sucesivamente'] != 0:
                                return d_lexicon['ls decenas'][int(s_the_digit)] + d_lexicon['s i'] + d_lexicon['s ochavo'] + ' '
                        return d_lexicon['ls decenas'][int(s_the_digit)] + d_lexicon['s i'] + d_lexicon['s ls_digitos fraccionarias'][int(s_the_digit_group[-1])] + ' '

                    def s_write_hundred():
                        if s_the_digit == '0':
                            return ''
                        if s_the_digit == '1':
                            if d_setting['i «centavo», y así sucesivamente / «centésimo» o «céntimo», y así sucesivamente'] != 0:
                                if d_setting['i «centésimo» / «céntimo»'] == 0:
                                    return d_lexicon['ls centenas ordinales'][int(s_the_digit)] + ' '
                                return d_lexicon['s céntimo'] + ' '
                        if s_the_digit_group.endswith('00'):
                            return d_lexicon['s centavo'] + ' '
                        if d_setting['i «centavo», y así sucesivamente / «centésimo» o «céntimo», y así sucesivamente'] == 0:
                            return d_lexicon['ls centenas'][int(s_the_digit)]
                        return d_lexicon['ls centenas'][int(s_the_digit)] + ' '

                    s_writing = ''
                    if i_the_magnitude == 0:
                        s_writing += s_write_unit()
                    elif i_the_magnitude == 1:
                        s_writing += s_write_ten_and_unit()
                    elif i_the_magnitude == 2:
                        s_writing += s_write_hundred()
                    elif i_the_magnitude in [3, 4, 5]:
                        if int(s_the_digit_group) > 0:
                            s_writing += s_write_integer(s_the_digit_group + '000').replace(' ', '')
                            s_writing = s_writing.replace(d_lexicon['s ún'], d_lexicon['s un'])
                            s_writing = s_writing.replace(d_lexicon['s y'], d_lexicon['s i']) + d_lexicon['s ésimo'] + ' '
                    if s_the_digit != '0':
                        if i_the_magnitude in [3, 4] or i_the_magnitude == 5 and s_the_digit_group.endswith('00'):
                            if i_the_archmagnitude == 1 and i_the_magnitude >= 3 and d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0:							
                                s_writing = s_writing.removesuffix(d_lexicon['s mil'] + d_lexicon['s ésimo'] + ' ') + d_lexicon['s millardésimo'] + ' '
                    return s_writing

                def s_write_archmagnitude():
                    if not (d_setting['i «mil millones», y así sucesivamente / «un millardo», y así sucesivamente'] != 0 and i_the_archmagnitude == 1 and int(s_the_digit_group) == 0):
                        if i_the_archmagnitude > 0:
                            return d_lexicon['ls archimagnitudes'][i_the_archmagnitude].replace(d_lexicon['s ó'], d_lexicon['s o']) + d_lexicon['s ésimo'] + ' '
                    return ''

                s_writing = ''
                i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
                i_the_magnitude = (len(s_notation) - 1) % (i_digit_grouping * 2)
                while i_the_archmagnitude >= 0:
                    s_the_double_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                    s_the_double_digit_group_start = s_the_double_digit_group_end + i_digit_grouping * 2
                    s_the_double_digit_group = f'{s_notation} '[-s_the_double_digit_group_start:-s_the_double_digit_group_end]
                    while i_the_magnitude >= 0:
                        if i_the_magnitude < i_digit_grouping:
                            s_the_digit_group = s_the_double_digit_group[-i_digit_grouping:]
                        else:
                            s_the_digit_group = s_the_double_digit_group[:-i_digit_grouping]
                        s_the_digit = s_the_double_digit_group[-i_the_magnitude - 1]
                        s_writing += s_write_digit_and_magnitude()
                        if i_the_magnitude == 1 and s_the_digit != '0':
                            i_the_magnitude -= 2
                        elif i_the_magnitude in [3, 4, 5]:
                            i_the_magnitude = 2
                        else:
                            i_the_magnitude -= 1
                    s_writing += s_write_archmagnitude()
                    i_the_archmagnitude -= 1
                    i_the_magnitude = i_digit_grouping * 2 - 1
                return s_writing.rstrip().replace(d_lexicon['s o']*2, d_lexicon['s o'])

            s_numerator_writing = s_write_integer(s_numerator_notation)
            if s_numerator_writing.endswith(d_lexicon['s ls_digitos'][1]):
                s_numerator_writing = s_numerator_writing.removesuffix(d_lexicon['s ls_digitos'][1]) + d_lexicon['s un']
            s_denominator_writing = s_write_denominator(s_denominator_notation)
            if s_numerator_notation != '1':
                s_denominator_writing += d_lexicon['s s']
            return s_numerator_writing + ' ' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + d_lexicon['s y'] + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += d_lexicon['s ls_digitos'][int(s_character)] + '-'
            return s_integer_writing + ' ' + d_lexicon['s coma'] + ' ' + s_decimal_writing.removesuffix('-')

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s menos'] + ' ' + s_write_integer(s_exponent_writing.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing + ' ' + d_lexicon['s por'] + ' ' + d_lexicon['ls diez a diecinueve'][0] + ' ' + d_lexicon['s a'] + ' ' + d_lexicon['s la'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                s_year_writing = s_write_integer(s_year_notation)
                s_month_writing = d_lexicon['ls meses'][int(s_month_notation)]
                if s_day_notation == '1':
                    s_day_writing = s_write_ordinal_number(s_day_notation)
                else:
                    s_day_writing = s_write_integer(s_day_notation)
                return d_lexicon['s el'] + ' ' + s_day_writing + ' ' + d_lexicon['s de'] + ' ' + s_month_writing + ' ' + d_lexicon['s de'] + ' ' + s_year_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')
                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        s_hour_writing = s_write_integer(s_hour_notation)
                        if d_setting['i _ / «en punto»'] != 0:
                            s_hour_writing += ' ' + d_lexicon['s en'] + ' ' + d_lexicon['s punto']
                    else:
                        if s_minute_notation == '45' and d_setting['i «quince», «cuarenta y cinco» / «y cuarto», «un cuarto para»'] != 0:
                            if s_hour_notation == '23':
                                s_hour_notation = '0'
                            else:
                                s_hour_notation = str(int(s_hour_notation) + 1)
                        if s_hour_notation == '1':
                            s_hour_writing = d_lexicon['s la'] + ' ' + d_lexicon['s una']
                        else:
                            s_hour_writing = d_lexicon['s la'] + d_lexicon['s s'] + ' ' + s_write_integer(s_hour_notation)
                        if s_minute_notation in ['15', '45'] and d_setting['i «quince», «cuarenta y cinco» / «y cuarto», «un cuarto para»'] != 0:
                            if s_minute_notation == '15':
                                s_hour_writing += ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s cuarto']
                            else:
                                s_hour_writing = d_lexicon['s un'] + ' ' + d_lexicon['s cuarto'] + ' ' + d_lexicon['s para'] + ' ' + s_hour_writing
                        elif s_minute_notation == '30' and d_setting['i «treinta» / «media»'] != 0:
                            s_hour_writing += ' ' + d_lexicon['s y'] + ' ' + d_lexicon['s media']
                        else:
                            s_minute_writing += s_write_integer(s_minute_notation)
                else:
                    if s_hour_notation == '1':
                        s_hour_writing = d_lexicon['s un'] + d_lexicon['s a']
                    else:
                        s_hour_writing = s_write_integer(s_hour_notation)
                    if s_minute_notation == '1':
                        s_minute_writing = d_lexicon['s un'] + ' ' + d_lexicon['s minuto']
                    else:
                        s_minute_writing = s_write_integer(s_minute_notation) + ' ' + d_lexicon['s minuto'] + d_lexicon['s s']
                    if s_second_notation == '1':
                        s_second_writing = d_lexicon['s un'] + ' ' + d_lexicon['s segundo']
                    else:
                        s_second_writing = s_write_integer(s_second_notation) + ' ' + d_lexicon['s segundo'] + d_lexicon['s s']

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += ' ' + d_lexicon['s y'] + ' ' + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += ' ' + d_lexicon['s y'] + ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing += ' ' + d_lexicon['s por'] + d_lexicon['ls centenas'][1]
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s menos'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
