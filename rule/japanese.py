def do(notation, notation_type, setting_identifier, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['japanese']
    if is_text_to_speech:
        script = ['Hrkt']
    else:
        script = setting['script']
    final_writing = ''
    for x in script:
        current_script = yaml.load(open('rule/library/script decode.yaml'))[x]
        common_lexicon = yaml.load(open('rule/lexicon/japanese.yaml'))[current_script]['common']

        if current_script == 'japanese':
            digit = common_lexicon['integer']['digit']['common']
            magnitude = common_lexicon['integer']['magnitude']['common']
            if setting['kanji – common or formal'] != 0:
                digit[:4] = common_lexicon['integer']['digit']['formal']
                magnitude[1] = common_lexicon['integer']['magnitude']['formal']
        elif current_script == 'japanese syllabaries':
            digit = common_lexicon['integer']['digit']['prevalent']

            if setting['kana 0 – prevalent or alternative'] != 0:
                digit[0] = common_lexicon['integer']['digit']['regular']['alternative']['0']
            if setting['kana 4 – prevalent or alternative'] != 0:
                digit[4] = common_lexicon['integer']['digit']['regular']['alternative']['4']
            if setting['kana 7 – prevalent or alternative'] != 0:
                digit[7] = common_lexicon['integer']['digit']['regular']['alternative']['7']
            if setting['kana 9 – prevalent or alternative'] != 0:
                digit[9] = common_lexicon['integer']['digit']['regular']['alternative']['9']

            ten = common_lexicon['integer']['ten']
            hundred = common_lexicon['integer']['hundred']['prevalent']
            if setting['one in 100 and 1000 – omit or keep'] != 0:
                hundred[1] = common_lexicon['integer']['hundred']['alternative']['1']
            if setting['kana 900 – prevalent or alternative'] != 0:
                hundred[9] = common_lexicon['integer']['hundred']['alternative']['9']

            thousand = common_lexicon['integer']['thousand']['prevalent']
            if setting['one in 100 and 1000 – omit or keep'] != 0:
                thousand[1] = common_lexicon['integer']['thousand']['alternative']['1']
            if setting['kana 4000 – prevalent or alternative'] != 0:
                thousand[4] = common_lexicon['integer']['thousand']['alternative']['4']

        archmagnitude = common_lexicon['integer']['archmagnitude']

        def write_integer(integer_notation):
            def renew_focused_magnitude():
                return setting['digit grouping'] - 1

            def write_digit_and_magnitude():
                def write_kanji_digit():
                    if current_digit == '0' and integer_notation != '0':
                        return ''
                    elif current_digit == '1' and \
                            (focused_magnitude == 1 or focused_magnitude > 1 and
                             setting['one in 100 and 1000 – omit or keep'] == 0):
                        return ''
                    return digit[int(current_digit)]

                def write_kanji_magnitude():
                    if focused_magnitude == 0 or current_digit == '0':
                        return ''
                    return magnitude[focused_magnitude]

                def write_kana_digit_and_magnitude():
                    if focused_magnitude == 0:
                        if current_digit == '0' and integer_notation != '0' or focused_archmagnitude > 0:
                            return ''
                        return digit[int(current_digit)]
                    if current_digit == '0':
                        return ''
                    if focused_magnitude == 1:
                        if current_digit == '1':
                            return ten
                        return digit[int(current_digit)] + ten
                    if focused_magnitude == 2:
                        return hundred[int(current_digit)]
                    return thousand[int(current_digit)]

                if current_script == 'japanese':
                    return write_kanji_digit() + write_kanji_magnitude()
                elif current_script == 'japanese syllabaries':
                    return write_kana_digit_and_magnitude()

            def write_archmagnitude():
                def write_kanji_archmagnitude():
                    return archmagnitude[focused_archmagnitude]

                def write_kana_unit_digit_and_archmagnitude():
                    if focused_archmagnitude == (3 or 6 or 10):
                        return archmagnitude['prefix']['s/ch'][int(current_digit)] + \
                               archmagnitude['suffix'][focused_archmagnitude]
                    elif focused_archmagnitude == (4 or 8 or 9):
                        return archmagnitude['prefix']['k'][int(current_digit)] + \
                               archmagnitude['suffix'][focused_archmagnitude]
                    else:
                        if current_digit == '0':
                            return archmagnitude['suffix'][focused_archmagnitude]
                        return digit[int(current_digit)] + archmagnitude['suffix'][focused_archmagnitude]

                if focused_archmagnitude > 0:
                    if int(current_digit_group) > 0:
                        if current_script == 'japanese':
                            return write_kanji_archmagnitude()
                        elif current_script == 'japanese syllabaries':
                            return write_kana_unit_digit_and_archmagnitude()
                return ''

            def determine_initial_focused():
                focused_magnitude = (len(integer_notation) - 1) % setting['digit grouping']
                if setting['buddhist archmagnitude digit group – single or double'] != 0 and \
                        len(integer_notation) > 12 * setting['digit grouping']:
                    focused_archmagnitude = (len(integer_notation) - 1 - 12 * setting['digit grouping']) // \
                                            (setting['digit grouping'] * 2) + 12 + 1
                else:
                    focused_archmagnitude = (len(integer_notation) - 1) // setting['digit grouping']
                return [focused_archmagnitude, focused_magnitude]

            def determine_buddhist_large_archmagnitude_digit_group():
                return get.digit_group(integer_notation[:-12 * setting['digit grouping']],
                                       focused_archmagnitude - 12, setting['digit grouping'] * 2)

            def write_buddhist_large_archmagnitude():
                return write_integer(current_digit_group) + write_archmagnitude()

            writing = ''
            focused_archmagnitude, focused_magnitude = determine_initial_focused()
            while focused_archmagnitude >= 0:
                if setting['buddhist archmagnitude digit group – single or double'] != 0 and focused_archmagnitude >= 48:
                    current_digit_group = determine_buddhist_large_archmagnitude_digit_group()
                    writing += write_buddhist_large_archmagnitude()
                else:
                    current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                          setting['digit grouping'])
                    while focused_magnitude >= 0:
                        current_digit = current_digit_group[-focused_magnitude - 1]
                        writing += write_digit_and_magnitude()
                        focused_magnitude -= 1
                    focused_magnitude = renew_focused_magnitude()
                writing += write_archmagnitude()
                focused_archmagnitude -= 1
            return writing

        def output_math_numeral(math_numeral_notation):
            math_numeral = yaml.load(open('rule/lexicon/japanese.yaml'))[current_script]['math numeral']
            
            def write_fraction(fraction_notation):
                numerator, denominator = fraction_notation.split('/')
                numerator = write_integer(numerator)
                denominator = write_integer(denominator) + math_numeral['fraction']
                return denominator + common_lexicon['particle'] + numerator

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                if setting['mixed number – conjunction or counter'] == 0:
                    return integer + math_numeral['mixed number']['conjunction'] + fraction
                return integer + math_numeral['mixed number']['counter'] + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                integer = write_integer(integer)
                decimal = write.consecutive_digit(decimal, digit)
                return integer + math_numeral['decimal'] + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)
                if check.is_negative(exponent):
                    exponent = common_lexicon['negative'] + write_integer(exponent[1:])
                else:
                    exponent = write_integer(exponent)
                if current_script == 'japanese':
                    return magnitude[1] + common_lexicon['particle'] + exponent + \
                           math_numeral['scientific notation']['power'] + \
                           common_lexicon['particle'] + multiplier + math_numeral['scientific notation']['fold']
                elif current_script == 'japanese syllabaries':
                    return ten + common_lexicon['particle'] + exponent + \
                           math_numeral['scientific notation']['power'] + \
                           common_lexicon['particle'] + multiplier + math_numeral['scientific notation']['fold']

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                percent = common_lexicon['percent']
                writing += percent
            if is_negative:
                negative = common_lexicon['negative']
                writing = negative + writing
            return writing

        def output_ordinal_number(ordinal_notation):
            return write_integer(ordinal_notation) + \
                   yaml.load(open('rule/lexicon/japanese.yaml'))[current_script]['ordinal']

        def output_nominal_number(nominal_notation):
            return write.consecutive_digit(nominal_notation, digit)

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/japanese.yaml'))[current_script]['date and time']

            def write_date():
                def write_year():
                    if year_notation == '1':
                        return date_and_time_lexicon['year']['first'] + date_and_time_lexicon['year']['suffix']
                    return write_integer(year_notation) + date_and_time_lexicon['year']['suffix']

                def write_kanji_month():
                    if setting['month – common or literal'] == 0:
                        return write_integer(month_notation) + date_and_time_lexicon['month']['suffix']
                    return date_and_time_lexicon['month']['literal'][int(month_notation)]

                def write_kanji_day():
                    return write_integer(day_notation) + date_and_time_lexicon['day']

                def write_kana_month():
                    if setting['month – common or literal'] == 0:
                        month_name = date_and_time_lexicon['month']['prevalent']
                        if month_notation == '7' and setting['kana_month 7 – prevalent or alternative'] == 1:
                            month_name[7] = date_and_time_lexicon['month']['alternative']
                        return month_name[int(month_notation)]
                    return date_and_time_lexicon['month']['literal'][int(month_notation)]

                def write_kana_day():
                    if day_notation == '1' and setting['kana day 1 – prevalent or alternative'] != 0:
                        return date_and_time_lexicon['day']['alternative']['1']
                    if day_notation == '7' and setting['kana day 7 – prevalent or original'] != 0:
                        return date_and_time_lexicon['day']['alternative']['7']
                    if day_notation == '14' and setting['kana day 14 – prevalent or alternative'] != 0:
                        return date_and_time_lexicon['day']['alternative']['14']
                    if day_notation == '30' and setting['kana day 30 – prevalent or alternative'] != 0:
                        return date_and_time_lexicon['day']['alternative']['30']
                    return date_and_time_lexicon['day']['prevalent'][int(day_notation)]

                def write_month_and_day():
                    if current_script == 'japanese':
                        return [write_kanji_month(), write_kanji_day()]
                    elif current_script == 'japanese syllabaries':
                        return [write_kana_month(), write_kana_day()]

                year_notation, month_notation, day_notation = date_and_time_notation[0]
                year_writing = write_year()
                month_writing, day_writing = write_month_and_day()
                return year_writing + month_writing + day_writing

            def write_time():
                def write_kanji_hour():
                    if minute_notation == '30' and second_notation == '0' and \
                            setting['minute 30 – number or half hour'] != 0:
                        return write_integer(hour_notation) + date_and_time_lexicon['hour'] + \
                               date_and_time_lexicon['half']
                    return write_integer(hour_notation) + date_and_time_lexicon['hour']

                def write_kana_hour():
                    if setting['kana hour 7 and 17 – prevalent or alternative'] != 0:
                        if hour_notation == '7':
                            return date_and_time_lexicon['hour']['alternative']['7']
                        if hour_notation == '17':
                            return date_and_time_lexicon['hour']['alternative']['17']
                    if minute_notation == '30' and second_notation == '0' and \
                            setting['minute 30 – number or half hour'] != 0:
                        return date_and_time_lexicon['hour'][int(hour_notation)] + \
                               date_and_time_lexicon['half']
                    return date_and_time_lexicon['hour']['prevalent'][int(hour_notation)]

                def write_kanji_minute():
                    if minute_notation != '0':
                        return write_integer(minute_notation) + date_and_time_lexicon['minute']
                    return ''

                def write_kana_minute():
                    if int(minute_notation) <= 10:
                        if minute_notation == '3' and setting['kana minutes with 3 – voiceless or semivoiced'] != 0:
                            return date_and_time_lexicon['minute']['ten']['alternative']['3']
                        if minute_notation == '4' and setting['kana minutes with 4 – voiceless or semivoiced'] != 0:
                            return date_and_time_lexicon['minute']['ten']['alternative']['4']
                        if minute_notation == '7' and setting['kana minutes with 7 – native or sinitic'] != 0:
                            return date_and_time_lexicon['minute']['ten']['alternative']['7']
                        if minute_notation == '8' and setting['kana minutes with 8 – voiceless or semivoiced'] != 0:
                            return date_and_time_lexicon['minute']['ten']['alternative']['8']
                        return date_and_time_lexicon['minute']['unit']['prevalent'][int(minute_notation)]
                    if int(minute_notation) % 10 == 0:
                        if setting['kana minutes with 10 – prevalent or alternative'] != 0:
                            return date_and_time_lexicon['minute']['tens']['prevalent'][
                                int(minute_notation) // 10]
                        return date_and_time_lexicon['minute']['tens']['alternative'][
                            int(minute_notation) // 10]
                    return digit[int(minute_notation) // 10] + ten + \
                           date_and_time_lexicon['minute']['unit']['prevalent'][int(minute_notation) % 10]

                def write_second():
                    if second_notation != '0':
                        return write_integer(second_notation) + date_and_time_lexicon['second']
                    return ''

                hour_notation, minute_notation, second_notation = date_and_time_notation[1]
                hour_writing = ''
                minute_writing = ''
                second_writing = ''
                if current_script == 'japanese':
                    hour_writing += write_kanji_hour()
                    minute_writing += write_kanji_minute()
                elif current_script == 'japanese syllabaries':
                    hour_writing += write_kana_hour()
                    minute_writing += write_kana_minute()
                second_writing += write_second()

                time_writing = hour_writing
                if minute_writing != '':
                    time_writing += minute_writing
                    if second_writing != '':
                        time_writing += second_writing
                return time_writing
            if is_text_to_speech:
                return write_date() + '。' + write_time()
            return write_date() + '\n' + write_time()

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
        if script.index(x) < len(script) - 1:
            final_writing += '\n'
    return final_writing
