def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/zho.yaml').read_text())
    
    if b_is_for_TTS:
        ls_script = ['漢字正體']
    else:
        ls_script = d_setting['ls 文字']

    if d_setting['s 進制'] == '下數':
        i_digit_grouping = 21
    else:
        i_digit_grouping = 4

    s_writing = ''
    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/zho.yaml').read_text())['d '+s_the_script]

        if d_setting['i 小寫／大寫'] == 0:
            ls_digit = d_lexicon['ls 小寫數字']
            ls_magnitude = d_lexicon['ls 小寫量級']
        else:
            ls_digit = d_lexicon['ls 大寫數字']
            ls_magnitude = d_lexicon['ls 大寫量級']
        if b_is_for_TTS:
            ls_digit[0] = d_lexicon['ls 大寫數字'][0]

        ls_short_scale_magnitude = ls_magnitude + d_lexicon['ls 主量級'][1:]

        if s_the_script in ['注音字母', '拉丁文粤拼']:
            s_syllable_separator = ' '
            s_word_separator = ' '
        elif s_the_script in ['拉丁文拼音']:
            s_syllable_separator = ''
            s_word_separator = ' '
        else:
            s_syllable_separator = ''
            s_word_separator = ''
        
        s_the_writing = ''

        def s_write_integer(s_notation):
            def s_write_short_scale_integer(s_notation):
                def s_write_digit(s_the_digit, s_notation, i_the_archmagnitude, i_the_magnitude):
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    elif s_the_digit == '1':
                        if i_the_magnitude == 1 and (len(s_notation) == 2 or d_setting['i 中低位10作「一十」／中低位10作「十」'] != 0):
                            return ''
                    elif s_the_digit == '2':
                        if d_setting['i 「二百」／「兩百」'] != 0 and i_the_magnitude == 2:
                            return d_lexicon['s 兩'] + s_syllable_separator
                        if d_setting['i 「二千」／「兩千」'] != 0 and i_the_magnitude == 3:
                            return d_lexicon['s 兩'] + s_syllable_separator
                        if d_setting['i 「二」主量級／「兩」主量級'] != 0 and i_the_archmagnitude > 0 and i_the_magnitude == 0:
                            return d_lexicon['s 兩'] + s_syllable_separator
                    return ls_digit[int(s_the_digit)] + s_syllable_separator

                def s_write_magnitude(s_the_digit, s_notation, i_the_archmagnitude, i_the_magnitude):
                    if s_the_digit == '0':
                        return ''
                    if d_setting['i 末位量級／_'] != 0 and i_the_archmagnitude == 0 and i_the_magnitude > 0 and int(s_notation[-i_the_magnitude:]) == 0:
                        return ''
                    return ls_short_scale_magnitude[i_the_magnitude] + s_word_separator

                s_writing = ''
                i_the_magnitude = len(s_notation) - 1
                while i_the_magnitude >= 0:
                    s_the_digit = s_notation[-i_the_magnitude - 1]
                    if d_setting['i 「二十」、「三十」、「四十」／「廿」、「卅」、「卌」'] != 0 and i_the_magnitude == 1 and s_the_digit in ['2', '3', '4']:
                        s_writing += d_lexicon['ls 廿、卅、卌'][int(s_the_digit)] + s_word_separator
                    else:
                        s_writing += s_write_digit(s_the_digit, s_notation, 0, i_the_magnitude) + s_write_magnitude(s_the_digit, s_notation, 0, i_the_magnitude)
                    if s_the_digit == '0':
                        if i_the_magnitude > 0 and s_notation[-i_the_magnitude] != '0':
                            s_writing += d_lexicon['s 零'] + s_word_separator
                    i_the_magnitude -= 1
                return s_writing.rstrip()

            def s_write_myriad_scale_integer(s_notation):
                s_writing = ''
                i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
                while i_the_archmagnitude >= 0:
                    s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                    s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                    s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                    if int(s_the_digit_group) == 0:
                        if i_the_archmagnitude > 0 and s_notation[-s_the_digit_group_end + 1] != '0':
                            s_writing += d_lexicon['s 零'] + s_word_separator
                    elif int(s_the_digit_group) == 2:
                        if d_setting['i 「二」主量級／「兩」主量級'] != 0:
                            s_writing += d_lexicon['s 兩'] + s_word_separator
                    else:
                        if s_the_digit_group.endswith('0') and int(s_the_digit_group) > 0:
                            s_writing += d_lexicon['s 零'] + s_word_separator
                        s_writing += s_write_short_scale_integer(str(int(s_the_digit_group))) + s_word_separator
                        if int(s_the_digit_group) != 0 and i_the_archmagnitude > 0:
                            s_writing += d_lexicon['ls 主量級'][i_the_archmagnitude] + s_word_separator
                    i_the_archmagnitude -= 1
                return s_writing.rstrip()

            def s_write_middle_scale_integer():
                ls_middle_scale_archmagnitude = d_lexicon['ls 主量級'][:1] + d_lexicon['ls 主量級'][2:]
                s_writing = ''
                i_the_archmagnitude = (len(s_notation) - 1) // (i_digit_grouping * 2)
                while i_the_archmagnitude >= 0:
                    s_the_digit_group_end = i_digit_grouping * 2 * i_the_archmagnitude + 1
                    s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping * 2
                    s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                    if int(s_the_digit_group) == 0:
                        if i_the_archmagnitude > 0 and int(f'{s_notation} '[-s_the_digit_group_end:-s_the_digit_group_end + i_digit_grouping]) > 0:
                            s_writing += d_lexicon['s 零'] + s_word_separator
                    elif int(s_the_digit_group) == 2:
                        if d_setting['i 「二」主量級／「兩」主量級'] != 0:
                            s_writing += d_lexicon['s 兩'] + s_word_separator
                    else:
                        if s_the_digit_group.endswith('0') and int(s_the_digit_group) > 0:
                            s_writing += d_lexicon['s 零'] + s_word_separator
                        s_writing += s_write_myriad_scale_integer(str(int(s_the_digit_group))) + s_word_separator
                        if int(s_the_digit_group) != 0 and i_the_archmagnitude > 0:
                            s_writing += ls_middle_scale_archmagnitude[i_the_archmagnitude] + s_word_separator
                    i_the_archmagnitude -= 1
                return s_writing.rstrip()

            if d_setting['s 進制'] == '下數':
                return s_write_short_scale_integer(s_notation)
            elif d_setting['s 進制'] == '萬進':
                return s_write_myriad_scale_integer(s_notation)
            elif d_setting['s 進制'] == '中數':
                return s_write_middle_scale_integer()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            return d_lexicon['s 第'] + s_word_separator + s_write_integer(s_notation)

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            if d_setting['i 「幺」作名義數１／「一」作名義數１'] == 0:
                ls_digit[1] = d_lexicon['s 幺']
                s_writing = ''
                for s_character in s_notation:
                    s_writing += ls_digit[int(s_character)] + s_syllable_separator
            return s_writing

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            s_numerator_writing = s_write_integer(s_numerator_notation)
            s_denominator_writing = s_write_integer(s_denominator_notation) + s_word_separator + d_lexicon['s 分']
            return s_denominator_writing + s_word_separator + d_lexicon['s 之'] + s_word_separator + s_numerator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + s_word_separator + d_lexicon['s 又'] + s_word_separator + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += ls_digit[int(s_character)] + s_syllable_separator
            return s_integer_writing + s_word_separator + d_lexicon['s 點'] + s_word_separator + s_decimal_writing

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            s_multiplied_by = d_lexicon['s 乘']
            if d_setting['i 「乘以」／「乘」'] == 0:
                s_multiplied_by += s_word_separator + d_lexicon['s 以']
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s 負'] + s_word_separator + s_write_integer(s_exponent.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing + s_word_separator + s_multiplied_by + s_word_separator + ls_magnitude[1] + s_word_separator + d_lexicon[d_setting['s 所有格助詞']] + s_word_separator + s_exponent_writing + s_word_separator + d_lexicon['s 次方']

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                if s_year_notation == '1':
                    s_year_writing = d_lexicon['s 元'] + s_syllable_separator
                else:
                    s_year_writing = ''
                    for s_character in s_year_notation:
                        s_year_writing += ls_digit[int(s_character)] + s_syllable_separator
                s_year_writing += s_word_separator + d_lexicon['s 年']
                if d_setting['i 格里曆／农曆'] == 0:
                    s_month_writing = s_write_integer(s_month_notation) + s_word_separator + d_lexicon['s 月']
                    if d_setting['i 「日」／「號」'] == 0:
                        s_day_writing =  s_write_integer(s_month_notation) + s_word_separator + d_lexicon['s 日']
                    else:
                        s_day_writing =  s_write_integer(s_month_notation) + s_word_separator + d_lexicon['s 號']
                else:
                    s_month_writing = d_lexicon['ls 农曆月份'][int(s_month_notation)]
                    if int(s_day_notation) < 10:
                        s_day_writing = d_lexicon['s 初'] + s_write_integer(s_day_notation)
                    else:
                        s_day_writing = s_write_integer(s_day_notation)
                s_date_writing = s_year_writing + s_word_separator + s_month_writing + s_word_separator + s_day_writing
                return s_date_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')

                s_hour_writing = ''
                if s_hour_notation == '2':
                    if d_setting['i 「二點」／「兩點」'] == 1 and d_setting['i 「時」／「點」'] != 0:
                        s_hour_writing += d_lexicon['s 兩']
                else:
                    s_hour_writing += s_write_integer(s_hour_notation)
                if d_setting['i 「時」／「點」'] == 0:
                    s_hour_writing += s_word_separator + d_lexicon['s 時']
                else:
                    s_hour_writing += s_word_separator + d_lexicon['s 點']
                
                s_minute_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        if d_setting['i _／「整」'] != 0:
                            s_hour_writing += s_word_separator + d_lexicon['s 整']
                    elif s_minute_notation in ['15', '45']:
                        if d_setting['i 「十五分」、「四十五分」／「一刻」、「三刻」'] != 0:
                            s_hour_writing += s_word_separator + ls_digit[int(s_minute_notation) // 15] + s_word_separator + d_lexicon['s 刻']
                    elif s_minute_notation == '30':
                        if d_setting['i 「三十分」／「半」'] != 0:
                            s_hour_writing += d_lexicon['s 半']
                    else:
                        s_minute_writing += s_write_integer(s_minute_notation) + s_word_separator + d_lexicon['s 分']
                else:
                    if d_setting['i 分鐘／五分鐘爲一箇字'] != 0 and int(s_minute_notation) % 5 == 0 and s_second_notation == '0':
                        if d_setting['i 「二箇字」／「兩箇字」'] != 0 and s_minute_notation == '10':
                            s_minute_writing += d_lexicon['s 兩']
                        else:
                            s_minute_writing += s_write_integer(str(int(s_minute_notation) // 5))
                        if d_setting['i 末位時間單位／_'] == 0:
                            s_minute_writing += s_word_separator
                            if d_setting['i 「個」／「箇」'] == 0:
                                s_minute_writing += d_lexicon['s 個']
                            else:
                                s_minute_writing += d_lexicon['s 箇']
                            s_minute_writing += s_word_separator + d_lexicon['s 字']
                    else:
                        s_minute_writing += s_write_integer(s_minute_notation)
                        if d_setting['i 末位時間單位／_'] != 0 and s_second_notation == '0' and int(s_minute_notation) < 10:
                            s_minute_writing = s_word_separator + d_lexicon['s 零'] + s_word_separator + s_minute_writing
                        else:
                            s_minute_writing += s_word_separator + d_lexicon['s 分']

                s_second_writing = ''
                if d_setting['i 「二秒」／「兩秒」'] != 0 and s_second_notation == '2':
                    s_second_writing = d_lexicon['s 兩'] + s_word_separator
                if d_setting['i 末位時間單位／_'] != 0:
                    if int(s_second_notation) < 10:
                        s_second_writing += d_lexicon['s 零'] + s_word_separator + ls_digit[int(s_second_notation)]
                    else:
                        s_second_writing += s_write_integer(s_second_notation)
                else:
                    if s_second_notation != '0':
                        s_second_writing += s_write_integer(s_second_notation) + s_word_separator + d_lexicon['s 秒']

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += s_word_separator + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += s_word_separator + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '。' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing = ls_magnitude[2] + s_syllable_separator + d_lexicon['s 分'] + s_syllable_separator + d_lexicon['s 之'] + s_word_separator + s_the_writing
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s 負'] + s_word_separator + s_the_writing
        s_writing += s_the_writing + '\n'

    s_writing.removeprefix('’')
    s_writing = s_writing.replace(' ’', ' ')
    s_writing = s_writing.replace('\n’', '\n')
    return s_writing
