def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/fra.yaml').read_text())
    if b_is_for_TTS:
        ls_script = ['latin']
    else:
        ls_script = d_setting['ls écriture']
    i_digit_grouping = 3
    s_writing = ''
    
    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/fra.yaml').read_text())['d '+s_the_script]
        s_the_writing = ''

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    if s_the_digit == '1':
                        if (i_the_magnitude == 2 or i_the_magnitude == 0 and i_the_archmagnitude in [1, 2] and int(s_the_digit_group) == 1):
                            return ''
                    return d_lexicon['ls chiffres'][int(s_the_digit)] + ' '

                def s_write_ten_and_unit():
                    if s_the_digit == '0':
                        return ''
                    elif s_the_digit == '1':
                        return d_lexicon['ls dix à dix-neuf'][int(s_the_digit_group[-1])] + ' '
                    elif int(s_the_digit) <= 6:
                        if s_the_digit_group.endswith('0'):
                            return d_lexicon['ls dizaines'][int(s_the_digit)] + ' '
                        elif s_the_digit_group.endswith('1'):
                            return d_lexicon['ls dizaines'][int(s_the_digit)] + '-' + d_lexicon['s et'] + '-' + d_lexicon['ls chiffres'][1] + ' '
                        return d_lexicon['ls dizaines'][int(s_the_digit)] + '-' + d_lexicon['ls chiffres'][int(s_the_digit_group[-1])] + ' '

                    if d_setting['i « quatre-vingts », et cétéra / « huitante », et cétéra ou …'] == 0 and int(s_the_digit) > 6:
                        if s_the_digit == '7':
                            return d_lexicon['ls dizaines'][6] + '-' + d_lexicon['ls dix à dix-neuf'][int(s_the_digit_group[-1])] + ' '
                        elif s_the_digit == '9':
                            return d_lexicon['ls chiffres'][4] + '-' + d_lexicon['ls dizaines'][2] + '-' + d_lexicon['ls dix à dix-neuf'][int(s_the_digit_group[-1])] + ' '
                        return d_lexicon['ls chiffres'][4] + '-' + d_lexicon['ls dizaines'][2] + d_lexicon['s s'] + '-' + d_lexicon['ls chiffres'][int(s_the_digit_group[-1])] + ' '
                    s_writing = ''
                    if s_the_digit == '8':
                        if d_setting['i « huitante », et cétéra / « octante », et cétéra'] == 0:
                            s_writing += d_lexicon['ls dizaines'][8]
                        else:
                            s_writing += d_lexicon['s octante']
                    if s_the_digit_group.endswith('0'):
                        return s_writing + ' '
                    elif s_the_digit_group.endswith('1'):
                        return s_writing + '-' + d_lexicon['s et'] + '-' + d_lexicon['ls chiffres'][1] + ' '
                    else:
                        return s_writing + '-' + d_lexicon['ls chiffres'][int(s_the_digit_group[-1])] + ' '
                    return s_writing

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    if s_the_digit == '1':
                        if int(s_the_digit_group) > 1:
                            return d_lexicon['s cent'] + ' '
                    return d_lexicon['ls chiffres'][int(s_the_digit)] + '-' + d_lexicon['s cent'] + d_lexicon['s s'] + ' '

                s_writing = ''
                if i_the_magnitude == 0:
                    s_writing += s_write_unit()
                elif i_the_magnitude == 1:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude == 2:
                    s_writing += s_write_hundred()
                if d_setting['i _ / « et » pour la place « 0 »'] != 0 and s_the_digit == '0' and i_the_magnitude > 0 and s_the_digit_group[-i_the_magnitude] != '0':
                    s_writing +=  d_lexicon['s et'] + ' '
                return s_writing

            def s_write_archmagnitude():
                if i_the_archmagnitude == 0 or int(s_the_digit_group) == 0:
                    return ''
                if i_the_archmagnitude > 1 and int(s_the_digit_group) > 1:
                    return d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + d_lexicon['s s'] + ' '
                return d_lexicon['ls archimagnitudes'][i_the_archmagnitude] + ' '

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
            i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
            while i_the_archmagnitude >= 0:
                s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                while i_the_magnitude >= 0:
                    s_the_digit = s_the_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude == 1 and s_the_digit != '0':
                        i_the_magnitude -= 1
                    i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            if len(s_notation) < 3:
                if int(s_notation) < 10:
                    if s_notation == '1':
                        return d_lexicon['s premier']
                    return d_lexicon['ls chiffres ordinaux'][int(s_notation)]
                if int(s_notation) < 20:
                    return d_lexicon['ls dix à dix-neuf ordinaux'][int(s_notation[-1])]
                if int(s_notation) < 60:
                    if s_notation.endswith('0'):
                        return d_lexicon['ls dizaines ordinaux'][int(s_notation[-2])]
                    if s_notation.endswith('1'):
                        return d_lexicon['ls dizaines'][int(s_notation[-2])] + '-' + d_lexicon['s et'] + '-' + d_lexicon['ls chiffres ordinaux'][int(s_notation[-1])]
                    return d_lexicon['ls dizaines'][int(s_notation[-2])] + '-' + d_lexicon['ls chiffres ordinaux'][int(s_notation[-1])]
                if d_setting['i « quatre-vingts », et cétéra / « huitante », et cétéra ou …'] == 0 and s_notation[-2] in ['7', '9']:
                    if s_notation.endswith('0'):
                        return d_lexicon['ls dizaines'][int(s_notation[-2]) - 1] + '-' + d_lexicon['s ordinaux dix à dix-neuf'][int(s_notation[-1])]
                    return d_lexicon['ls dizaines'][int(s_notation[-2])] + '-' + d_lexicon['s ordinaux dix à dix-neuf'][int(s_notation[-1])]
                if s_notation.endswith('0'):
                    return d_lexicon['ls dizaines ordinaux'][int(s_notation[-2])]
                if s_notation.endswith('1'):
                    return d_lexicon['ls dizaines'][int(s_notation[-2])] + '-' + d_lexicon['s et'] + '-' + d_lexicon['ls chiffres ordinaux'][int(s_notation[-1])]
                return d_lexicon['ls dizaines'][int(s_notation[-2])] + '-' + d_lexicon['ls chiffres ordinaux'][int(s_notation[-1])]
            s_first_two_places = s_notation[-2:]
            if int(s_first_two_places) == 0:
                if s_notation[-i_digit_grouping-1] != '0':
                    return s_write_integer(s_notation)[:-len(d_lexicon['ls archimagnitudes'][1])] + d_lexicon['s millième']
                return s_write_integer(s_notation) + d_lexicon['s ième']
            return s_write_integer(s_notation)[:-len(s_write_integer(s_first_two_places))] + s_write_ordinal_number(s_first_two_places)

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            if d_setting['i numéro comme nombres à deux chiffres / numéro comme nombres à un chiffre'] == 0:
                s_writing = ''
                if len(s_notation) % 2 == 1:
                    s_writing += d_lexicon['ls chiffres'][int(s_notation[0])] + ', '
                    s_notation = s_notation[1:]
                ls_digit_group = [s_notation[s_character:s_character + 2] for s_character in range(0, len(s_notation), 2)]
                for s_character in ls_digit_group:
                    if s_character[0] == '0':
                        s_writing += d_lexicon['ls chiffres'][0] + '-' + d_lexicon['ls chiffres'][int(s_character[1])]
                    else:
                        s_writing += s_write_integer(s_character)
                    s_writing += ', '
                return s_writing.strip(', ')
            for s_character in s_notation:
                s_writing += d_lexicon['ls chiffres'][int(s_character)] + '-'
            return s_writing.removesuffix('-')

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            s_numerator_writing = s_write_integer(s_numerator_notation)
            if s_denominator_notation == '2':
                s_denominator_writing = d_lexicon['s demi']
            elif s_denominator_notation == '4':
                s_denominator_writing = d_lexicon['s quart']
            else:
                s_denominator_writing = s_write_ordinal_number(s_denominator_notation)
            if s_numerator_notation != '1':
                s_denominator_writing += d_lexicon['s s']
            return s_numerator_writing + ' ' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + d_lexicon['s et'] + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += d_lexicon['ls chiffres'][int(s_character)] + '-'
            return s_integer_writing + ' ' + d_lexicon['s virgule'] + ' ' + s_decimal_writing.removesuffix('-')

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s moins'] + ' ' + s_write_integer(s_exponent_notation.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing + ' ' + d_lexicon['s fois'] + ' ' + d_lexicon['ls dix à dix-neuf'][0] + ' ' + d_lexicon['s puissance'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                s_year_writing = s_write_integer(s_year_notation)
                s_month_writing = d_lexicon['s mois'][int(s_month_notation)]
                if s_day_notation == '1':
                    s_day_writing = s_write_ordinal_number(s_day_notation)
                else:
                    s_day_writing = s_write_integer(s_day_notation)
                return d_lexicon['s le'] + ' ' + s_day_writing + ' ' + s_month_writing + ',' + ' ' + s_year_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')
                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s heure']
                        if s_hour_notation not in ['0', '1']:
                            s_hour_writing += d_lexicon['s s']
                        if d_setting['i _ / « pile »'] != 0:
                            s_hour_writing += ' ' + d_lexicon['s pile']
                    else:
                        if s_minute_notation == '45':
                            if d_setting['i « quinze », « quarante-cinq » / « et quart », « moins le quart »'] != 0:
                                if s_hour_notation == '23':
                                    s_hour_notation = '0'
                                else:
                                    s_hour_notation = str(int(s_hour_notation) + 1)
                        if s_hour_notation == '1':
                            s_hour_writing = d_lexicon['s une'] + ' ' + d_lexicon['s heure']
                        else:
                            s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s heure'] + d_lexicon['s s']
                        if s_minute_notation in ['15', '45']:
                            if d_setting['i « quinze », « quarante-cinq » / « et quart », « moins le quart »'] != 0:
                                if s_minute_notation == '15':
                                    s_hour_writing += ' ' + d_lexicon['s et'] + ' ' + d_lexicon['s quart']
                                else:
                                    s_hour_writing += ' ' + d_lexicon['s moins'] + ' ' + d_lexicon['s le'] + ' ' + d_lexicon['s quart']
                        elif s_minute_notation == '30':
                            if d_setting['i « trente » / « et demie »'] != 0:
                                s_hour_writing += ' ' + d_lexicon['s et'] + ' ' + d_lexicon['s demie']
                        else:
                            s_minute_writing += s_write_integer(s_minute_notation)
                else:
                    if s_hour_notation == '1':
                        s_hour_writing = d_lexicon['s une'] + ' ' + d_lexicon['s heure']
                    else:
                        s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s heure'] + d_lexicon['s s']
                    if s_minute_notation == '1':
                        s_minute_writing = d_lexicon['s une']
                    else:
                        s_minute_writing = s_write_integer(s_minute_notation)
                    if s_second_notation == '1':
                        s_second_writing = d_lexicon['s une']
                    else:
                        s_second_writing = s_write_integer(s_second_notation)

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += ' ' + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += ' ' + d_lexicon['s et'] + ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing += ' ' + d_lexicon['s pourcent']
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s moins'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
