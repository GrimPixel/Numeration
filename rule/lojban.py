def do(notation, notation_type, setting_identifier, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['lojban']
    if is_text_to_speech:
        script = ['latin']
    else:
        script = setting['script']
    final_writing = ''
    for x in script:
        current_script = yaml.load(open('rule/library/script decode.yaml'))[x]
        common_lexicon = yaml.load(open('rule/lexicon/lojban.yaml'))[current_script]['common']
        _space = common_lexicon['_space']

        def output_math_numeral(math_numeral_notation):
            math_numeral_lexicon = yaml.load(open('rule/lexicon/lojban.yaml'))[current_script]['math numeral']

            def write_integer(integer_notation):
                local_writing = ''
                if setting['integer and fraction and decimal – consecutive or grouped'] != 0:
                    focused_archmagnitude = (len(integer_notation) - 1) // setting['digit grouping']
                    while focused_archmagnitude >= 0:
                        current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                              setting['digit grouping'])
                        current_digit_group = str(int(current_digit_group))
                        if current_digit_group != '0' or integer_notation == '0':
                            local_writing += write.consecutive_digit(current_digit_group,
                                                                     common_lexicon['integer']['digit'])
                        if focused_archmagnitude > 0:
                            local_writing += common_lexicon['integer']['comma']
                        focused_archmagnitude -= 1
                else:
                    local_writing += write.consecutive_digit(integer_notation, common_lexicon['integer']['digit'])
                return local_writing

            def write_fraction(fraction_notation):
                numerator, denominator = fraction_notation.split('/')
                denominator = write_integer(denominator)
                if numerator == '1' and setting['numerator_1 – omit or keep'] == 0:
                    return math_numeral_lexicon['fraction'] + _space + denominator
                numerator = write_integer(numerator)
                return numerator + _space + math_numeral_lexicon['fraction'] + _space + denominator

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                if check.is_negative(mixed_number_notation):
                    return integer + _space + math_numeral_lexicon['mixed number']['minus'] + _space + fraction
                return integer + _space + math_numeral_lexicon['mixed number']['plus'] + _space + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                decimal = write_integer(decimal)
                if integer == '0' and setting['decimal integer part 0 – omit or keep'] == 0:
                    return math_numeral_lexicon['decimal'] + _space + decimal
                integer = write_integer(integer)
                return integer + _space + math_numeral_lexicon['decimal'] + _space + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)
                multiplier = multiplier.replace(_space, '')
                if check.is_negative(exponent):
                    exponent = common_lexicon['negative'] + _space + write_integer(exponent[1:]).replace(_space, '')
                else:
                    exponent = write_integer(exponent)
                scientific = math_numeral_lexicon['scientific notation']
                return exponent + _space + scientific + _space + multiplier

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                writing += _space + common_lexicon['percent']
            if is_negative:
                writing = common_lexicon['negative'] + _space + writing
            return writing

        def output_ordinal_number(ordinal_notation):
            return write.consecutive_digit(ordinal_notation, common_lexicon['integer']['digit']).replace(_space, '') + \
                   yaml.load(open('rule/lexicon/lojban.yaml'))[current_script]['ordinal']

        def output_nominal_number(nominal_number):
            return write.consecutive_digit(nominal_number, common_lexicon['integer']['digit']).replace(_space, '')

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/lojban.yaml'))[current_script]['date and time']
            year_notation, month_notation, day_notation = date_and_time_notation[0]
            hour_notation, minute_notation, second_notation = date_and_time_notation[1]

            date_writing = date_and_time_lexicon['number'] + _space + \
                           write.consecutive_digit(day_notation, common_lexicon['integer']['digit']) + _space + \
                           date_and_time_lexicon['separator'] + _space + \
                write.consecutive_digit(month_notation, common_lexicon['integer']['digit']) + _space + \
                           date_and_time_lexicon['separator'] + _space + \
                write.consecutive_digit(year_notation, common_lexicon['integer']['digit'])
            time_writing = date_and_time_lexicon['number'] + _space + \
                           write.consecutive_digit(hour_notation, common_lexicon['integer']['digit']) + _space + \
                           date_and_time_lexicon['separator'] + _space + \
                write.consecutive_digit(minute_notation, common_lexicon['integer']['digit']) + _space + \
                           date_and_time_lexicon['separator'] + _space + \
                write.consecutive_digit(second_notation, common_lexicon['integer']['digit'])
            if is_text_to_speech:
                return date_writing + '. ' + time_writing
            return date_writing + '\n' + time_writing

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
    return final_writing
