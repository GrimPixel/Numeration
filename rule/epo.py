def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/epo.yaml').read_text())
    if b_is_for_TTS:
        ls_script = ['latino']
    else:
        ls_script = d_setting['ls skripto']
    i_digit_grouping = 3
    s_writing = ''
    
    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/epo.yaml').read_text())['d '+s_the_script]
        if d_setting['i esperantaj ĉefgrandoj / tradiciaj ĉefgrandoj'] == 0:
            ls_archmagnitude = d_lexicon['ls esperantaj ĉefgrandoj']
        else:
            ls_archmagnitude = d_lexicon['ls tradiciaj ĉefgrandoj']
        s_the_writing = ''

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                if s_the_digit == '0':
                    if s_notation != '0':
                        return ''
                if s_the_digit == '1':
                    if i_the_magnitude == 0 and i_the_archmagnitude > 0 and int(s_the_digit_group) == 1:
                        return ''
                    elif i_the_magnitude > 0:
                        return d_lexicon['ls grandoj'][i_the_magnitude] + ' '
                if d_setting['i _ / “kaj” por lokoj “0”'] != 0 and s_the_digit == '0' and i_the_magnitude > 0 and s_the_digit_group[-i_the_magnitude] != '0':
                    return d_lexicon['ls ciferoj'][int(s_the_digit)] + d_lexicon['ls grandoj'][i_the_magnitude] + ' ' + d_lexicon['s kaj'] + ' '
                return d_lexicon['ls ciferoj'][int(s_the_digit)] + d_lexicon['ls grandoj'][i_the_magnitude] + ' '

            def s_write_archmagnitude():
                if i_the_archmagnitude == 0 or int(s_the_digit_group) == 0:
                    return ''
                if i_the_archmagnitude == 1 or int(s_the_digit_group) == 1:
                    return ls_archmagnitude[i_the_archmagnitude] + ' '
                return ls_archmagnitude[i_the_archmagnitude] + d_lexicon['s j'] + ' '

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
            i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
            while i_the_archmagnitude >= 0:
                s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                while i_the_magnitude >= 0:
                    s_the_digit = s_the_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude() + write_conjunction_in_vacant_place()
                    i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            s_writing = s_write_integer(s_notation)
            if s_writing.endswith(d_lexicon['s o'] + d_lexicon['s j']):
                return s_writing.removesuffix(d_lexicon['s o'] + d_lexicon['s j']) + d_lexicon['s a']
            elif s_writing.endswith(d_lexicon['s o']):
                return s_writing.removesuffix(d_lexicon['s o']) + d_lexicon['s a']
            return s_writing + d_lexicon['s a']

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            s_nominal_writing = ''
            for s_character in s_notation:
                s_nominal_writing += d_lexicon['ls ciferoj'][int(s_character)] + '-'
            return s_nominal_writing.removesuffix('-')

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            s_numerator_writing = s_write_integer(s_numerator_notation)
            s_denominator_writing = s_write_integer(s_denominator_notation).replace(' ', '-')
            if s_denominator_writing != s_denominator_writing.removesuffix(d_lexicon['s o']):
                s_denominator_writing = s_denominator_writing.removesuffix(d_lexicon['s o'])
            elif s_denominator_writing != s_denominator_writing.removesuffix(d_lexicon['s o'] + d_lexicon['s j']):
                s_denominator_writing = s_denominator_writing.removesuffix(d_lexicon['s o'] + d_lexicon['s j'])
            s_denominator_writing += d_lexicon['s ono']
            if int(s_denominator_notation) > 1:
                s_denominator_writing += d_lexicon['s j']
            return s_numerator_writing + ' ' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + d_lexicon['s kaj'] + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += d_lexicon['ls ciferoj'][int(s_character)] + '-'
            return s_integer_writing + ' ' + d_lexicon['s komo'] + ' ' + s_decimal_writing.removesuffix('-')

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s minus'] + ' ' + s_write_integer(s_exponent_notation.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing.replace(' ', '-') + ' ' + d_lexicon['s oble'] + ' ' + d_lexicon['ls grandoj'][1] + ' ' + d_lexicon['s alt'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                s_year_writing = s_write_integer(s_year_notation)
                s_month_writing = d_lexicon['s de'] + ' ' + d_lexicon['ls monatoj'][int(s_month_notation)]
                s_day_writing = d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_day_notation).replace(' ', '')
                return s_day_writing + ' ' + s_month_writing + ',' + ' ' + s_year_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')

                def s_write_minute_or_second(s_unit, s_notation):
                    s_minute_or_second = s_write_integer(s_notation)
                    if d_setting['i _ / “minutoj”, “sekundoj”'] != 0:
                        s_minute_or_second += ' ' + s_unit
                        if int(s_second_notation) > 1:
                            s_minute_or_second += d_lexicon['s j']
                    return s_minute_or_second

                def s_write_quarter_of_hour(preposition):
                    s_writing = d_lexicon['ls ciferoj'][4] + d_lexicon['s ono'] + ' ' + preposition + ' ' + d_lexicon['s la'] + ' '
                    if preposition == d_lexicon['s antaŭ']:
                        if s_hour_notation == '23':
                            return s_writing  + s_write_ordinal_number('0')
                        return s_writing + s_write_ordinal_number(str(int(s_hour_notation) + 1))
                    return s_writing + s_write_ordinal_number(s_hour_notation)

                def s_write_regular_hour_and_minute():
                    s_hour_writing = ''
                    s_minute_writing = ''
                    if d_setting['i horo kaj minuto / “antaŭ”, “post” la horo'] == 0:
                        s_hour_writing += d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_hour_notation).replace(' ', '')
                        s_minute_writing += s_write_minute_or_second(d_lexicon['s minuto'], s_minute_notation)
                    else:
                        if int(s_minute_notation) < 30:
                            s_hour_writing += d_lexicon['s post'] + ' ' + d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_hour_notation).replace(' ', '')
                            s_minute_writing += s_write_minute_or_second(d_lexicon['s minuto'], s_minute_notation)
                        else:
                            s_hour_writing += d_lexicon['s antaŭ'] + ' '
                            s_minute_writing += s_write_minute_or_second(d_lexicon['s minuto'], str(60 - int(s_minute_notation)))
                            if s_hour_notation == '23':
                                s_hour_writing += d_lexicon['s la'] + ' ' + s_write_ordinal_number('0').replace(' ', '')
                            else:
                                s_hour_writing += d_lexicon['s la'] + ' ' + s_write_ordinal_number(str(int(s_hour_notation) + 1)).replace(' ', '')
                    return [s_hour_writing, s_minute_writing]

                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_second_notation == '0':
                    if s_minute_notation == '0':
                        s_hour_writing += d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_hour_notation)
                    elif s_minute_notation == '15':
                        if d_setting['i “dek kvin”, “kvardek kvin” / “kvarono post”, “kvarono antaŭ”'] != 0:
                            s_hour_writing += s_write_quarter_of_hour(d_lexicon['s post'])
                    elif s_minute_notation == '30':
                        if d_setting['i “tridek” / “duono post” aŭ “duono antaŭ”'] != 0:
                            s_fraction_suffix = d_lexicon['s ono']
                            s_hour_writing = ''
                            if d_setting['i “duono post” / “duono antaŭ”'] == 0:
                                s_hour_writing += d_lexicon['ls ciferoj'][2] + s_fraction_suffix + ' ' + d_lexicon['s post'] + ' ' + d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_hour_notation).replace(' ', '')
                            else:
                                s_hour_writing += d_lexicon['ls ciferoj'][2] + s_fraction_suffix + ' ' + d_lexicon['s antaŭ'] + ' ' + d_lexicon['s la'] + ' '
                                if s_hour_notation == '23':
                                    s_hour_writing = '-1'
                                s_hour_writing += s_write_ordinal_number(str(int(s_hour_notation) + 1))
                    elif s_minute_notation == '45':
                        if d_setting['i “dek kvin”, “kvardek kvin” / “kvarono post”, “kvarono antaŭ”'] != 0:
                            s_hour_writing += s_write_quarter_of_hour(d_lexicon['s antaŭ'])
                    else:
                        s_hour_writing, s_minute_writing = s_write_regular_hour_and_minute()
                else:
                    s_hour_writing += d_lexicon['s la'] + ' ' + s_write_ordinal_number(s_hour_notation)
                    s_minute_writing += s_write_minute_or_second(d_lexicon['s minuto'], s_minute_notation)
                    s_second_writing += s_write_minute_or_second(d_lexicon['s sekundo'], s_second_notation)
                if d_setting['i _ / “minutoj”, “sekundoj”'] != 0:
                    s_hour_writing += ' ' + d_lexicon['s horo']

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    if d_setting['i horo kaj minuto / minuto “antaŭ”, “post” la horo'] != 0 and s_second_writing == '':
                        s_time_writing = s_minute_writing + ' ' + s_time_writing
                    else:
                        if d_setting['i “kaj” inter tempounuoj / _'] == 0:
                            s_time_writing += ' ' + d_lexicon['s kaj'] + ' ' + s_minute_writing
                            if s_second_writing != '':
                                s_time_writing += ' ' + d_lexicon['s kaj'] + ' ' + s_second_writing
                        else:
                            s_time_writing += ' ' + s_minute_writing
                            if s_second_writing != '':
                                s_time_writing += ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            if d_setting['i “elcento” / “procento”'] == 0:
                s_the_writing += ' ' + d_lexicon['s elcento']
            else:
                s_the_writing += ' ' + d_lexicon['s procento']
            if float(s_notation) != 1.0:
                s_the_writing += d_lexicon['s j']
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s minus'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
