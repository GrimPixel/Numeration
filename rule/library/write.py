def unified_sign(notation):
    notation = notation.replace('⁄', '/')
    notation = notation.replace('٪', '%')
    replacement = [',', '·', "'", '٫']
    for x in replacement:
        notation = notation.replace(x, '.')
    notation = notation.replace('−', '-')
    notation = notation.replace('E', 'e')
    return notation


def unified_date_separator(date):
    replacement = ['.', '‐', '‒', '/']
    for x in replacement:
        date = date.replace(x, '-')
    return date


def unified_time_separator(time):
    time = time.replace('.', ':')
    return time


def consecutive_digit(notation, digit, medium=''):
    writing = ''
    focused = len(notation)
    while focused > 0:
        name = digit[int(notation[-focused])] + medium
        writing += name
        focused -= 1
    if medium == '':
        return writing
    return writing[:-1]
