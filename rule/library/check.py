import os.path
from ruamel.yaml import YAML
yaml = YAML(typ='safe')


def language_setting(language_setting):
    available_languages = list(yaml.load(open('rule/library/language decode.yaml')).keys())
    language_setting = language_setting.split()
    if len(language_setting) == 0:
        return False
    for x in language_setting:
        language_code = x[:3]
        if language_code not in available_languages:
            return False
        setting_identifier = x[3:]
        if setting_identifier == '':
            setting_identifier = '0'
        if not os.path.isfile(f'rule/setting/{setting_identifier}.yaml'):
            return False
        language_name = yaml.load(open('rule/library/language decode.yaml'))[language_code]
        if isinstance(language_name, list):
            language_name = language_name[1]
        if language_name not in list(yaml.load(open(f'rule/setting/{setting_identifier}.yaml')).keys()):
            return False
    return True


def is_negative(notation):
    if notation.count('-') == 1:
        return notation[0] == '-' and len(notation) > 1
    elif notation.count('-') == 2:
        if notation.count('/') == 1 and notation[0] == '-' and len(notation) > 1:
            notation = notation.split('-')
            return notation[0] == '' and is_natural_number(notation[1]) and is_non_negative_fraction(notation[2])
        elif notation[0] == '-' and notation.count('e-') == 1:
            notation = notation.split('e')
            return is_negative(notation[0]) and is_negative(notation[1])
    return False


def is_percent(notation):
    return len(notation) > 1 and notation[-1] == '%'


def is_natural_number(notation):
    return notation.isdigit()


def is_non_negative_fraction(notation):
    if notation.count('/') == 1:
        part = notation.split('/')
        return is_natural_number(part[0]) and is_natural_number(part[1])
    return False


def is_mixed_number(notation, is_negative):
    if is_negative:
        notation = notation.replace('-', '+')
    if notation.count('+') == 1:
        part = notation.split('+')
        return is_natural_number(part[0]) and is_non_negative_fraction(part[1])
    return False


def is_non_negative_decimal(notation):
    if notation.count('.') == 1:
        part = notation.split('.')
        return is_natural_number(part[0]) and is_natural_number(part[1])
    return False


def is_non_negative_scientific_notation(notation):
    if notation.count('e') == 1:
        part = notation.split('e')
        if is_natural_number(part[1]) or \
                is_negative(part[1]) and is_natural_number(part[1][1:]):
            if is_natural_number(part[0]) or \
                    is_negative(part[0]) and is_natural_number(part[0][1:]):
                return abs(int(part[0])) < 10
            elif is_non_negative_decimal(part[0]) or \
                    is_negative(part[0]) and is_non_negative_decimal(part[0][1:]):
                return abs(float(part[0])) < 10
    return False


def date(date_notation, calendar='common'):
    if len(date_notation) == 3 and '' not in date_notation:
        for x in date_notation:
            if not x.isdigit():
                return False
        if calendar == 'common':
            return 1 <= int(date_notation[1]) <= 12 and 1 <= int(date_notation[2]) <= 31
    return False


def time(time_notation, clock='common'):
    if len(time_notation) == 3 and '' not in time_notation:
        for x in time_notation:
            if not x.isdigit():
                return False
        if clock == 'common':
            return 0 <= int(time_notation[0]) <= 23 and 0 <= int(time_notation[1]) <= 59 and 0 <= int(time_notation[2]) <= 59
    return False

