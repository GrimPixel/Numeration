from ruamel.yaml import YAML
yaml = YAML(typ='safe')


def replacement(notation, note):
    for x in note:
        notation = notation.replace(x, str(note.index(x)))
    return notation


def plural_lists_replacement(notation, note):
    for x in note:
        for y in x:
            notation = notation.replace(y, str(x.index(y)))
    return notation


def common_writing_system_with_0_to_9_to_western_arabic(notation, numeral_system):
    note = yaml.load(open('rule/lexicon/symbol/note.yaml'))[numeral_system]
    return replacement(notation, note)


def common_writing_system_with_0_to_9_and_variants_to_western_arabic(notation, numeral_system):
    if numeral_system == 'eastern arabic':
        mark = yaml.load(open('rule/lexicon/symbol/sign.yaml'))
        notation = notation.replace(mark['percent']['arabic'],
                                    mark['percent']['common'])
        notation = notation.replace(mark['separator']['arabic decimal separator'],
                                    mark['separator']['period'])
        note = yaml.load(open('rule/lexicon/symbol/note.yaml'))['eastern arabic']
        note = [note['common'], note['variant']]
        return plural_lists_replacement(notation, note)
    return notation
