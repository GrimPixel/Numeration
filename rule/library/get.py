import re
from ruamel.yaml import YAML
yaml = YAML(typ='safe')


def common_maximal_length(language_setting):
    maximal_length = []
    for x in language_setting:
        setting_identifier = x[1]
        language_name = x[0]
        setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))
        if isinstance(language_name, list):
            language_maximal_length = setting[language_name[1]]['maximal length']
        else:
            language_maximal_length = setting[language_name]['maximal length']
        maximal_length.append(language_maximal_length)
    return min(maximal_length)


def notation_numeral_system(notation):
    if ' ' not in notation:
        character_set = yaml.load(open('rule/library/character set.yaml'))
        for x in character_set.keys():
            if re.match(x, notation):
                return character_set[x]


def notation_type(notation):
    import rule.library.check as check
    if check.is_natural_number(notation) or \
            check.is_negative(notation) and check.is_natural_number(notation[1:]):
        return 'integer'
    elif check.is_natural_number(notation[:-1]) and check.is_percent(notation) or \
            check.is_negative(notation) and check.is_natural_number(notation[1:-1]) and check.is_percent(notation):
        return 'integer percent'
    elif check.is_non_negative_fraction(notation) or \
            check.is_negative(notation) and check.is_non_negative_fraction(notation[1:]):
        return 'fraction'
    elif check.is_mixed_number(notation, False) or \
            check.is_negative(notation) and check.is_mixed_number(notation[1:], True):
        return 'mixed number'
    elif check.is_non_negative_decimal(notation) or \
            check.is_negative(notation) and check.is_non_negative_decimal(notation[1:]):
        return 'decimal'
    elif check.is_non_negative_decimal(notation[:-1]) and check.is_percent(notation) or \
            check.is_negative(notation) and check.is_non_negative_decimal(notation[1:-1]) and check.is_percent(notation):
        return 'decimal percent'
    elif check.is_non_negative_scientific_notation(notation) or \
            check.is_negative(notation) and check.is_non_negative_scientific_notation(notation[1:]):
        return 'scientific notation'
    return ''


def digit_group(notation, focused_archmagnitude, digit_grouping):
    if isinstance(digit_grouping, int):
        if focused_archmagnitude == 0:
            return notation[-digit_grouping:]
        focused = (focused_archmagnitude+1) * digit_grouping
        return notation[-focused:-focused + digit_grouping]

    if focused_archmagnitude == 0:
        return notation[-digit_grouping[0]:]
    focused = 0
    for x in range(focused_archmagnitude+1):
        if x < len(digit_grouping):
            focused += digit_grouping[x]
        else:
            focused += digit_grouping[-1]
    if focused_archmagnitude < len(digit_grouping):
        return notation[-focused:-focused + digit_grouping[focused_archmagnitude]]
    return notation[-focused:-focused + digit_grouping[-1]]


def language_code_for_text_to_speech_engine(language_name, engine):
    if isinstance(language_name, list):
        language_name = language_name[0]
    language_name_to_code = yaml.load(open('rule/library/language encode.yaml'))[engine]
    for x in language_name_to_code.keys():
        if language_name == x:
            return language_name_to_code[x]
    return ''
