def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/jpn.yaml').read_text())
    if b_is_for_TTS:
        ls_script = ['仮名']
    else:
        ls_script = d_setting['ls 文字']
    i_digit_grouping = 4
    s_writing = ''
    
    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/jpn.yaml').read_text())['d '+s_the_script]
        s_the_writing = ''

        if s_the_script == '仮名交じり文':
            if d_setting['i 小字／大字'] == 0:
                ls_digit = d_lexicon['ls 小字數字']
                ls_magnitude = d_lexicon['ls 小字桁']
            else:
                ls_digit = d_lexicon['ls 大字數字']
                ls_magnitude = d_lexicon['ls 大字桁']
        elif s_the_script == '仮名':
            ls_digit = d_lexicon['ls 數字']
            if d_setting['i 「ゼロ」／「れい」'] != 0:
                ls_digit[0] = d_lexicon['s れい']
            if d_setting['i 「よん」／「し」'] != 0:
                ls_digit[4] = d_lexicon['s し']
            if d_setting['i 「なな」／「しち」'] != 0:
                ls_digit[7] = d_lexicon['s しち']
            if d_setting['i 「きゅう」／「く」'] != 0:
                ls_digit[9] = d_lexicon['s く']
            ls_ten = d_lexicon['s 十']
            ls_hundred = d_lexicon['ls 百']
            if d_setting['i 「百」、「千」／「一百」、「一千」'] != 0:
                ls_hundred[1] = d_lexicon['s 一百']
            if d_setting['i 「くひゃく」／「きゅうひゃく」'] != 0:
                ls_hundred[9] = d_lexicon['s きゅうひゃく']
            ls_thousand = d_lexicon['ls 千']
            if d_setting['i 「百」、「千」／「一百」、「一千」'] != 0:
                ls_thousand[1] = d_lexicon['s 一千']
            if d_setting['i 「よんせん」／「しせん」'] != 0:
                ls_thousand[4] = d_lexicon['s しせん']

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                if s_the_script == '仮名交じり文':
                    s_writing = ''
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    elif s_the_digit == '1':
                        if i_the_magnitude == 1:
                            return ''
                        if d_setting['i 「百」、「千」／「一百」、「一千」'] == 0:
                            return ''
                    else:
                        s_writing += ls_digit[int(s_the_digit)]
                    if i_the_magnitude > 0 and s_the_digit != '0':
                        s_writing += ls_magnitude[i_the_magnitude]
                    return s_writing
                elif s_the_script == '仮名':
                    if i_the_magnitude == 0:
                        if s_the_digit == '0':
                            if s_notation != '0' or i_the_archmagnitude > 0:
                                return ''
                        return ls_digit[int(s_the_digit)]
                    if s_the_digit == '0':
                        return ''
                    if i_the_magnitude == 1:
                        if s_the_digit == '1':
                            return ls_ten
                        return ls_digit[int(s_the_digit)] + ls_ten
                    if i_the_magnitude == 2:
                        return ls_hundred[int(s_the_digit)]
                    return ls_thousand[int(s_the_digit)]

            def s_write_archmagnitude():
                if i_the_archmagnitude > 0:
                    if int(s_the_digit_group) > 0:
                        if s_the_script == '仮名交じり文':
                            return d_lexicon['ls 主桁'][i_the_archmagnitude]
                        elif s_the_script == '仮名':
                            if i_the_archmagnitude in [3, 6, 10]:
                                return d_lexicon['ls 主桁さた行接頭'][int(s_the_digit)] + d_lexicon['ls 主桁'][i_the_archmagnitude]
                            elif i_the_archmagnitude in [4, 8, 9]:
                                return d_lexicon['ls 主桁か行接頭'][int(s_the_digit)] + d_lexicon['ls 主桁'][i_the_archmagnitude]
                            if s_the_digit == '0':
                                return d_lexicon['ls 主桁'][i_the_archmagnitude]
                            return ls_digit[int(s_the_digit)] + d_lexicon['ls 主桁'][i_the_archmagnitude]
                return ''

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
            i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
            while i_the_archmagnitude >= 0:
                s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                while i_the_magnitude >= 0:
                    s_the_digit = s_the_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping - 1
            return s_writing

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            return s_write_integer(s_notation) + d_lexicon['s 番目']

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            s_writing = ''
            for s_character in s_notation:
                s_writing += ls_digit[int(s_character)]
            return s_writing

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            s_numerator_writing = s_write_integer(s_numerator_notation)
            if s_the_script == '仮名交じり文':
                s_denominator_writing = s_write_integer(s_denominator_notation) + d_lexicon['ls 分']
            elif s_the_script == '仮名':
                s_denominator_writing = s_write_integer(s_denominator_notation) + d_lexicon['s ぶん']
            return s_denominator_writing + d_lexicon['s の'] + s_numerator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            if d_setting['i 「と」／「箇」'] == 0:
                return s_integer_writing + d_lexicon['s と'] + s_fraction_writing
            return s_integer_writing + d_lexicon['s 箇'] + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += ls_digit[int(s_character)]
            return s_integer_writing + d_lexicon['s 点'] + s_decimal_writing

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.isdecimal():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s マイナス'] + s_write_integer(s_exponent_notation.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            if s_the_script == '仮名交じり文':
                return s_coefficient_writing + d_lexicon['s 掛ける'] + ls_magnitude[1] + d_lexicon['s の'] + s_exponent_writing + d_lexicon['s 乗']
            elif s_the_script == '仮名':
                return s_coefficient_writing + d_lexicon['s 掛ける'] + ls_ten + d_lexicon['s の'] +s_exponent_writing + d_lexicon['s 乗']

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                if s_year_notation == '1':
                    s_year_writing =  d_lexicon['s 元'] + d_lexicon['s 年']
                else:
                    s_year_writing =  s_write_integer(s_year_notation) + d_lexicon['s 年']
                if s_the_script == '仮名交じり文':
                    if d_setting['i グレゴリオ暦／和暦'] == 0:
                        s_month_writing =  s_write_integer(s_month_notation) + d_lexicon['s 月']
                    else:
                        s_month_writing =  d_lexicon['ls 和暦月'][int(s_month_notation)]
                    s_day_writing = s_write_integer(s_day_notation) + d_lexicon['s 日']
                elif s_the_script == '仮名':
                    if d_setting['i グレゴリオ暦／和暦'] == 0:
                        ls_month_name = d_lexicon['s 月']
                        if s_month_notation == '7':
                            if d_setting['i 「しちがつ」／「なながつ」'] == 1:
                                ls_month_name[7] = d_lexicon['s なながつ']
                        s_month_writing =  ls_month_name[int(s_month_notation)]
                    else:
                        s_month_writing =  d_lexicon['ls 和暦月'][int(s_month_notation)]
                    if s_day_notation == '1':
                        if d_setting['i 「ついたち」／「いちにち」'] != 0:
                            s_day_writing =  d_lexicon['s いちにち']
                    elif s_day_notation == '7':
                        if d_setting['i 「なのか」／「なぬか」'] != 0:
                            s_day_writing =  d_lexicon['s なぬか']
                    elif s_day_notation == '24':
                        if d_setting['i 「にじゅうよっか」／「にじゅうよんにち」'] != 0:
                            s_day_writing =  d_lexicon['s にじゅうよんにち']
                    elif s_day_notation == '30':
                        if d_setting['i 「さんじゅうにち」／「みそか」'] != 0:
                            s_day_writing =  d_lexicon['s みそか']
                    else:
                        s_day_writing =  d_lexicon['s 日'][int(s_day_notation)]
                return s_year_writing + s_month_writing + s_day_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')

                s_hour_writing = ''
                s_minute_writing = ''
                s_second_writing = ''
                if s_the_script == '仮名交じり文':
                    s_hour_writing += s_write_integer(s_hour_notation) + d_lexicon['ls 時']
                    if s_minute_notation == '30' and s_second_notation == '0':
                        if d_setting['i 「三十分」／「半」'] != 0:
                            s_hour_writing += d_lexicon['s 半']
                    elif s_minute_notation != '0':
                        s_minute_writing += s_write_integer(s_minute_notation) + d_lexicon['ls 分']
                elif s_the_script == '仮名':
                    if d_setting['i 「しちじ」、「じゅうしちじ」／「ななじ」、「じゅうななじ」'] != 0:
                        if s_hour_notation == '7':
                            s_hour_writing +=  d_lexicon['s ななじ']
                        elif s_hour_notation == '17':
                            s_hour_writing +=  d_lexicon['s じゅうななじ']
                    s_hour_writing += d_lexicon['ls 時'][int(s_hour_notation)]
                    if s_minute_notation == '30' and s_second_notation == '0':
                        if d_setting['i 「三十分」／「半」'] != 0:
                            s_hour_writing += d_lexicon['s 半']
                    elif int(s_minute_notation) <= 10:
                        if s_minute_notation == '3':
                            if d_setting['i 「さんぷん」、「よんぷん」、「はっぷん」／「さんふん」、「よんふん」、「はちふん」'] != 0:
                                s_minute_writing += d_lexicon['s さんふん']
                        elif s_minute_notation == '4':
                            if d_setting['i 「さんぷん」、「よんぷん」、「はっぷん」／「さんふん」、「よんふん」、「はちふん」'] != 0:
                                s_minute_writing += d_lexicon['s よんふん']
                        elif s_minute_notation == '7':
                            if d_setting['i 「ななふん」／「しちふん」'] != 0:
                                s_minute_writing += d_lexicon['s しちふん']
                        elif s_minute_notation == '8':
                            if d_setting['i 「さんぷん」、「よんぷん」、「はっぷん」／「さんふん」、「よんふん」、「はちふん」'] != 0:
                                s_minute_writing += d_lexicon['s はちふん']
                        else:
                            s_minute_writing += d_lexicon['ls 分'][int(s_minute_notation)]
                    elif int(s_minute_notation) % 10 == 0:
                        if d_setting['i 「じゅっぷん」／「じっぷん」'] != 0:
                            s_minute_writing += d_lexicon['ls じゅっぷん'][int(s_minute_notation) // 10]
                        else:
                            s_minute_writing += d_lexicon['ls じっぷん'][int(s_minute_notation) // 10]
                    else:
                        s_minute_writing += s_write_integer(str(int(s_minute_notation) // 10)) + ls_ten + d_lexicon['ls 分'][int(s_minute_notation) % 10]
                if s_second_notation != '0':
                    s_second_writing += s_write_integer(s_second_notation) + d_lexicon['s 秒']

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '。' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            percent = d_lexicon['s パーセント']
            s_the_writing += percent
        if b_is_negative:
            s_notation = '-' + s_notation
            negative = d_lexicon['s マイナス']
            s_the_writing = negative + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
