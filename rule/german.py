def do(notation, notation_type, setting_identifier, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['german']
    if is_text_to_speech:
        script = ['Latn']
    else:
        script = setting['script']
    final_writing = ''
    for x in script:
        current_script = yaml.load(open('rule/library/script decode.yaml'))[x]
        common_lexicon = yaml.load(open('rule/lexicon/german.yaml'))[current_script]['common']
        
        _space = common_lexicon['_space']

        digit = common_lexicon['integer']['digit']['prevalent']
        if setting['2 – prevalent or alternative'] != 0:
            digit[2] = common_lexicon['integer']['digit']['alternative']
        ten_to_nineteen = common_lexicon['integer']['ten to nineteen']['prevalent']
        tens = common_lexicon['integer']['tens']['prevalent']
        if setting['15 and 50 – prevalent or alternative'] == 1:
            ten_to_nineteen[5] = common_lexicon['integer']['ten to nineteen']['alternative']
            tens[5] = common_lexicon['integer']['tens']['alternative']['50']
        if setting['30 prevalent or alternative'] == 1:
            tens[3] = common_lexicon['integer']['tens']['alternative']['30']
        hundred = common_lexicon['integer']['hundred']
        archmagnitude = common_lexicon['integer']['archmagnitude']['word']

        def write_integer(integer_notation):
            def determine_initial_focused():
                focused_archmagnitude = (len(integer_notation) - 1) // setting['digit grouping']
                focused_magnitude = (len(integer_notation) - 1) % setting['digit grouping']
                return [focused_archmagnitude, focused_magnitude]

            def determine_focused_magnitude_change(focused_magnitude):
                if focused_magnitude == 1 and current_digit != '0':
                    focused_magnitude -= 2
                else:
                    focused_magnitude -= 1
                return focused_magnitude

            def renew_focused_magnitude():
                return setting['digit grouping'] - 1

            def write_digit(current_digit, focused_magnitude):
                if current_digit == '0' and integer_notation != '0':
                    return ''
                elif current_digit == '1':
                    if focused_magnitude == 0 and focused_archmagnitude == 0 and int(
                            current_digit_group) % 100 == 1:
                        return digit[int(current_digit)]
                    if focused_magnitude == 2 and setting['one in 100 and 1000 – omit or keep'] == 0:
                        return ''
                    if focused_magnitude == 0 and focused_archmagnitude == 1 and \
                            setting['one in 100 and 1000 – omit or keep'] == 0 and int(current_digit_group) == 1:
                        return ''
                    if focused_magnitude == 0 and focused_archmagnitude > 1 and int(current_digit_group) % 100 == 1:
                        return common_lexicon['singular']['eine']
                    return common_lexicon['singular']['ein']
                return digit[int(current_digit)]

            def write_ten_to_nineteen():
                return ten_to_nineteen[int(current_digit_group[-1])]

            def write_tens():
                return tens[int(current_digit)]

            def write_hundred():
                return hundred

            def write_digit_and_magnitude(current_digit, focused_magnitude):
                digit_and_magnitude_writing = ''
                if focused_magnitude == 0:
                    digit_and_magnitude_writing += write_digit(current_digit, focused_magnitude)
                elif focused_magnitude == 1:
                    if current_digit != '0':
                        if current_digit == '1':
                            digit_and_magnitude_writing += write_ten_to_nineteen()
                        else:
                            unit = current_digit_group[-1]
                            digit_and_magnitude_writing += write_digit(current_digit_group[-1], 0)
                            if unit != '0':
                                digit_and_magnitude_writing += common_lexicon['conjunction']
                            if setting['vacant place in zeroth archmagnitude – without or with conjunction'] != 0:
                                next_digit = current_digit_group[-focused_magnitude]
                                if current_digit == '0' and focused_magnitude > 0 and focused_archmagnitude == 0 and \
                                        next_digit != '0':
                                    digit_and_magnitude_writing += common_lexicon['conjunction']
                            digit_and_magnitude_writing += write_tens()
                else:
                    if current_digit != '0':
                        digit_and_magnitude_writing += write_digit(current_digit,
                                                                   focused_magnitude) + write_hundred()
                return digit_and_magnitude_writing

            def write_archmagnitude():
                if focused_archmagnitude == 0 or int(current_digit_group) == 0:
                    return ''
                archmagnitude_writing = ''
                if focused_archmagnitude == 1:
                    archmagnitude_writing += archmagnitude[focused_archmagnitude]
                else:
                    if int(current_digit_group) > 1:
                        archmagnitude_writing += _space + archmagnitude[focused_archmagnitude]
                        if archmagnitude[focused_archmagnitude][-1] == 'n':
                            archmagnitude_writing += common_lexicon['integer']['archmagnitude']['plural'][
                                'en']
                        else:
                            archmagnitude_writing += common_lexicon['integer']['archmagnitude']['plural']['n']
                    else:
                        archmagnitude_writing += _space + archmagnitude[focused_archmagnitude]
                return archmagnitude_writing + _space

            writing = ''
            focused_archmagnitude, focused_magnitude = determine_initial_focused()
            while focused_archmagnitude >= 0:
                current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                      setting['digit grouping'])
                while focused_magnitude >= 0:
                    current_digit = current_digit_group[-focused_magnitude - 1]
                    writing += write_digit_and_magnitude(current_digit, focused_magnitude)
                    focused_magnitude = determine_focused_magnitude_change(focused_magnitude)
                focused_magnitude = renew_focused_magnitude()
                writing += write_archmagnitude()
                focused_archmagnitude -= 1
            if writing[-1] == _space:
                return writing[:-1]
            return writing

        def write_ordinal(ordinal_notation):
            ordinal_lexicon = yaml.load(open('rule/lexicon/german.yaml'))[current_script]['ordinal']
            first_ten_and_unit = ordinal_notation[-2:]
            unit = first_ten_and_unit[-1]
            if ordinal_notation == first_ten_and_unit:
                if 0 < int(first_ten_and_unit) < 10:
                    if setting['2 – prevalent or alternative'] == 1 and unit == '2':
                        return ordinal_lexicon['unit']['alternative']
                    return ordinal_lexicon['unit']['prevalent'][int(unit)]
                if int(first_ten_and_unit) < 20:
                    return write_integer(ordinal_notation).replace(_space, '').lower() + ordinal_lexicon['suffix']
                return write_integer(ordinal_notation).replace(_space, '').lower() + \
                       ordinal_lexicon['interfix'] + ordinal_lexicon['suffix']
            if int(first_ten_and_unit) == 0:
                return write_integer(ordinal_notation).replace(_space, '').lower() + \
                       ordinal_lexicon['interfix'] + ordinal_lexicon['suffix']
            return write_integer(ordinal_notation)[:-len(write_integer(first_ten_and_unit))] \
                       .replace(_space, '').lower() + write_ordinal(first_ten_and_unit)

        def output_math_numeral(math_numeral_notation):
            math_numeral_lexicon = yaml.load(open('rule/lexicon/german.yaml'))[current_script]['math numeral']

            def write_fraction(fraction_notation):
                numerator, denominator = fraction_notation.split('/')
                one = common_lexicon['singular']
                if numerator == '1':
                    numerator = one['ein']
                else:
                    numerator = write_integer(numerator)
                fraction = common_lexicon['fraction']
                if denominator == '2':
                    denominator = fraction['half']
                else:
                    denominator = write_ordinal(denominator)[:-2]
                    denominator = denominator[0].upper() + denominator[1:] + fraction['suffix']
                return numerator + _space + denominator

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                return integer + _space + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                integer = write_integer(integer)
                decimal = write.consecutive_digit(decimal, digit)
                return integer + _space + math_numeral_lexicon['decimal'] + _space + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)
                if check.is_negative(exponent):
                    exponent = common_lexicon['negative'] + _space + write_integer(exponent[1:])
                else:
                    exponent = write_integer(exponent)
                return multiplier + _space + math_numeral_lexicon['scientific notation']['multiply'] + _space + \
                       ten_to_nineteen[0] + _space + math_numeral_lexicon['scientific notation']['high'] + _space +\
                       exponent

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                writing += _space + common_lexicon['percent']
            if is_negative:
                writing = common_lexicon['negative'] + _space + writing
            return writing

        def output_ordinal_number(ordinal_notation):
            return write_ordinal(ordinal_notation)

        def output_nominal_number(nominal_notation):
            if setting['nominal 2 – preferred or alternative'] == 0:
                digit[2] = common_lexicon['integer']['digit']['alternative']
            return write.consecutive_digit(nominal_notation, digit)

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/german.yaml'))[current_script]['date and time']
            
            def write_date():
                year_notation, month_notation, day_notation = date_and_time_notation[0]
                if 0 <= int(year_notation) <= 100:
                    year_writing = common_lexicon['article']['neuter'] + _space + \
                                   date_and_time_lexicon['year'] + _space + \
                                   write_integer(year_notation)
                else:
                    year_writing = write_integer(year_notation)
                if setting['month – name or order'] == 0:
                    month_writing = date_and_time_lexicon['month'][int(month_notation)]
                else:
                    month_writing = write_ordinal(month_notation)
                day_writing = write_ordinal(day_notation)
                return common_lexicon['article']['masculine'] + _space + day_writing + _space + \
                       month_writing + date_and_time_lexicon['_comma'] + _space + year_writing

            def write_time():
                def write_exact_hour():
                    if setting['exact hour – without or with sharp'] != 0:
                        return date_and_time_lexicon['sharp'] + _space + \
                               write_integer(hour_notation) + _space + date_and_time_lexicon['o clock']
                    return write_integer(hour_notation) + _space + date_and_time_lexicon['o clock']

                def write_half_hour():
                    if hour_notation == '23':
                        return date_and_time_lexicon['half'] + _space + write_integer('0')
                    return date_and_time_lexicon['half'] + _space + write_integer(str(int(hour_notation) + 1))

                def write_quarter_of_hour_with_preposition(hour_preposition):
                    hour = digit[4].capitalize() + common_lexicon['fraction']['suffix'] + _space + \
                           hour_preposition + _space
                    if hour_preposition == date_and_time_lexicon['preposition']['vor']:
                        if hour_notation == '23':
                            return hour + write_integer('0')
                        return hour + write_integer(str(int(hour_notation) + 1))
                    return hour + write_integer(str(int(hour_notation)))

                def write_quarter_of_hour_without_preposition():
                    if minute_notation == '15':
                        hour = digit[4] + common_lexicon['fraction']['suffix'] + _space
                    else:
                        hour = digit[3] + digit[4] + common_lexicon['fraction']['suffix'] + _space
                    if hour_notation == '23':
                        return hour + write_integer('0')
                    return hour + write_integer(str(int(hour_notation) + 1))

                def write_preposition_with_half_hour():
                    if int(minute_notation) < 30:
                        minute_writing = write_integer(minute_notation)
                        hour_writing = date_and_time_lexicon['preposition']['nach'] + _space + \
                                       write_integer(hour_notation)
                    else:
                        minute_writing = write_integer(str(60 - int(minute_notation)))
                        hour_writing = date_and_time_lexicon['preposition']['vor'] + _space
                        if hour_notation == '23':
                            hour_writing += write_integer('0')
                        else:
                            hour_writing += write_integer(str(int(hour_notation) + 1))
                    return [hour_writing, minute_writing]

                def write_regular_time():
                    hour_writing = ''
                    minute_writing = ''
                    if setting['time – without or with preposition'] == 0:
                        hour_writing = write_integer(hour_notation) + _space + date_and_time_lexicon['o clock']
                        minute_writing = write_integer(minute_notation)
                    else:
                        if int(minute_notation) < 30:
                            hour_writing += date_and_time_lexicon['preposition']['nach'] + _space + \
                                            write_integer(hour_notation)
                            minute_writing += write_integer(minute_notation)
                        else:
                            hour_writing += date_and_time_lexicon['preposition']['vor'] + _space
                            minute_writing += write_integer(str(60 - int(minute_notation)))
                            if hour_notation == '23':
                                hour_writing += write_integer('0')
                            else:
                                hour_writing += write_integer(str(int(hour_notation) + 1))
                    return [hour_writing, minute_writing]

                def write_units():
                    if second_notation == '0':
                        hour_writing = ''
                        minute_writing = ''
                        second_writing = ''
                        if minute_notation == '0':
                            hour_writing += write_exact_hour()
                            return [hour_writing, minute_writing, second_writing]
                        elif minute_notation == '15' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            if setting['minute 15 and 45 expressed by quarter hour – with or without preposition'] == 0:
                                hour_writing += write_quarter_of_hour_with_preposition(
                                    date_and_time_lexicon['preposition']['nach'])
                            else:
                                hour_writing += write_quarter_of_hour_without_preposition()
                        elif minute_notation == '30' and setting['minute 30 – number or half hour'] != 0:
                            hour_writing += write_half_hour()
                        elif minute_notation == '45' and setting['minute 15 and 45 – number or quarter hour'] != 0:
                            if setting['minute 15 and 45 expressed by quarter hour – with or without preposition'] == 0:
                                hour_writing += write_quarter_of_hour_with_preposition(
                                    date_and_time_lexicon['preposition']['vor'])
                            else:
                                hour_writing += write_quarter_of_hour_without_preposition()
                        if setting['time with preposition – include or exclude half hour'] == 0:
                            hour_writing, minute_writing = write_regular_time()
                        else:
                            hour_writing, minute_writing = write_preposition_with_half_hour()
                    else:
                        hour_writing = write_integer(hour_notation) + _space + date_and_time_lexicon['o clock']
                        minute_writing = write_integer(minute_notation)
                        second_writing = write_integer(second_notation)
                    return [hour_writing, minute_writing, second_writing]

                hour_notation, minute_notation, second_notation = date_and_time_notation[1]
                hour_writing, minute_writing, second_writing = write_units()
                time_writing = hour_writing
                if minute_writing != '':
                    if setting['time – without or with preposition'] != 0 and second_writing == '':
                        time_writing = minute_writing + _space + time_writing
                    else:
                        time_writing += _space + minute_writing + _space + second_writing
                return time_writing
            if is_text_to_speech:
                return write_date() + '. ' + write_time()
            return write_date() + '\n' + write_time()

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
    return final_writing
