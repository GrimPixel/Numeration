def do(notation, notation_type, setting_identifier, is_text_to_speech):
    import rule.library.check as check
    import rule.library.get as get
    import rule.library.write as write
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    setting = yaml.load(open(f'rule/setting/{setting_identifier}.yaml'))['english']
    if is_text_to_speech:
        script = ['Latn']
    else:
        script = setting['script']
    final_writing = ''
    for x in script:
        current_script = yaml.load(open('rule/library/script decode.yaml'))[x]
        common_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['common']

        _space = common_lexicon['_space']
        if isinstance(setting['digit grouping'], int):
            if setting['scale'] == 'short':
                archmagnitude = common_lexicon['integer']['archmagnitude']['original']['short']
            elif setting['scale'] == 'long':
                archmagnitude = common_lexicon['integer']['archmagnitude']['original']['long']
        else:
            archmagnitude = common_lexicon['integer']['archmagnitude']['south asian']

        def write_integer(integer_notation):
            def determine_initial_focused():
                if isinstance(setting['digit grouping'], int):
                    focused_archmagnitude = (len(integer_notation) - 1) // setting['digit grouping']
                    focused_magnitude = (len(integer_notation) - 1) % setting['digit grouping']
                else:
                    if len(integer_notation) <= setting['digit grouping'][0]:
                        focused_archmagnitude = 0
                        focused_magnitude = len(integer_notation) - 1
                    else:
                        focused_archmagnitude = (len(integer_notation) - 1 - setting['digit grouping'][0]) // \
                                                setting['digit grouping'][1] + 1
                        focused_magnitude = (len(integer_notation) - 1 - setting['digit grouping'][0]) % \
                                            setting['digit grouping'][1]
                return [focused_archmagnitude, focused_magnitude]

            def determine_focused_magnitude_change(focused_magnitude):
                if focused_magnitude == 1 and current_digit == '1':
                    focused_magnitude -= 2
                else:
                    focused_magnitude -= 1
                return focused_magnitude

            def renew_focused_magnitude():
                if isinstance(setting['digit grouping'], int):
                    return setting['digit grouping'] - 1
                else:
                    if focused_archmagnitude == 1:
                        return setting['digit grouping'][0] - 1
                    return setting['digit grouping'][1] - 1
            
            def write_digit():
                if current_digit == '0' and integer_notation != '0':
                    return ''
                if current_digit == '1':
                    if (setting['one in 100 and 1000 – number or indefinite article'] != 0 and focused_magnitude == 2) \
                            or \
                            (setting['one in 100 and 1000 – number or indefinite article'] != 0 and
                             focused_archmagnitude[0] > 0) and int(current_digit_group) == 1:
                        return common_lexicon['article']['indefinite'] + _space
                return common_lexicon['integer']['digit'][int(current_digit)] + _space

            def write_ten_to_nineteen():
                return common_lexicon['integer']['ten to nineteen'][int(current_digit_group[-1])] + _space

            def write_tens():
                if current_digit_group[-1] != '0':
                    return common_lexicon['integer']['tens'][int(current_digit)] + common_lexicon['_hyphen']
                return common_lexicon['integer']['tens'][int(current_digit)] + _space

            def write_hundred():
                return common_lexicon['integer']['hundred'] + _space

            def write_conjunction_between_hundred_and_ten():
                if setting['between hundred and ten – with or without conjunction'] == 0 and \
                        current_digit_group[-2] != '0':
                    return common_lexicon['conjunction'] + _space
                return ''

            def write_conjunction_in_vacant_place():
                if setting['vacant place – with or without conjunction'] == 0:
                    next_digit = current_digit_group[-focused_magnitude]
                    if current_digit == '0' and focused_magnitude > 0 and next_digit != '0':
                        return common_lexicon['conjunction'] + _space
                return ''

            def write_digit_and_magnitude():
                local_writing = ''
                if focused_magnitude == 0:
                    local_writing += write_digit()
                elif focused_magnitude == 1:
                    if current_digit != '0':
                        if current_digit == '1':
                            local_writing += write_ten_to_nineteen()
                        else:
                            local_writing += write_tens()
                elif focused_magnitude == 2:
                    if current_digit != '0':
                        local_writing += write_digit() + write_hundred() + \
                                         write_conjunction_between_hundred_and_ten()
                local_writing += write_conjunction_in_vacant_place()
                return local_writing

            def write_archmagnitude():
                if focused_archmagnitude == 0 or int(current_digit_group) == 0:
                    return ''
                return archmagnitude[focused_archmagnitude] + _space

            writing = ''
            focused_archmagnitude, focused_magnitude = determine_initial_focused()
            while focused_archmagnitude >= 0:
                current_digit_group = get.digit_group(integer_notation, focused_archmagnitude,
                                                      setting['digit grouping'])
                while focused_magnitude >= 0:
                    current_digit = current_digit_group[-focused_magnitude-1]
                    writing += write_digit_and_magnitude()
                    focused_magnitude = determine_focused_magnitude_change(focused_magnitude)
                focused_magnitude = renew_focused_magnitude()
                writing += write_archmagnitude()
                focused_archmagnitude -= 1
            return writing[:-1]

        def write_ordinal(ordinal_notation):
            ordinal_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['common']['ordinal']
            first_ten_and_unit = ordinal_notation[-2:]
            if ordinal_notation == first_ten_and_unit:
                if int(first_ten_and_unit) <= 19:
                    return ordinal_lexicon['zero to nineteen'][int(first_ten_and_unit)]
                if int(first_ten_and_unit) % 10 == 0:
                    return ordinal_lexicon['tens'][int(first_ten_and_unit) // 10]
                return common_lexicon['integer']['tens'][int(first_ten_and_unit) // 10] + \
                    common_lexicon['_hyphen'] + ordinal_lexicon['zero to nineteen'][int(first_ten_and_unit) % 10]
            if int(first_ten_and_unit) == 0:
                return write_integer(ordinal_notation) + ordinal_lexicon['suffix']
            return write_integer(ordinal_notation)[:-len(write_integer(first_ten_and_unit))] + \
                write_ordinal(first_ten_and_unit)

        def output_math_numeral(math_numeral_notation):
            math_numeral_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['math numeral']

            def write_fraction(fraction_notation):
                numerator, denominator = fraction_notation.split('/')
                if denominator == '2':
                    if numerator == '1':
                        denominator = common_lexicon['fraction']['half']['singular']
                    else:
                        denominator = common_lexicon['fraction']['half']['plural']
                elif denominator == '4':
                    denominator = common_lexicon['fraction']['quarter']
                    if numerator != '1':
                        denominator += common_lexicon['fraction']['plural']
                else:
                    numerator = write_integer(numerator)
                    denominator = write_ordinal(denominator)
                return numerator + _space + denominator

            def write_mixed_number(mixed_number_notation):
                integer, fraction = mixed_number_notation.replace('-', '+').split('+')
                integer = write_integer(integer)
                fraction = write_fraction(fraction)
                return integer + _space + common_lexicon['conjunction'] + _space + fraction

            def write_decimal(decimal_notation):
                integer, decimal = decimal_notation.split('.')
                integer = write_integer(integer)
                decimal = write.consecutive_digit(decimal, common_lexicon['integer']['digit'],
                                                  common_lexicon['_hyphen'])
                return integer + _space + math_numeral_lexicon['decimal'] + _space + decimal

            def write_scientific_notation(scientific_notation):
                multiplier, exponent = scientific_notation.split('e')
                if check.is_natural_number(multiplier):
                    multiplier = write_integer(multiplier)
                else:
                    multiplier = write_decimal(multiplier)
                exponent_is_negative = check.is_negative(exponent)
                if exponent_is_negative:
                    exponent = exponent[1:]
                if setting['exponent – cardinal or ordinal'] == 0:
                    exponent = write_integer(exponent)
                else:
                    exponent = write_ordinal(exponent)
                if exponent_is_negative:
                    if setting['negative number – negative or minus'] == 0:
                        exponent = common_lexicon['negative']['negative'] + _space + exponent
                    else:
                        exponent = common_lexicon['negative']['minus'] + _space + exponent
                if setting['exponent – shortened or full'] == 0:
                    return multiplier + _space + \
                           math_numeral_lexicon['scientific notation']['multiply'] + _space + \
                           common_lexicon['integer']['ten to nineteen'][0] + _space + \
                           common_lexicon['preposition']['to'] + _space + \
                           common_lexicon['article']['definite'] + _space + exponent
                else:
                    if setting['exponent – cardinal or ordinal'] == 0:
                        return multiplier + _space + \
                               math_numeral_lexicon['scientific notation']['multiply'] + _space + \
                               common_lexicon['integer']['ten to nineteen'][0] + _space + \
                               common_lexicon['preposition']['to'] + _space + \
                               common_lexicon['article']['definite'] + _space + \
                               math_numeral_lexicon['scientific notation']['power'] + _space + \
                               common_lexicon['preposition']['of'] + _space + exponent
                    else:
                        return multiplier + _space + \
                               math_numeral_lexicon['scientific notation']['multiply'] + _space + \
                               common_lexicon['integer']['ten to nineteen'][0] + _space + \
                               common_lexicon['preposition']['to'] + _space + \
                               common_lexicon['article']['definite'] + _space + \
                               exponent + _space + math_numeral_lexicon['scientific notation']['power']

            is_negative = check.is_negative(math_numeral_notation)
            if is_negative:
                math_numeral_notation = math_numeral_notation[1:]
            is_percent = check.is_percent(math_numeral_notation)
            if is_percent:
                math_numeral_notation = math_numeral_notation[:-1]

            if notation_type == 'integer' or notation_type == 'integer percent':
                writing = write_integer(math_numeral_notation)
            elif notation_type == 'fraction':
                writing = write_fraction(math_numeral_notation)
            elif notation_type == 'mixed number':
                writing = write_mixed_number(math_numeral_notation)
            elif notation_type == 'decimal' or notation_type == 'decimal percent':
                writing = write_decimal(math_numeral_notation)
            else:
                writing = write_scientific_notation(math_numeral_notation)

            if is_percent:
                writing += _space + common_lexicon['percent']
            if is_negative:
                if setting['negative number – negative or minus'] == 0:
                    writing = common_lexicon['negative']['negative'] + _space + writing
                else:
                    writing = common_lexicon['negative']['minus'] + _space + writing
            return writing

        def output_ordinal_number(ordinal_notation):
            return write_ordinal(ordinal_notation)

        def output_nominal_number(nominal_notation):
            nominal_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['nominal']
            if setting['nominal 0 – change or keep'] == 0:
                common_lexicon['integer']['digit'][0] = nominal_lexicon
            return write.consecutive_digit(nominal_notation, common_lexicon['integer']['digit'],
                                           common_lexicon['_hyphen'])

        def output_date_and_time(date_and_time_notation):
            date_and_time_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['date and time']

            def write_date():
                year_notation, month_notation, day_notation = date_and_time_notation[0]
                if 0 <= int(year_notation) <= 100:
                    year_writing = common_lexicon['article']['definite'] + _space + \
                                   date_and_time_lexicon['year'] + _space + \
                                   write_integer(year_notation)
                else:
                    current_digit_group = [year_notation[:-2], year_notation[-2:]]
                    if current_digit_group[-2][-2:] == '00':
                        year_writing = write_integer(current_digit_group[-2]) + _space + \
                                       common_lexicon['integer']['hundred'] + _space + \
                                       write_integer(current_digit_group[1])
                    elif current_digit_group[-2][-1:] == '0' and current_digit_group[-1][0] == '0':
                        year_writing = write_integer(year_notation)
                    else:
                        year_writing = write_integer(current_digit_group[-2]) + _space + \
                                       write_integer(current_digit_group[-1])
                month_writing = date_and_time_lexicon['month'][int(month_notation)]
                day_writing = common_lexicon['article']['definite'] + _space + write_ordinal(day_notation)

                if setting['date – day before month or month before day'] == 0:
                    date_writing = day_writing + _space + \
                                   common_lexicon['preposition']['of'] + _space + month_writing
                else:
                    date_writing = month_writing + _space + day_writing
                date_writing += date_and_time_lexicon['_comma'] + _space + year_writing
                return date_writing

            def write_time():
                nominal_lexicon = yaml.load(open('rule/lexicon/english.yaml'))[current_script]['nominal']

                def write_minute_or_second(minute_or_second_notation):
                    if int(minute_or_second_notation) < 10:
                        return nominal_lexicon + common_lexicon['_hyphen'] + \
                               write_integer(minute_or_second_notation)
                    return write_integer(minute_or_second_notation)

                def write_exact_hour():
                    if setting['exact hour – without or with sharp'] != 0:
                        return write_integer(hour_notation) + _space + \
                               date_and_time_lexicon['o clock'] + _space + \
                               date_and_time_lexicon['sharp']
                    return write_integer(hour_notation) + _space + date_and_time_lexicon['o clock']

                def write_quarter(hour_preposition):
                    hour = common_lexicon['fraction']['quarter'] + _space + hour_preposition + _space
                    if hour_preposition == common_lexicon['preposition']['to']:
                        if hour_notation == '23':
                            return hour + write_integer('0')
                        return hour + write_integer(str(int(hour_notation) + 1))
                    return hour + write_integer(str(int(hour_notation)))

                def write_half():
                    return common_lexicon['fraction']['half']['singular'] + _space + \
                           common_lexicon['preposition']['past'] + _space + write_integer(hour_notation)

                def write_regular_time():
                    hour_writing = ''
                    minute_writing = ''
                    if setting['time – without or with preposition'] == 0:
                        hour_writing += write_integer(hour_notation)
                        minute_writing += write_minute_or_second(minute_notation)
                    else:
                        if int(minute_notation) < 30:
                            hour_writing += common_lexicon['preposition']['past'] + _space + \
                                            write_integer(hour_notation)
                            minute_writing += write_minute_or_second(minute_notation)
                        else:
                            hour_writing += common_lexicon['preposition']['to'] + _space
                            minute_writing += write_integer(str(60 - int(minute_notation)))
                            if hour_notation == '23':
                                hour_writing += write_integer('0')
                            else:
                                hour_writing += write_integer(str(int(hour_notation) + 1))
                    return [hour_writing, minute_writing]

                def write_units():
                    if second_notation == '0':
                        hour_writing = ''
                        minute_writing = ''
                        second_writing = ''
                        if minute_notation == '0':
                            hour_writing += write_exact_hour()
                        elif minute_notation == '15':
                            if setting['minute 15 and 45 – number or quarter hour'] != 0:
                                hour_writing += write_quarter(common_lexicon['preposition']['past'])
                        elif minute_notation == '30' and setting['minute 30 – number or half hour'] != 0:
                            hour_writing += write_half()
                        elif minute_notation == '45':
                            if setting['minute 15 and 45 – number or quarter hour'] != 0:
                                hour_writing += write_quarter(common_lexicon['preposition']['to'])
                        else:
                            hour_writing, minute_writing = write_regular_time()
                    else:
                        hour_writing = write_integer(hour_notation)
                        minute_writing = write_minute_or_second(minute_notation)
                        second_writing = write_minute_or_second(second_notation)
                    return [hour_writing, minute_writing, second_writing]

                hour_notation, minute_notation, second_notation = date_and_time_notation[1]
                hour_writing, minute_writing, second_writing = write_units()
                time_writing = hour_writing
                if minute_writing != '':
                    if setting['time – without or with preposition'] != 0 and second_writing == '':
                        time_writing = minute_writing + _space + time_writing
                    else:
                        if setting['time – without or with preposition'] == 0:
                            time_writing += _space + minute_writing
                            if second_writing != '':
                                time_writing += _space + second_writing
                        else:
                            time_writing += _space + common_lexicon['conjunction'] + _space + minute_writing
                            if second_writing != '':
                                time_writing += _space + common_lexicon['conjunction'] + _space + second_writing
                return time_writing
            if is_text_to_speech:
                return write_date() + '. ' + write_time()
            return write_date() + '\n' + write_time()

        if notation_type == 'date and time':
            final_writing += output_date_and_time(notation)
        elif notation_type == 'nominal number':
            final_writing += output_nominal_number(notation)
        elif notation_type == 'ordinal number':
            final_writing += output_ordinal_number(notation)
        else:
            final_writing += output_math_numeral(notation)
    return final_writing
