def s_do(s_notation_type, s_notation, b_is_for_TTS):
    from pathlib import Path
    from ruamel.yaml import YAML
    yaml = YAML(typ='safe')

    d_setting = yaml.load(Path('rule/setting/deu.yaml').read_text())
    ls_script = d_setting['ls Schrift']
    i_digit_grouping = 3
    s_writing = ''

    for s_the_script in ls_script:
        d_lexicon = yaml.load(Path('rule/lexicon/deu.yaml').read_text())['d '+s_the_script]
        s_the_writing = ''

        ls_digit = d_lexicon['ls Ziffern']
        if d_setting['i „zwei“ als Zahl 2 / „zwo“ als Zahl 2'] != 0:
            ls_digit[2] = d_lexicon['s zwo']
        ls_ten_to_nineteen = d_lexicon['ls zehn bis neunzehn']
        ls_ten = d_lexicon['ls Zehnen']
        if d_setting['i „fünfzehn“, „fünfzig“ / „fuffzehn“, „fuffzig“'] == 1:
            ls_ten_to_nineteen[5] = d_lexicon['s fuffzehn']
            ls_ten[5] = d_lexicon['s fuffzig']
        if d_setting['i „dreiẞig” / „dreissig“'] == 1:
            ls_ten[3] = d_lexicon['s dreissig']
        ls_archmagnitude = d_lexicon['ls Erzgrößen']

        def s_write_integer(s_notation):
            def s_write_digit_and_magnitude():
                def s_write_unit():
                    if s_the_digit == '0':
                        if s_notation != '0':
                            return ''
                    elif s_the_digit == '1':
                        if i_the_archmagnitude == 1 and int(s_the_digit_group) == 1:
                            if d_setting['i „hundert“, „tausand“ / „einhundert“, „eintausand“'] == 0:
                                return ''
                            return d_lexicon['s ein']
                        if i_the_archmagnitude > 1 and int(s_the_digit_group) % 100 == 1:
                            return d_lexicon['s eine']
                    return ls_digit[int(s_the_digit)]

                def s_write_ten_and_unit():
                    if s_the_digit == '0':
                        return ''
                    if s_the_digit == '1':
                        return ls_ten_to_nineteen[int(s_the_digit_group[-1])]
                    if s_the_digit_group.endswith('0'):
                        return ls_ten[int(s_the_digit)]
                    if s_the_digit_group.endswith('1'):
                        return d_lexicon['s ein'] + d_lexicon['s und'] + ls_ten[int(s_the_digit)]
                    return ls_digit[int(s_the_digit_group[-1])] + d_lexicon['s und'] + ls_ten[int(s_the_digit)]

                def s_write_hundred():
                    if s_the_digit == '0':
                        return ''
                    if s_the_digit == '1':
                        if d_setting['i „hundert“, „tausand“ / „einhundert“, „eintausand“'] == 0:
                            return d_lexicon['s hundert']
                        return d_lexicon['s ein'] + d_lexicon['s hundert']
                    return ls_digit[int(s_the_digit)] + d_lexicon['s hundert']

                s_writing = ''
                if i_the_magnitude == 0:
                    s_writing += s_write_unit()
                elif i_the_magnitude == 1:
                    s_writing += s_write_ten_and_unit()
                elif i_the_magnitude == 2:
                    s_writing += s_write_hundred()
                if d_setting['i _ / „und“ für Stellen der „0“'] != 0 and s_the_digit == '0' and i_the_magnitude > 0 and s_the_digit_group[-i_the_magnitude] != '0':
                    if i_the_magnitude == 2 and i_the_archmagnitude > 0:
                        s_writing += ' ' + d_lexicon['s und'] + ' '
                    else:
                        s_writing += d_lexicon['s und']
                return s_writing

            def s_write_archmagnitude():
                if i_the_archmagnitude == 0 or int(s_the_digit_group) == 0:
                    return ''
                if i_the_archmagnitude == 1:
                    return ls_archmagnitude[i_the_archmagnitude]
                if int(s_the_digit_group) > 1:
                    if ls_archmagnitude[i_the_archmagnitude].endswith(d_lexicon['s n']):
                        s_plural_suffix = d_lexicon['s en']
                    else:
                        s_plural_suffix = d_lexicon['s n']
                else:
                    s_plural_suffix = ''
                return ' ' + ls_archmagnitude[i_the_archmagnitude] + s_plural_suffix + ' '

            s_writing = ''
            i_the_archmagnitude = (len(s_notation) - 1) // i_digit_grouping
            i_the_magnitude = (len(s_notation) - 1) % i_digit_grouping
            while i_the_archmagnitude >= 0:
                s_the_digit_group_end = i_digit_grouping * i_the_archmagnitude + 1
                s_the_digit_group_start = s_the_digit_group_end + i_digit_grouping
                s_the_digit_group = f'{s_notation} '[-s_the_digit_group_start:-s_the_digit_group_end]
                while i_the_magnitude >= 0:
                    s_the_digit = s_the_digit_group[-i_the_magnitude - 1]
                    s_writing += s_write_digit_and_magnitude()
                    if i_the_magnitude == 1 and s_the_digit != '0':
                        i_the_magnitude -= 1
                    i_the_magnitude -= 1
                s_writing += s_write_archmagnitude()
                i_the_archmagnitude -= 1
                i_the_magnitude = i_digit_grouping - 1
            return s_writing.rstrip()

        def s_write_ordinal_number(s_notation):
            s_notation = s_notation.removesuffix('.')
            if len(s_notation) < 3:
                if int(s_notation) == 0:
                    return s_write_integer(s_notation) + d_lexicon['s te']
                if int(s_notation) < 10:
                    if d_setting['i „zwei“ als Zahl 2 / „zwo“ als Zahl 2'] != 0 and s_notation.endswith('2'):
                        return d_lexicon['s zwote']
                    return d_lexicon['ls ordinale Ziffern'][int(s_notation[-1])]
                if int(s_notation) < 20:
                    return s_write_integer(s_notation).replace(' ', '').lower() + d_lexicon['s te']
                return s_write_integer(s_notation).replace(' ', '').lower() + d_lexicon['s s'] + d_lexicon['s te']
            if int(s_notation[-2:]) == 0:
                if len(s_notation) > i_digit_grouping * 2 - 1 and int(s_notation[-i_digit_grouping * 2:]) == 0:
                    return s_write_integer(s_notation).replace(' ', '').lower().replace(d_lexicon['s eine'], d_lexicon['s ein']) + d_lexicon['s s'] + d_lexicon['s te']
                return s_write_integer(s_notation).replace(' ', '').lower() + d_lexicon['s s'] + d_lexicon['s te']
            return s_write_integer(s_notation)[:-len(s_write_integer(s_notation[-2:]))].replace(' ', '').lower() + s_write_ordinal_number(s_notation[-2:])

        def s_write_nominal_number(s_notation):
            s_notation = s_notation.removeprefix('#')
            if d_setting['i „zwo“ als Nummer 2 / „zwei“ als Nummer 2'] == 0:
                ls_digit[2] = d_lexicon['s zwo']
            s_writing = ''
            for s_character in s_notation:
                s_writing += ls_digit[int(s_character)]
            return s_writing

        def s_write_fraction(s_notation):
            s_numerator_notation, s_denominator_notation = s_notation.split('/')
            if s_numerator_notation == '1':
                s_numerator_writing = d_lexicon['s ein']
            else:
                s_numerator_writing = s_write_integer(s_numerator_notation)
            if s_denominator_notation == '2':
                s_denominator_writing = d_lexicon['s halb']
            else:
                s_denominator_writing = s_write_ordinal_number(s_denominator_notation)[:-len(d_lexicon['s te'])]
                s_denominator_writing = s_denominator_writing.capitalize() + d_lexicon['s tel']
            return s_numerator_writing + ' ' + s_denominator_writing

        def s_write_mixed_number(s_notation):
            s_integer_notation, s_fraction_notation = s_notation.replace('-', '+').split('+')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_fraction_writing = s_write_fraction(s_fraction_notation)
            return s_integer_writing + ' ' + s_fraction_writing

        def s_write_decimal(s_notation):
            s_integer_notation, s_decimal_notation = s_notation.split('.')
            s_integer_writing = s_write_integer(s_integer_notation)
            s_decimal_writing = ''
            for s_character in s_decimal_notation:
                s_decimal_writing += ls_digit[int(s_character)]
            return s_integer_writing + ' ' + d_lexicon['s Komma'] + ' ' + s_decimal_writing

        def s_write_scientific_or_engineering_notation(s_notation):
            s_coefficient_notation, s_exponent_notation = s_notation.split('E')
            if s_coefficient_notation.is_digit():
                s_coefficient_writing = s_write_integer(s_coefficient_notation)
            else:
                s_coefficient_writing = s_write_decimal(s_coefficient_notation)
            if s_exponent_notation.startswith('-'):
                s_exponent_writing = d_lexicon['s minus'] + ' ' + s_write_integer(s_exponent.removeprefix('-'))
            else:
                s_exponent_writing = s_write_integer(s_exponent_notation)
            return s_coefficient_writing + ' ' + d_lexicon['s mal'] + ' ' + ls_ten_to_nineteen[0] + ' ' + d_lexicon['s hoch'] + ' ' + s_exponent_writing

        def s_write_date_and_time(s_notation):
            def s_write_date():
                s_year_notation, s_month_notation, s_day_notation = s_date_notation.split('-')
                s_year_writing = s_write_integer(s_year_notation)
                if d_setting['i Monateamen / Monateummer'] == 0:
                    s_month_writing = d_lexicon['ls Monatenamen'][int(s_month_notation)]
                else:
                    s_month_writing = s_write_ordinal_number(s_month_notation)
                s_day_writing = s_write_ordinal_number(s_day_notation)
                return d_lexicon['s der'] + ' ' + s_day_writing + ' ' + s_month_writing + ',' + ' ' + s_year_writing

            def s_write_time():
                s_hour_notation, s_minute_notation, s_second_notation = s_time_notation.split(':')
                def write_half_hour(s_notation):
                    if s_notation == '23':
                        s_notation = '-1'
                    return d_lexicon['s halb'] + ' ' + s_write_integer(str(int(s_notation) + 1))

                def s_write_quarter_of_hour(s_notation):
                    if d_setting['i „Viertel nach“, „Viertel vor“ / „viertel“, „dreiviertel“'] == 0:
                        if s_notation == '23' and int(s_minute_notation) > 30:
                            s_notation = '-1'
                        if int(s_minute_notation) < 30:
                            return s_write_ordinal_number('1/4').capitalize() + ' ' + d_lexicon['s nach'] + ' ' + s_write_integer(s_notation)
                        return s_write_ordinal_number('1/4').capitalize() + ' ' + d_lexicon['s vor'] + ' ' + s_write_integer(str(int(s_notation) + 1))
                    else:
                        if s_notation == '23':
                            s_notation = '-1'
                        if int(s_minute_notation) < 30:
                            s_hour_fraction = s_write_ordinal_number('1/4')
                        else:
                            s_hour_fraction = s_write_ordinal_number('3/4')
                        return s_hour_fraction + ' ' + s_write_integer(str(int(s_notation) + 1))

                if s_second_notation == '0':
                    s_hour_writing = ''
                    s_minute_writing = ''
                    s_second_writing = ''
                    if s_minute_notation == '0':
                        s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s Uhr']
                        if d_setting['i _ / „Punkt“'] != 0:
                            s_hour_writing = d_lexicon['s Punkt'] + ' ' + s_hour_writing
                    elif s_minute_notation == '30':
                        if d_setting['i „dreißig“ / „halb“'] != 0:
                            s_hour_writing = write_half_hour(s_hour_notation)
                    elif s_minute_notation in ['15', '45']:
                        if d_setting['i „fünfzehn“, „fünfundvierzig“ / „Viertel nach“, „Viertel vor“ oder …'] != 0 and d_setting['i „vor“, „nach“ zur vollen Stunde / „vor“, „nach“ zur vollen, halben Stunde oder …'] != 0:
                            s_hour_writing = s_write_quarter_of_hour(s_hour_notation)
                    else:
                        if d_setting['i Uhr und Minute / „vor“, „nach“ zur vollen Stunde oder …'] == 0:
                            s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s Uhr']
                            s_minute_writing = s_write_integer(s_minute_notation)
                        else:
                            s_minute_count = ''
                            s_hour_fraction = ''
                            if d_setting['i „vor“, „nach“ zur vollen Stunde / „vor“, „nach“ zur vollen, halben Stunde oder …'] == 0:
                                if int(s_minute_notation) < 30:
                                    s_minute_count = s_write_integer(s_minute_notation)
                                    s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_integer(s_hour_notation)
                                else:
                                    s_minute_count = s_write_integer(str(60 - int(s_minute_notation)))
                                    if s_hour_notation == '23':
                                        s_hour_notation = '-1'
                                    s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_integer(str(int(s_hour_notation) + 1))
                            else:
                                if d_setting['i „vor“, „nach“ zur vollen, halben Stunde / „vor“, „nach“ zur vollen, halben Stunde und Viertelstunde'] == 0:
                                    if int(s_minute_notation) < 15:
                                        s_minute_count = s_write_integer(s_minute_notation)
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_integer(s_hour_notation)
                                    elif int(s_minute_notation) < 30:
                                        s_minute_count = s_write_integer(str(30 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_integer(str(int(s_hour_notation) + 1))
                                    elif int(s_minute_notation) < 45:
                                        s_minute_count = s_write_integer(str(int(s_minute_notation) - 30))
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_integer(s_hour_notation)
                                    else:
                                        s_minute_count = s_write_integer(str(60 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_integer(str(int(s_hour_notation) + 1))
                                else:
                                    if int(s_minute_notation) < 8:
                                        s_minute_count = s_write_integer(s_minute_notation)
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_integer(s_hour_notation)
                                    elif int(s_minute_notation) < 15:
                                        s_minute_count = s_write_integer(str(15 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_quarter_of_hour(s_hour_notation)
                                    elif int(s_minute_notation) < 23:
                                        s_minute_count = s_write_integer(str(int(s_minute_notation) - 15))
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_quarter_of_hour(s_hour_notation)
                                    elif int(s_minute_notation) < 30:
                                        s_minute_count = s_write_integer(str(30 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + write_half_hour(s_hour_notation)
                                    elif int(s_minute_notation) < 38:
                                        s_minute_count = s_write_integer(str(int(s_minute_notation) - 30))
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + write_half_hour(s_hour_notation)
                                    elif int(s_minute_notation) < 45:
                                        s_minute_count = s_write_integer(str(45 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_quarter_of_hour(s_hour_notation)
                                    elif int(s_minute_notation) < 53:
                                        s_minute_count = s_write_integer(str(int(s_minute_notation) - 45))
                                        s_hour_fraction = d_lexicon['s nach'] + ' ' + s_write_quarter_of_hour(s_hour_notation)
                                    else:
                                        s_minute_count = s_write_integer(str(60 - int(s_minute_notation)))
                                        s_hour_fraction = d_lexicon['s vor'] + ' ' + s_write_quarter_of_hour(s_hour_notation)
                            s_hour_writing = s_minute_count + ' ' + s_hour_fraction
                else:
                    s_hour_writing = s_write_integer(s_hour_notation) + ' ' + d_lexicon['s Uhr']
                    s_minute_writing = s_write_integer(s_minute_notation)
                    s_second_writing = s_write_integer(s_second_notation)

                s_time_writing = s_hour_writing
                if s_minute_writing != '':
                    s_time_writing += ' ' + s_minute_writing
                    if s_second_writing != '':
                        s_time_writing += ' ' + s_second_writing
                return s_time_writing

            s_date_notation, s_time_notation = s_notation.split()
            if b_is_for_TTS:
                return s_write_date() + '. ' + s_write_time()
            return s_write_date() + '\n' + s_write_time()

        b_is_negative = s_notation.startswith('-')
        if b_is_negative:
            s_notation = s_notation.removeprefix('-')
        b_is_percent = s_notation.endswith('%')
        if b_is_percent:
            s_notation = s_notation.removesuffix('%')

        if s_notation_type in [ 'integer', 'integer percent' ]:
            s_the_writing += s_write_integer(s_notation)
        elif s_notation_type == 'fraction':
            s_the_writing += s_write_fraction(s_notation)
        elif s_notation_type == 'mixed number':
            s_the_writing += s_write_mixed_number(s_notation)
        elif s_notation_type in [ 'decimal', 'decimal percent' ]:
            s_the_writing += s_write_decimal(s_notation)
        elif s_notation_type in [ 'scientific notation', 'engineering notation' ]:
            s_the_writing += s_write_scientific_or_engineering_notation(s_notation)
        elif s_notation_type == 'ordinal number':
            s_the_writing += s_write_ordinal_number(s_notation)
        elif s_notation_type == 'nominal number':
            s_the_writing += s_write_nominal_number(s_notation)
        elif s_notation_type == 'date and time' :
            s_the_writing += s_write_date_and_time(s_notation)

        if b_is_percent:
            s_notation += '%'
            s_the_writing += ' ' + d_lexicon['s Prozent']
        if b_is_negative:
            s_notation = '-' + s_notation
            s_the_writing = d_lexicon['s minus'] + ' ' + s_the_writing
        s_writing += s_the_writing + '\n'
    return s_writing
