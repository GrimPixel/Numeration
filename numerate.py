import rule.library.check as check
import rule.library.convert as convert
import rule.library.get as get
import rule.library.write as write
from random import randrange
from importlib import import_module
from playsound2 import playsound
from os import remove
from ruamel.yaml import YAML

yaml = YAML(typ='safe')


def language_setting_selection():
    language_code = yaml.load(open('setting.yaml'))['language']
    if not check.language_setting(language_code):
        language_code = input(locale['Invalid language code!'])
        exit()
    language_code = language_code.split()
    language_setting = []
    for x in language_code:
        if len(x) > 3:
            setting_identifier = x[3:]
            x = x[:3]
        else:
            setting_identifier = '0'
        language_name = yaml.load(open('rule/library/language decode.yaml'))[x]
        language_setting.append([language_name, setting_identifier])
    return language_setting


def subject_selection():
    subject = input(locale['Subject:'] + '\n' +
                    locale['0. integer, fraction, mixed number, percentage, decimal, scientific notation'] + '\n' +
                    locale['1. ordinal number'] + '\n' +
                    locale['2. nominal number'] + '\n' +
                    locale['3. date and time'] + '\n')
    while not (subject.isdigit() and 0 <= int(subject) <= 3):
        subject = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
    return subject


def depose_unnecessary_zero(notation):
    def depose_integer_and_decimal_zero(integer_and_decimal):
        while integer_and_decimal[0] == '0' and integer_and_decimal != '0':
            integer_and_decimal = integer_and_decimal[1:]
        return integer_and_decimal

    def depose_fraction_zero(fraction):
        numerator, denominator = fraction.split('/')
        return depose_integer_and_decimal_zero(numerator) + '/' + depose_integer_and_decimal_zero(denominator)

    def depose_mixed_number_zero(mixed_number):
        if is_negative:
            connector = '-'
            integer, fraction = mixed_number.split('-')
        else:
            connector = '+'
            integer, fraction = mixed_number.split('+')
        return depose_integer_and_decimal_zero(integer) + connector + depose_fraction_zero(fraction)

    def depose_scientific_notation_zero(scientific_notation):
        multiplier, exponent = scientific_notation.split('e')
        multiplier = depose_integer_and_decimal_zero(multiplier)
        if '-' in exponent:
            exponent = '-' + depose_integer_and_decimal_zero(exponent[1:])
        else:
            exponent = depose_integer_and_decimal_zero(exponent)
        return multiplier + 'e' + exponent

    def depose_date_and_time_zero(date_and_time):
        for x in range(len(date_and_time)):
            date_and_time[x] = depose_integer_and_decimal_zero(date_and_time[x])
        return date_and_time

    if check.is_negative(notation):
        is_negative = True
        notation = notation[1:]
    else:
        is_negative = False
    if check.is_percent(notation):
        is_percent = True
        notation = notation[:-1]
    else:
        is_percent = False

    if check.is_natural_number(notation) or check.is_non_negative_decimal(notation):
        notation = depose_integer_and_decimal_zero(notation)
    elif check.is_non_negative_fraction(notation):
        notation = depose_fraction_zero(notation)
    elif check.is_mixed_number(notation, is_negative):
        notation = depose_mixed_number_zero(notation)
    elif check.is_non_negative_scientific_notation(notation):
        notation = depose_scientific_notation_zero(notation)
    else:
        notation = depose_date_and_time_zero(notation)

    if is_percent:
        notation += '%'
    if is_negative:
        notation = '-' + notation
    return notation


def convert_to_western_arabic(notation, numeral_system):
    if numeral_system == 'eastern arabic':
        return convert.common_writing_system_with_0_to_9_and_variants_to_western_arabic(notation, numeral_system)
    elif numeral_system in ['adlam', 'balinese', 'bengali', 'burmese', 'chakma', 'cham', 'devanagari', 'gujarati',
                            'gunjala gondi', 'gurmukhi', 'hanifi rohingya', 'javanese', 'kannada', 'kayah li',
                            'khmer', 'khudawadi', 'lao', 'lepcha', 'limbu', 'masaram gondi', 'modi', 'mongolian',
                            'mro', 'newa', 'nko', 'ol chiki', 'osmanya', 'saurashtra', 'shan', 'sharada',
                            'sora sompeng', 'sundanee', 'tai laing', 'takri', 'thai', 'tirhuta', 'vai', 'wancho']:
        return convert.common_writing_system_with_0_to_9_to_western_arabic(notation, numeral_system)
    return notation


def input_notation(language_setting):
    maximal_length = get.common_maximal_length(language_setting)
    notation = input(locale['Number with maximal length {{maximal_length}:'] + str(maximal_length) +
                     locale['Number with maximal length {maximal_length}}:'] + '\n')
    while not 0 < len(notation) <= maximal_length:
        notation = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
    if subject == '0':
        notation_numeral_system = ''
        notation_type = ''
        while notation_numeral_system == '' or notation_type == '':
            notation_numeral_system = get.notation_numeral_system(notation)
            notation = write.unified_sign(notation)
            notation_type = get.notation_type(notation)
            if notation_numeral_system == '':
                print(locale['Invalid note!'])
            if notation_type == '':
                print(locale['Invalid sign!'])
            if notation_numeral_system == '' or notation_type == '':
                notation = input(locale['Retry:'] + '\n')
    else:
        notation_numeral_system = get.notation_numeral_system(notation)
        if subject == '1':
            notation_type = 'ordinal number'
        else:
            notation_type = 'nominal number'
        while notation_numeral_system == '':
            notation = input(locale['Invalid note!'] + '\n' + locale['Retry:'] + '\n')
            notation_numeral_system = get.notation_numeral_system(notation)
            notation = write.unified_sign(notation)

    if notation_numeral_system != 'western arabic':
        notation = convert_to_western_arabic(notation, notation_numeral_system)
    notation = write.unified_sign(notation)
    if subject != '2':
        notation = depose_unnecessary_zero(notation)
    return [notation, notation_type]


def input_date_and_time_notation(language_setting):
    def date_process(date_notation):
        date_notation = write.unified_date_separator(date_notation)
        date_notation = date_notation.split('-')
        for x in range(len(date_notation)):
            notation_numeral_system = get.notation_numeral_system(date_notation[x])
            date_notation[x] = convert_to_western_arabic(date_notation[x], notation_numeral_system)
            date_notation[x] = depose_unnecessary_zero(date_notation[x])
        if len(date_notation) != 3:
            return date_notation
        if date_format == '1':
            day, month, year = date_notation
        elif date_format == '2':
            month, day, year = date_notation
        else:
            year, month, day = date_notation
        date_notation = [year, month, day]
        return date_notation

    def time_process(time_notation):
        time_notation = write.unified_time_separator(time_notation)
        time_notation = time_notation.split(':')
        for x in range(len(time_notation)):
            notation_numeral_system = get.notation_numeral_system(time_notation[x])
            time_notation[x] = convert_to_western_arabic(time_notation[x], notation_numeral_system)
            time_notation[x] = depose_unnecessary_zero(time_notation[x])
        return time_notation

    maximal_length = get.common_maximal_length(language_setting)
    date_format = yaml.load(open('setting.yaml'))['date format']
    date_notation = input(locale['Date with maximal length {{maximal_length}:'] + str(maximal_length) +
                          locale['Date with maximal length {maximal_length}}:'] + '\n')
    date_notation = date_process(date_notation)
    while not check.date(date_notation):
        date_notation = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
        date_notation = date_process(date_notation)
    time_notation = input(locale['Date with maximal length {{maximal_length}:'] + str(maximal_length) +
                          locale['Date with maximal length {maximal_length}}:'] + '\n')
    time_notation = time_process(time_notation)
    while not check.time(time_notation):
        time_notation = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
        time_notation = time_process(time_notation)
    return [date_notation, time_notation]


def output_writing(current_language_setting, notation, notation_type, is_text_to_speech):
    writing = ''
    language_name, setting_identifier = current_language_setting
    if isinstance(language_name, str):
        language_module = import_module(f'rule.{language_name}')
        writing += language_module.do(notation, notation_type, setting_identifier, is_text_to_speech)
    elif isinstance(language_name, list):
        individual_language_name, macrolanguage_name = language_name
        language_module = import_module(f'rule.{macrolanguage_name}')
        writing += language_module.do(notation, notation_type, setting_identifier,
                                      individual_language_name, is_text_to_speech)
    return writing


def speak(current_language_setting, notation, notation_type):
    text_to_speech_module = setting['text-to-speech']['module']
    if text_to_speech_module != 'none':
        file = '/tmp/temp.mp3'
        if text_to_speech_module == 'gtts':
            from gtts import gTTS
            language_code_for_gtts = get.language_code_for_text_to_speech_engine(current_language_setting[0],
                                                                                 'google translate')
            if language_code_for_gtts != '':
                if language_code_for_gtts == 'zh':
                    language_code_for_gtts += '-' + setting['text-to-speech']['gtts']['voice']['mandarin chinese']
                is_slow = setting['text-to-speech']['gtts']['is slow']
                text_to_speech_writing = output_writing(current_language_setting, notation, notation_type, True)
                gTTS(text=text_to_speech_writing, lang=language_code_for_gtts, slow=is_slow).save(file)
                playsound(file)
                remove(file)
        if text_to_speech_module == 'pyttsx3':
            import pyttsx3
            language_code_for_pyttsx3 = ''
            engine = setting['text-to-speech']['pyttsx3']['engine']
            if engine == 'espeak':
                language_code_for_pyttsx3 = get.language_code_for_text_to_speech_engine(current_language_setting[0],
                                                                                        'espeak')
            elif engine == 'nsss':
                language_code_for_pyttsx3 = get.language_code_for_text_to_speech_engine(current_language_setting[0],
                                                                                        'nsss')
            elif engine == 'sapi5':
                language_code_for_pyttsx3 = get.language_code_for_text_to_speech_engine(current_language_setting[0],
                                                                                        'sapi5')
            if language_code_for_pyttsx3 != '':
                text_to_speech_writing = output_writing(current_language_setting, notation, notation_type, True)
                pyttsx3.init().save_to_file(text_to_speech_writing, file)
                playsound(file)
                remove(file)


setting = yaml.load(open('setting.yaml'))
mode = setting['mode']
locale = setting['locale']['code']
locale = yaml.load(open('locale/' + locale + '.yaml'))

language_setting = language_setting_selection()
subject = ''
notation = ''
if mode == 'learning':
    route = '2'
    while route != '0':
        if route == '2':
            subject = subject_selection()
        if subject != '3':
            notation, notation_type = input_notation(language_setting)
        else:
            notation = input_date_and_time_notation(language_setting)
            notation_type = 'date and time'

        for x in language_setting:
            current_language_setting = x
            print(output_writing(current_language_setting, notation, notation_type, False))
            speak(current_language_setting, notation, notation_type)

        route = input('\n' +
                      locale['Next:'] + '\n' +
                      locale['0. End'] + '\n' +
                      locale['1. Do again with the subject unchanged'] + '\n' +
                      locale['2. Start over'] + '\n')
        while not (route.isdigit() and 0 <= int(route) <= 2):
            route = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
elif mode == 'practising':
    def add_percent_and_negative(notation):
        if without_or_with_percent == '0':
            notation += '%'
        if without_or_with_negative == '0':
            notation = '-' + notation
        return notation

    def practise_integer():
        integer_range = practice_setting['integer range']
        notation = str(randrange(integer_range[0], integer_range[1]))
        notation = add_percent_and_negative(notation)
        final_writing = ''
        for x in language_setting:
            current_language_setting = x
            writing = output_writing(current_language_setting, notation, notation_type, False)
            final_writing += writing + '\n'
            speak(current_language_setting, notation, notation_type)
        return [notation, final_writing]

    practice_setting = yaml.load(open('setting.yaml'))['practice']
    notation_type = practice_setting['notation type']
    without_or_with_negative = practice_setting['without or with negative']
    without_or_with_percent = practice_setting['without or with percent']
    route = '1'
    while route != '0':
        if route == '1':
            language_setting = language_setting_selection()
        writing = ''
        if notation_type == 'integer':
            notation, writing = practise_integer()
        input()
        print(notation)
        input()
        print(writing)
        route = input(locale['Next:'] + '\n' +
                      locale['0. End'] + '\n' +
                      locale['1. Start over'] + '\n')
        while not (route.isdigit() and 0 <= int(route) <= 1):
            route = input(locale['Invalid!'] + '\n' + locale['Retry:'] + '\n')
