import library.get as get
import library.convert as convert
import library.unify as unify
import library.depose as depose
import library.write as write
from importlib import import_module
from pathlib import Path
from playsound import playsound
from ruamel.yaml import YAML
from tempfile import NamedTemporaryFile
yaml = YAML(typ='safe')


def ls_input_notation():
    s_notation = ''
    while True:
        s_notation = input()
        if s_notation == '':
            exit()
        s_notation = unify.s_symbols(s_notation)
        s_notation_type = get.s_notation_type(s_notation)
        i_common_maximal_length = get.i_common_maximal_length(ls_language_code)
        if s_notation_type != '' and 0 < len(s_notation) <= i_common_maximal_length:
            s_notation = depose.s_leading_zeros(s_notation_type, s_notation)
            break
        else:
            print(d_locale['s Invalid input'] + '\n')
    return [s_notation_type, s_notation]


def s_output_writing(s_the_language_code, s_notation_type, s_notation, b_is_for_TTS=False):
    s_writing = ''
    language_code_module = import_module('rule.' + s_the_language_code)
    s_writing += language_code_module.s_do(s_notation_type, s_notation, b_is_for_TTS)
    return s_writing


def _speak_text(s_notation_type, d_TTS_setting):
    s_engine_language_code = yaml.load(Path('library/dictionary/engine_language_code.yaml').read_text())

    if d_TTS_setting['s module'] == 'gtts':
        from gtts import gTTS
    elif d_TTS_setting['s module'] == 'rlvoice':
        import rlvoice
        engine = rlvoice.init(d_setting['d rlvoice']['s driver'])

    if d_TTS_setting['s module'] == 'gtts':
        if s_the_language_code == 'zho':
            if yaml.load(Path('rule/setting/zho.yaml').read_text())['s 語音方言'] == '官話':
                s_engine_language_code = s_engine_language_code['d google translate']['s cmn'] + '-' + d_TTS_setting['d gtts']['d region']['s cmn']
            else:
                s_engine_language_code = ''
            s_top_level_domain = 'com'
        elif s_the_language_code in [ 'eng', 'fra', 'por', 'spa' ]:
            s_engine_language_code = s_engine_language_code['d google translate']['s '+s_the_language_code]
            s_region = d_TTS_setting['d gtts']['d region']['s '+s_the_language_code]
            s_top_level_domain = yaml.load(Path('library/dictionary/top_level_domain.yaml').read_text())['d google translate']['s '+s_region]
        else:
            s_engine_language_code = yaml.load(Path('library/engine_language_code.yaml').read_text())['d google translate']['s '+s_the_language_code]
            s_top_level_domain = 'com'

        if s_engine_language_code != '':
            b_slow = d_TTS_setting['d gtts']['b is slow']
            s_text_to_speech_writing = s_output_writing(s_the_language_code, s_notation_type, s_notation, True)
            gTTS(text=s_text_to_speech_writing, lang=s_engine_language_code, tld=s_top_level_domain, slow=b_slow).write_to_fp(voice_segment := NamedTemporaryFile())
            playsound(voice_segment.name)
            voice_segment.close()
    elif d_TTS_setting['s module'] == 'rlvoice':
        if d_setting['d rlvoice']['s driver'] == 'espeak':
            if s_the_language_code in [ 'eng', 'por', 'spa' ] and d_setting['d rlvoice']['d espeak']['d region']['s '+s_the_language_code] != '':
                s_engine_language_code = s_engine_language_code['d espeak']['s '+s_the_language_code] + '-' + d_setting['d rlvoice']['d region']['s '+s_the_language_code]
            else:
                s_engine_language_code = s_engine_language_code['d espeak']['s '+s_the_language_code]
        elif d_setting['d rlvoice']['s driver'] == 'sapi5':
            s_engine_language_code = s_engine_language_code['d sapi5']['s '+s_the_language_code] + '-' + d_setting['d rlvoice']['d region']['s '+s_the_language_code]
        engine.save_to_file(item, audio_file_name)


d_setting = yaml.load(Path('setting.yaml').read_text())
s_mode = d_setting['s mode']
d_locale = yaml.load(Path('locale/' + d_setting['s locale'] + '.yaml').read_text())

ls_language_code = d_setting['ls language code']
i_maximal_length = get.i_common_maximal_length(ls_language_code)
s_notation = ''

if s_mode == 'learning':
    print(d_locale['s Input notation with maximal length {{i_maximal_length}:'] + str(i_maximal_length) + d_locale['s Input notation with maximal length {i_maximal_length}}:'] + '\n' + d_locale['s Input no character to end'])
    while True:
        s_notation_type, s_notation = ls_input_notation()
        for s_the_language_code in ls_language_code:
            print(convert.s_from_european(d_setting['s output numeral system'].replace(' ', '_').lower(), s_notation))
            print(s_output_writing(s_the_language_code, s_notation_type, s_notation, False))
            if d_setting['d TTS']['s module'] != '':
                try:
                    _speak_text(s_notation_type, d_setting['d TTS'])
                except:
                    print(d_locale['s TTS language code unsupported'])
elif s_mode == 'practising':
    d_practice_setting = yaml.load(Path('setting.yaml').read_text())['d practising']
    while True:
        s_notation = write.s_notation(d_practice_setting)
        if s_notation != '0' and d_practice_setting['i with or without negative'] == 0:
            s_notation = write.negative(s_notation)
        if d_setting['d TTS']['s module'] != '':
            for s_the_language_code in ls_language_code:
                try:
                    _speak_text(d_practice_setting['s notation type'], d_setting['s TTS'])
                except:
                    print(d_locale['s TTS language code unsupported'])
        input()
        print(convert.s_from_european(d_setting['s output numeral system'].replace(' ', '_').lower(), s_notation))
